package authorization.client.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import authorization.client.canale.CanaleCli;
import authorization.client.canale.CanaleCliZMQ;
import jsonRpc.client.JsonRpcClient;

public class ClientCore {

    private final CanaleCli canale; 
    private final ObjectMapper mapper;
    private final JsonRpcClient jsonRpcClient;

    /**
     * Le operazioni indispensabili per l'esecuzione del client sono la inizializzazione dei seguenti:<br>
     * - canale di comunicazione<br>
     * - procedure remote json per client<br>
     * - gestore chiamate<br>
     */
    public ClientCore(String ip) {
        //Creo e apro il canale sul ip scelto alla porta standard
        canale = new CanaleCliZMQ(ip);

        //Imposto date come timestamp
        mapper = new ObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        //Creo un oggetto json remote procedure call di tipo client sul canale aperto 
        jsonRpcClient = new JsonRpcClient(canale,mapper);
    }
    
    public void avvia(){    
    	//Apro il canale di comunicazione sul quale invare i messaggi
    	canale.apri();

    	//Assegno al servitore delle chiamate il client tramite il quale effettuare le chiamate remote
    	ServitoreClient.getInstance().setJsonRpcClient(jsonRpcClient);
    }

    public void arresta() {
        //Chiudo tutte le connessioni aperte
        canale.chiudi();
    }
    
}
