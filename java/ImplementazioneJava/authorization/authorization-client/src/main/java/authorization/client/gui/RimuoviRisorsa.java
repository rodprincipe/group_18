package authorization.client.gui;

import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import authorization.client.core.ServitoreClient;
import authorization.shared.dati.TabellaCompleta;
import net.miginfocom.swing.MigLayout;

public class RimuoviRisorsa extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField txtIdRis;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    RimuoviRisorsa frame = new RimuoviRisorsa();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public RimuoviRisorsa() {
        setTitle("Rimuovi risorsa");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "[grow][][][grow]", "[grow][][][grow][][grow]"));
        
        JLabel lblIdRisorsa = new JLabel("Id Risorsa");
        contentPane.add(lblIdRisorsa, "cell 1 1,alignx trailing");
        
        txtIdRis = new JTextField();
        txtIdRis.setText("idRis");
        contentPane.add(txtIdRis, "cell 2 1,alignx left");
        txtIdRis.setColumns(10);
        
        JButton btnVediRisorse = new JButton("Vedi risorse");
        btnVediRisorse.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Point p = getLocation();
                p.translate(30, 30);
                TabellaCompleta tabellaValori = ServitoreClient.getInstance().tutteRisorse();
                ShowTabella frame = new ShowTabella("Risorse: selezionare identificativo risorsa", tabellaValori, txtIdRis);
                frame.setLocation(p);
                frame.setVisible(true);
            }
        });
        contentPane.add(btnVediRisorse, "cell 2 2");
        
        JButton btnRimuovi = new JButton("Rimuovi");
        btnRimuovi.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String idRis = txtIdRis.getText();
                if(idRis.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Non lasciare parametri vuoti");
                }
                else {
                            ServitoreClient.getInstance().rimuoviRisorsa(idRis);
                }
            }
        });
        contentPane.add(btnRimuovi, "cell 2 4");
    }
}
