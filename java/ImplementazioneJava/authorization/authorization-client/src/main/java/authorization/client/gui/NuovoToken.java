package authorization.client.gui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import authorization.client.core.ServitoreClient;
import authorization.shared.dati.TabellaCompleta;
import authorization.shared.dati.Token;
import jsonRpc.client.eccezioni.JsonRpcException;
import net.miginfocom.swing.MigLayout;

public class NuovoToken extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField txtIdRisorsa;
    private JButton btnCrea;
    private JTextPane textChiave;
    private JLabel lblChiaveGenerata;
    private JTextField txtChiave;
    private JButton btnVediAutorizzazioni;
    private JButton btnVediRisorse;
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    NuovoToken frame = new NuovoToken();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the FrameworkSrv.
     */
    public NuovoToken() {
        setTitle("Nuovo token");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 652, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "[33%:n,grow][33%:n,grow][20%:n,grow]", "[grow][][grow][][][grow][][][][grow]"));


        JLabel lblTitolo = new JLabel("Riempi i seguenti campi");
        contentPane.add(lblTitolo, "cell 0 1 2 1,alignx center");

        JLabel lblChiave = new JLabel("Chiave identificativa dell'autorizzazione");
        contentPane.add(lblChiave, "cell 0 3,alignx right");

        txtChiave = new JTextField();
        txtChiave.setMinimumSize(new Dimension(200, 22));
        txtChiave.setText("");
        contentPane.add(txtChiave, "cell 1 3,alignx left");
        //        txtUser.setColumns(10);

        btnVediAutorizzazioni = new JButton("Vedi Autorizzazioni");
        btnVediAutorizzazioni.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                Point p = getLocation();
                p.translate(30, 30);
                TabellaCompleta tabellaValori = ServitoreClient.getInstance().tutteAutorizzazioni();
                ShowTabella frame = new ShowTabella("Autorizzazioni: selezionare identificativo chiave", tabellaValori, txtChiave);
                frame.setLocation(p);
                frame.setVisible(true);
            }
        });
        contentPane.add(btnVediAutorizzazioni, "cell 2 3,growx");

        JLabel lblIdRisorsa = new JLabel("Identificativo della risorsa");
        contentPane.add(lblIdRisorsa, "cell 0 4,alignx right");

        txtIdRisorsa = new JTextField();
        txtIdRisorsa.setMinimumSize(new Dimension(200, 22));
        contentPane.add(txtIdRisorsa, "cell 1 4,alignx left");
        //        txtIdRisorsa.setColumns(10);

        btnCrea = new JButton("Crea");
        btnCrea.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                Token t = null;
                String chiave = txtChiave.getText();
                String idRis = txtIdRisorsa.getText();
                if(chiave.isEmpty() || idRis.isEmpty())
                    JOptionPane.showMessageDialog(null, "Non lasciare parametri vuoti");
                else {
                    try {

                        t = ServitoreClient.getInstance().generaToken(chiave, idRis);
                        lblChiaveGenerata.setVisible(true);
                        textChiave.setVisible(true);
                        textChiave.setText(t.getIdToken());
                    }catch (JsonRpcException e) {
                        JOptionPane.showMessageDialog(null, e.getMessaggioErrore().getMessaggio());
                    }
                }

            }
        });

        btnVediRisorse = new JButton("Vedi Risorse");
        btnVediRisorse.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Point p = getLocation();
                p.translate(30, 30);
                TabellaCompleta tabellaValori = ServitoreClient.getInstance().tutteRisorse();
                ShowTabella frame = new ShowTabella("Risorse: selezionare identificativo risorsa", tabellaValori, txtIdRisorsa);
                frame.setLocation(p);
                frame.setVisible(true);
            }
        });
        contentPane.add(btnVediRisorse, "cell 2 4,growx");
        contentPane.add(btnCrea, "cell 1 6,alignx center");

        lblChiaveGenerata = new JLabel("Token generato (ctrl+c per copiare)");
        lblChiaveGenerata.setVisible(false);
        contentPane.add(lblChiaveGenerata, "cell 0 7,alignx left,aligny bottom");

        textChiave = new JTextPane();
        textChiave.setVisible(false);
        textChiave.setEditable(false);
        contentPane.add(textChiave, "cell 0 8 3 1,growx,aligny bottom");
    }
}
