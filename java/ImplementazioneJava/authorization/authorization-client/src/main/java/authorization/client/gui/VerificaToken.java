package authorization.client.gui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import authorization.client.core.ServitoreClient;
import authorization.shared.dati.TabellaCompleta;
import authorization.shared.dati.Token;
import jsonRpc.client.eccezioni.JsonRpcException;
import net.miginfocom.swing.MigLayout;

public class VerificaToken extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField txtIdToken;
    private JButton btnVerifica;
    private JTextPane textTempoRim;
    private JLabel lblTempoRim;
    private JButton btnVediToken;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    VerificaToken frame = new VerificaToken();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the FrameworkSrv.
     */
    public VerificaToken() {
        setTitle("Verifica tempo di validita' rimasto al token");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 652, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "[33%:n,grow][33%:n,grow][20%:n,grow]", "[grow][][grow][][][grow][][][][grow]"));


        JLabel lblTitolo = new JLabel("Riempi i seguenti campi");
        contentPane.add(lblTitolo, "cell 0 1 2 1,alignx center");

        JLabel lblIdToken = new JLabel("Identificativo del token da verificare");
        contentPane.add(lblIdToken, "cell 0 4,alignx left");

        btnVerifica = new JButton("Verifica");
        btnVerifica.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                String idToken = txtIdToken.getText();
                if(idToken.isEmpty()){
                    JOptionPane.showMessageDialog(null, "ID token vuoto");
                }else {

                    String durataStringa = null;
                    try {
                        durataStringa = ServitoreClient.getInstance().verificaToken(new Token(idToken, null, null, null));
                        lblTempoRim.setVisible(true);
                        textTempoRim.setVisible(true);
                        Duration d = Duration.parse(durataStringa);
                        long ore = d.toHours();
                        d = d.minus(ore, ChronoUnit.HOURS);
                        long min = d.toMinutes();
                        d = d.minus(min, ChronoUnit.MINUTES);
                        long sec = d.getSeconds();
                        textTempoRim.setText(ore + "H " + min + "M " + sec + "S rimanenti");
                    }catch (JsonRpcException e) {
                        JOptionPane.showMessageDialog(null, e.getMessaggioErrore().getMessaggio());
                    }
                }
            }
        });

        btnVediToken = new JButton("Vedi Token");
        btnVediToken.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Point p = getLocation();
                p.translate(30, 30);
                TabellaCompleta tabellaValori = ServitoreClient.getInstance().tuttiToken();
                ShowTabella frame = new ShowTabella("Tokens: selezionare identificativo token", tabellaValori, txtIdToken);
                frame.setLocation(p);
                frame.setVisible(true);
            }
        });
        contentPane.add(btnVediToken, "cell 2 4,growx");

        txtIdToken = new JTextField();
        txtIdToken.setMinimumSize(new Dimension(200, 22));
        contentPane.add(txtIdToken, "cell 0 5 2 1,growx");
        contentPane.add(btnVerifica, "cell 1 6,alignx center");

        lblTempoRim = new JLabel("Tempo rimasto al token");
        lblTempoRim.setVisible(false);
        contentPane.add(lblTempoRim, "cell 0 7,alignx left,aligny bottom");

        textTempoRim = new JTextPane();
        textTempoRim.setVisible(false);
        textTempoRim.setEditable(false);
        contentPane.add(textTempoRim, "cell 0 8 3 1,growx,aligny bottom");
    }

}
