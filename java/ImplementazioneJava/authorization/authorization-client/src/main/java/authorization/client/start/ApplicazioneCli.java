package authorization.client.start;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import authorization.client.core.ClientCore;
import authorization.client.gui.ClientGui;

/**
 * Classe per avviare l'applicazione client
 * @author Rodrigo
 *
 */
public class ApplicazioneCli {
    private final ClientCore core;
    private final ClientGui gui;


    /**
     * Lancia applicazione lato client.
     */
    public static void main(String[] args) {
        ApplicazioneCli app = new ApplicazioneCli();
        app.avvia();
    }

    /**
     * Costruttore per la applicazione.
     */
    public ApplicazioneCli() {
        String ip = (String)JOptionPane.showInputDialog("Inserisci indirizzo","localhost");
        if(ip==null)
        	System.exit(0);
        ip = ip.replaceAll(" ", "");
        if(ip.isEmpty()) {
        	JOptionPane.showMessageDialog(null, "Indirizzo vuoto");
        	System.exit(0);
        }
        core = new ClientCore(ip);
        gui = new ClientGui();
    }

    /**
     * Avvia la applicazione.
     */
    public void avvia() {
        inizializzaCore();
        inizializzaGui();
    }

    /**
     * Inizializza la grafica del sistema.
     */
    private void inizializzaGui() {
        gui.addWindowListener(exit);
        gui.setVisible(true);
    }

    /**
     * Inizializza il nucleo operativo del sistema.
     * @throws SQLException 
     */
    private void inizializzaCore() {
        core.avvia();
    }

    /**
     * Termina il nucleo del sistema
     */
    private void terminaCore() {
        core.arresta();
    }

    /**
     * Contenitore del evento di chiusura
     */
    private final WindowAdapter exit = new WindowAdapter() {
        public void windowClosing(WindowEvent e)
        {
            JFrame frame = (JFrame)e.getSource();
            //Dialog di chiusura applicazione. 
            int result = JOptionPane.showConfirmDialog(
                    frame,
                    "Sei sicuro di voler chiudere l'applicazione?\n"
                    + "Le eventuali richieste non spedite verranno perse.",
                    "Chiusura applicazione",
                    JOptionPane.YES_NO_OPTION);

            //Se la chiusura viene confermata lancio la procedura di terminazione.
            if (result == JOptionPane.YES_OPTION) {
                terminaCore();
                gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        }
    };

}
