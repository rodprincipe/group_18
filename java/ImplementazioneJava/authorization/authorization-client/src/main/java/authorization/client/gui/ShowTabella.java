package authorization.client.gui;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import authorization.shared.dati.TabellaCompleta;
import net.miginfocom.swing.MigLayout;

public class ShowTabella extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;

    /**
     * Create the FrameworkSrv.
     * @param txtUser 
     * @throws SQLException 
     */
    public ShowTabella(String titolo, TabellaCompleta tabellaValori, final JTextField txtField) {
        setTitle(titolo);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 850, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "[grow]", "[grow]"));
        TabellaCompletaJTable tabellaStrutturata = new TabellaCompletaJTable(tabellaValori);
        tabellaStrutturata.addMouseListener(new MouseAdapter() {

            public void mouseClicked(MouseEvent e) {
                
                JTable tabella = (JTable)e.getSource();
                
                int riga = tabella.rowAtPoint(e.getPoint());
                int col = tabella.columnAtPoint(e.getPoint());

                Object oggSelezionato = tabella.getValueAt(riga, col);

                txtField.setText((String) oggSelezionato);
                
                dispose();
                
            }
        });
        JScrollPane scrollPane = new JScrollPane(tabellaStrutturata);
        contentPane.add(scrollPane, "cell 0 0,grow");
    }
    
    public class TabellaCompletaJTable extends JTable {
        
        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        public TabellaCompletaJTable(String[][] test, String[] columnNames) {
            super(test,columnNames);
        }
        
        public TabellaCompletaJTable(TabellaCompleta tab) {
            super(tab.getRecordTabella(),tab.getNomiColonne());
        }
        
    }

}
