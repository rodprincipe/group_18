package authorization.client.canale;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;
import org.zeromq.ZMQException;

import zmq.ZError;

/**
 * Canale di comunicazione ZeroMQ con approccio a code lato client
 * @author Rodrigo
 *
 */
public class CanaleCliZMQ extends CanaleCli {

	private final ZContext contesto;
	private Socket socket;
	private final String indirizzo;
	
	/**
	 * Ammontare di millisecondi che indica il tempo per il quale i canale attendera' una risposta dal server
	 */
	public static final int TIMEOUT = 1500;

	private static final Logger log = LoggerFactory.getLogger(CanaleCliZMQ.class);

	/**
	 * Crea un nuovo canale di comunicazione ad un determinato indirizzo
	 * @param ip sul quale creare il canale di comunicazione
	 * @param porta sulla quale creare il canale di comunicazione
	 */
	public CanaleCliZMQ(String ip, String porta){
		contesto = new ZContext();
		creaSocket();
		this.indirizzo = "tcp://"+ip+":"+porta;
	}
	
	/**
	 * Crea un nuovo canale di comunicazione sul ip fornito e sulla porta standard 5555
	 * @param ip al quale ci si vuole collegare sulla porta standard
	 */
	public CanaleCliZMQ(String ip) {
		this(ip, "5555");
	}

	/**
	 * Crea un nuovo canale di comunicazione sul ip localhost e sulla porta standard 5555
	 */
	public CanaleCliZMQ() {
		this("localhost","5555");
	}

	private void creaSocket() {
		socket = contesto.createSocket(ZMQ.REQ);
		socket.setSendTimeOut(TIMEOUT);
		socket.setReceiveTimeOut(TIMEOUT);
	}
	
	/**
	 * Apre il canale di comunicazione
	 */
	public void apri() {
		socket.connect(this.indirizzo);
	}

	public String inoltra(String messaggio) throws IOException {

		try {
			return inoltro(messaggio);
		}catch (ZMQException e) {
			if (e.getErrorCode()==ZError.EFSM)
				return reInoltro(messaggio);
			else
				throw e;
		}
	}

	/**
	 * Primo tentativo di invio del messaggio.
	 * @param messaggio messaggio che inoltro
	 * @return risposta come stringa
	 * @throws IOException nel caso in cui il server non risponde entro il {@link #TIMEOUT}
	 */
	private String inoltro(String messaggio) throws IOException {
		socket.send(messaggio);
		log.debug("Canale OUT: "+messaggio);
		
		String risposta = socket.recvStr();
		if(risposta==null && socket.base().errno() == 35)
			throw new IOException("Il server non risponde.");
		log.debug("Canale IN: "+risposta);
		
		return risposta;
	}

	/**
	 * Secondo tentativo di invio del messaggio, dopo aver reimpostato il canale di comunicazione.
	 * @param messaggio messaggio che inoltro
	 * @return risposta come stringa
	 * @throws IOException nel caso in cui il server non risponde entro il {@link #TIMEOUT}
	 * @throws ZMQException per altri errori sul canale
	 */
	private String reInoltro(String messaggio) throws IOException, ZMQException {
		socket.disconnect(indirizzo);
		creaSocket();
		apri();
		return inoltro(messaggio);
	}

	public void chiudi() {
		contesto.destroy();
	}
}