package authorization.client.gui;

import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQException;

import jsonRpc.client.eccezioni.JsonRpcException;
import net.miginfocom.swing.MigLayout;
import zmq.ZError;


public class ClientGui extends JFrame {

    private static final long serialVersionUID = 1L;
    
    private JPanel contentPane;
    
    private static final Logger log = LoggerFactory.getLogger(ClientGui.class);


    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ClientGui frame = new ClientGui();
                    frame.setVisible(true);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Crea il framework.
     */
    public ClientGui() {
        
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            public void uncaughtException(Thread t, Throwable e) {
            	log.error("Eccezione:",e);
            	if (e instanceof ZMQException) {
            		JOptionPane.showMessageDialog(null, "Errore ZMQ; "+ZError.toString(((ZMQException)e).getErrorCode()));
            	}else if(e instanceof JsonRpcException) {
            		JOptionPane.showMessageDialog(null, "Errore JSON-RPC; "+((JsonRpcException)e).getMessaggioErrore().getMessaggio());
                }else {
                	String msg = "<html><center>"+
                				 "Errore<br>"+
                				 e.getMessage()+
                				 "</center></html>";
                    JOptionPane.showMessageDialog(null, msg);
                    JOptionPane.showMessageDialog(null, "Termino l'applicazione.");
                }
            }
        });
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        setBounds(100, 100, 450, 350);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "[grow]", "[][grow][][grow][][grow][][grow][][grow][][grow]"));

        JButton btnNuovaAutorizzazione = new JButton("Nuova Autorizzazione");
        btnNuovaAutorizzazione.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NuovaAutorizzazione frame = new NuovaAutorizzazione();
                Point p = getLocation();
                p.translate(30, 30);
                frame.setLocation(p);
                frame.setVisible(true);
            }
        });
        contentPane.add(btnNuovaAutorizzazione, "cell 0 0,alignx center");

        JButton btnVerificaToken = new JButton("Verifica Token");
        btnVerificaToken.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                VerificaToken frame = new VerificaToken();
                Point p = getLocation();
                p.translate(30, 30);
                frame.setLocation(p);
                frame.setVisible(true);
            }
        });
        contentPane.add(btnVerificaToken, "cell 0 6,alignx center");

        JButton btnNuovoToken = new JButton("Nuovo Token");
        btnNuovoToken.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NuovoToken frame = new NuovoToken();
                Point p = getLocation();
                p.translate(30, 30);
                frame.setLocation(p);
                frame.setVisible(true);
            }
        });
        contentPane.add(btnNuovoToken, "cell 0 2,alignx center");

        JButton btnRevocaAutorizzazione = new JButton("Revoca Autorizzazione");
        btnRevocaAutorizzazione.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                RevocaAutorizzazione frame = new RevocaAutorizzazione();
                Point p = getLocation();
                p.translate(30, 30);
                frame.setLocation(p);
                frame.setVisible(true);
            }
        });
        contentPane.add(btnRevocaAutorizzazione, "cell 0 4,alignx center");

        JButton btnAggiungiRisorsa = new JButton("Aggiungi Risorsa");
        btnAggiungiRisorsa.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AggiungiRisorsa frame = new AggiungiRisorsa();
                Point p = getLocation();
                p.translate(30, 30);
                frame.setLocation(p);
                frame.setVisible(true);
            }
        });
        contentPane.add(btnAggiungiRisorsa, "cell 0 8,alignx center");

        JButton btnEliminaRisorsa = new JButton("Elimina Risorsa");
        btnEliminaRisorsa.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                RimuoviRisorsa frame = new RimuoviRisorsa();
                Point p = getLocation();
                p.translate(30, 30);
                frame.setLocation(p);
                frame.setVisible(true);
            }
        });
        contentPane.add(btnEliminaRisorsa, "cell 0 10,alignx center");

    }

}
