/**
 * 
 */
package authorization.client.core;

import java.sql.Timestamp;
import java.util.UUID;

import authorization.shared.dati.Risorsa;
import authorization.shared.dati.TabellaCompleta;
import authorization.shared.dati.Token;
import authorization.shared.interfacce.Servizi;
import jsonRpc.client.JsonRpcClient;

/**
 * Classe che si occupa di lanciare le chiamate ai vari metodi remoti. Ogni chiamata viene costruita con i parametri opportuni.
 * @author Rodrigo
 *
 */
public class ServitoreClient implements Servizi { 

    private static ServitoreClient instance = null;
    private static JsonRpcClient jsonRpcClient = null;

    private ServitoreClient() {
        super();
    }

    public static ServitoreClient getInstance() {
    	if (jsonRpcClient == null & instance != null)
    		throw new NullPointerException("Manca il client Json Rcp");
        if (instance == null)
            instance = new ServitoreClient();
        return instance;
    }

    public void setJsonRpcClient(JsonRpcClient jsonRpcClient) {
        ServitoreClient.jsonRpcClient = jsonRpcClient;
    }

    public String generaChiave(String user, int livello, Timestamp scadenza){
        return jsonRpcClient.creaRichiesta()
                            .id(UUID.randomUUID().toString())
                            .metodo("generaChiave")
                            .parametro("user",user)
                            .parametro("livello", livello)
                            .parametro("scadenza",scadenza)
                            .ritorna(String.class)
                            .esegui();
    }

    public void revocaAutorizzazione(String chiave) {
        jsonRpcClient.creaNotifica()
                             .metodo("revocaAutorizzazione")
                             .parametro("chiave",chiave)
                             .esegui();
    }

    public Token generaToken(String chiave, String idRisorsa) {
        return jsonRpcClient.creaRichiesta()
                .id(UUID.randomUUID().toString())
                .metodo("generaToken")
                .parametro("chiave",chiave)
                .parametro("idRisorsa", idRisorsa)
                .ritorna(Token.class)
                .esegui();
    }

    public String verificaToken(Token t) {
        return jsonRpcClient.creaRichiesta()
                .id(UUID.randomUUID().toString())
                .metodo("verificaToken")
                .parametro("t",t)
                .ritorna(String.class)
                .esegui();
    }
    
    public TabellaCompleta tutteAutorizzazioni() {
        return jsonRpcClient.creaRichiesta()
                .id(UUID.randomUUID().toString())
                .metodo("tutteAutorizzazioni")
                .ritorna(TabellaCompleta.class)
                .esegui();
    }
    
    public TabellaCompleta tuttiToken() {
        return jsonRpcClient.creaRichiesta()
                .id(UUID.randomUUID().toString())
                .metodo("tuttiToken")
                .ritorna(TabellaCompleta.class)
                .esegui();
    }
    
    public TabellaCompleta tutteRisorse() {
        return jsonRpcClient.creaRichiesta()
                .id(UUID.randomUUID().toString())
                .metodo("tutteRisorse")
                .ritorna(TabellaCompleta.class)
                .esegui();
    }

    public TabellaCompleta tuttiUtenti() {
        return jsonRpcClient.creaRichiesta()
                .id(UUID.randomUUID().toString())
                .metodo("tuttiUtenti")
                .ritorna(TabellaCompleta.class)
                .esegui();
    }

    public boolean aggiungiRisorsa(Risorsa r) {
        return jsonRpcClient.creaRichiesta()
               .id(UUID.randomUUID().toString())
               .metodo("aggiungiRisorsa")
               .parametro("r", r)
               .ritorna(boolean.class)
               .esegui();
    }

    public void rimuoviRisorsa(String idRisorsa) {
        jsonRpcClient.creaNotifica()
                .metodo("rimuoviRisorsa")
                .parametro("idRisorsa", idRisorsa)
                .esegui();
    }
}