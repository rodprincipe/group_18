package authorization.client.canale;

import java.io.IOException;

import jsonRpc.client.Canale;

public abstract class CanaleCli implements Canale {

	/**
	 * Apre il canale di comunicazione lato client
	 */
	public abstract void apri();

	/**
	 * Inoltra sul canale un messaggio
	 * @param messaggio nuovo messaggio da inviare
	 * @return risposta come stringa
	 */
	public abstract String inoltra(String messaggio) throws IOException;

	/**
	 * Chiude il canale di comunicazione lato client
	 */
	public abstract void chiudi();


}