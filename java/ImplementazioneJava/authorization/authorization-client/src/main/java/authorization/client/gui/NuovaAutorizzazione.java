package authorization.client.gui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;

import authorization.client.core.ServitoreClient;
import authorization.shared.dati.TabellaCompleta;
import authorization.shared.utility.CalcolatoreTempo;
import jsonRpc.client.eccezioni.JsonRpcException;
import net.miginfocom.swing.MigLayout;

public class NuovaAutorizzazione extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField txtLivello;
    private JTextField txtScadenza;
    private JButton btnCrea;
    private JTextPane textChiave;
    private JLabel lblChiaveGenerata;
    private JTextField txtUser;
    private JLabel lblFormatoData;
    private JButton btnVediUtenti;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    NuovaAutorizzazione frame = new NuovaAutorizzazione();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the FrameworkSrv.
     */
    public NuovaAutorizzazione() {
        setMinimumSize(new Dimension(600, 350));
        setTitle("Richiedi nuova autorizzazione");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 500, 300);
        contentPane = new JPanel();
        contentPane.setMinimumSize(new Dimension(0, 0));
        contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "[33%:n,grow][33%:n,grow][20%:n,grow]", "[grow][][grow][][][][][grow][][][][grow]"));

        JLabel lblTitolo = new JLabel("Riempi i seguenti campi");
        contentPane.add(lblTitolo, "cell 0 1 2 1,alignx center");

        JLabel lblUser = new JLabel("User");
        contentPane.add(lblUser, "cell 0 3,alignx right");

        txtUser = new JTextField();
        txtUser.setMinimumSize(new Dimension(200, 22));
        txtUser.setText("mariorossi");
        contentPane.add(txtUser, "cell 1 3,alignx left");
        txtUser.setColumns(10);

        btnVediUtenti = new JButton("Vedi Utenti");
        btnVediUtenti.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                Point p = getLocation();
                p.translate(30, 30);
                TabellaCompleta tabellaValori = ServitoreClient.getInstance().tuttiUtenti();
                ShowTabella frame = new ShowTabella("Copia id utente (ctrl+c)",tabellaValori,txtUser);
                frame.setLocation(p);
                frame.setVisible(true);

            }
        });
        contentPane.add(btnVediUtenti, "cell 2 3,alignx center");

        JLabel lblLivello = new JLabel("Livello");
        contentPane.add(lblLivello, "cell 0 4,alignx right");

        txtLivello = new JTextField();
        txtLivello.setMinimumSize(new Dimension(200, 22));
        txtLivello.setText("3");
        contentPane.add(txtLivello, "cell 1 4,alignx left");
        txtLivello.setColumns(10);

        JLabel lblScadenza = new JLabel("Scadenza");
        contentPane.add(lblScadenza, "cell 0 5,alignx right");

        txtScadenza = new JTextField();
        txtScadenza.setMinimumSize(new Dimension(200, 22));
        txtScadenza.setText(CalcolatoreTempo.adesso().toString());
        contentPane.add(txtScadenza, "cell 1 5,alignx left");
        txtScadenza.setColumns(10);

        btnCrea = new JButton("Crea");
        btnCrea.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                String chiave = null;
                if(txtUser.getText().isEmpty())
                    JOptionPane.showMessageDialog(null, "Valore del campo utente vuoto");
                else {
                    try {
                        int livello = Integer.parseInt(txtLivello.getText());
                        Timestamp scadenza = Timestamp.valueOf(txtScadenza.getText());
                        try {
                            chiave = ServitoreClient.getInstance().generaChiave(txtUser.getText(), livello, scadenza);
                            lblChiaveGenerata.setVisible(true);
                            textChiave.setVisible(true);
                            textChiave.setText(chiave);
                        } catch (JsonRpcException e) {
                            JOptionPane.showMessageDialog(null, e.getMessaggioErrore().getMessaggio());
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Valore del campo livello non valido");
                    }catch(IllegalArgumentException e) {
                        JOptionPane.showMessageDialog(null, "Valore del campo data non valido");
                    }
                }
            }
        });

    lblFormatoData = new JLabel("in formato AAAA-MM-GG HH:MM:SS");
    lblFormatoData.setFont(new Font("Tahoma", Font.PLAIN, 10));
    contentPane.add(lblFormatoData, "cell 1 6,alignx left,aligny top");
    contentPane.add(btnCrea, "cell 1 8,alignx center");

    lblChiaveGenerata = new JLabel("Chiave generata (ctrl+c per copiare)");
    lblChiaveGenerata.setVisible(false);
    contentPane.add(lblChiaveGenerata, "cell 0 9,alignx left,aligny bottom");

    textChiave = new JTextPane();
    textChiave.setVisible(false);
    textChiave.setEditable(false);
    contentPane.add(textChiave, "cell 0 10 3 1,growx,aligny bottom");
}

}
