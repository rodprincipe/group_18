package authorization.client.gui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import authorization.client.core.ServitoreClient;
import authorization.shared.dati.Risorsa;
import net.miginfocom.swing.MigLayout;

public class AggiungiRisorsa extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField txtId;
    private JLabel lblLivello;
    private JTextField txtDescr;
    private JTextField txtLiv;
    private JLabel lblIndirizzo;
    private JTextField txtInd;
    private JButton btnAggiungiRisorsa;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    AggiungiRisorsa frame = new AggiungiRisorsa();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public AggiungiRisorsa() {
        setTitle("Aggiungi risorsa");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "[grow][grow][grow][grow]", "[grow][][][][][grow][][grow]"));

        JLabel lblIdRisorsa = new JLabel("Id risorsa");
        contentPane.add(lblIdRisorsa, "cell 1 1,alignx trailing");

        txtId = new JTextField();
        contentPane.add(txtId, "cell 2 1,growx");
        txtId.setColumns(10);

        JLabel lblDescrizione = new JLabel("Descrizione");
        contentPane.add(lblDescrizione, "cell 1 2,alignx trailing");

        txtDescr = new JTextField();
        contentPane.add(txtDescr, "cell 2 2,growx");
        txtDescr.setColumns(10);

        lblLivello = new JLabel("Livello");
        contentPane.add(lblLivello, "cell 1 3,alignx trailing");

        txtLiv = new JTextField();
        contentPane.add(txtLiv, "cell 2 3,growx");
        txtLiv.setColumns(10);

        lblIndirizzo = new JLabel("Indirizzo");
        contentPane.add(lblIndirizzo, "cell 1 4,alignx right");

        txtInd = new JTextField();
        contentPane.add(txtInd, "cell 2 4,growx");
        txtInd.setColumns(10);

        btnAggiungiRisorsa = new JButton("Aggiungi risorsa");
        btnAggiungiRisorsa.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String id = txtId.getText();
                String descr = txtDescr.getText();
                String livString = txtLiv.getText();
                String ind = txtInd.getText();
                if(id.isEmpty() || descr.isEmpty() || livString.isEmpty())
                    JOptionPane.showMessageDialog(null,"Non lasciare parametri vuoti");
                else {
                    try {
                        int liv = Integer.parseInt(livString);
                        Risorsa r = new Risorsa(id,descr,liv,ind);
                        boolean aggiunta = ServitoreClient.getInstance().aggiungiRisorsa(r);
                        if(aggiunta)
                            JOptionPane.showMessageDialog(null,"Aggiunta");
                        else
                            JOptionPane.showMessageDialog(null,"Non aggiunta");
                            
                    }catch (NumberFormatException ee) {
                        JOptionPane.showMessageDialog(null,"Livello non valido");
                    }
                }
            }
        });
        contentPane.add(btnAggiungiRisorsa, "cell 2 6,alignx right");
    }

}
