package authorization.client.gui;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import authorization.client.core.ServitoreClient;
import authorization.shared.dati.TabellaCompleta;
import jsonRpc.client.eccezioni.JsonRpcException;
import net.miginfocom.swing.MigLayout;

public class RevocaAutorizzazione extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField txtChiave;
    private JButton btnRevoca;
    private JButton btnVediAutorizzazioni;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    RevocaAutorizzazione frame = new RevocaAutorizzazione();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the FrameworkSrv.
     */
    public RevocaAutorizzazione() {

        setTitle("Revoca autorizzazione");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 652, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(new MigLayout("", "[33%:n,grow][33%:n,grow][20%:n,grow]", "[grow][][grow][][][grow][][][][grow]"));


        JLabel lblTitolo = new JLabel("Riempi i seguenti campi");
        contentPane.add(lblTitolo, "cell 0 1 2 1,alignx center");

        JLabel lblChiave = new JLabel("Chiave dell'autorizzazione da revocare");
        contentPane.add(lblChiave, "cell 0 4,alignx right");

        txtChiave = new JTextField();
        txtChiave.setMinimumSize(new Dimension(200, 22));
        contentPane.add(txtChiave, "cell 1 4,alignx left");

        btnRevoca = new JButton("Revoca");
        btnRevoca.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                String chiave = txtChiave.getText();
                if(chiave.isEmpty())
                    JOptionPane.showMessageDialog(null, "Chiave vuota");
                else {
                    try {
                        ServitoreClient.getInstance().revocaAutorizzazione(chiave);
                    }catch (JsonRpcException e) {
                        JOptionPane.showMessageDialog(null, e.getMessaggioErrore().getMessaggio());
                    }

                } 
            }
        });
        contentPane.add(btnRevoca, "cell 1 7,alignx center");

        btnVediAutorizzazioni = new JButton("Vedi Autorizzazioni");
        btnVediAutorizzazioni.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                TabellaCompleta tabellaValori = ServitoreClient.getInstance().tutteAutorizzazioni();
                ShowTabella frame = new ShowTabella("Autorizzazioni: selezionare identificativo autorizzazione", tabellaValori, txtChiave);
                frame.setVisible(true);
            }
        });
        contentPane.add(btnVediAutorizzazioni, "cell 2 4,growx");
    }

}
