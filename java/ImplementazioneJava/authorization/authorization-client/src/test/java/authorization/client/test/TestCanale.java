package authorization.client.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;
import org.zeromq.ZMQException;

import authorization.client.canale.CanaleCli;
import authorization.client.canale.CanaleCliZMQ;
import jsonRpc.client.Canale;
import zmq.ZError;

public class TestCanale {

	private Thread thread;
	private ZContext context;
	private int tipoSrv;
	
	private String ip = "localhost";
	private String porta = "5556";

	/**
	 * Simulo un server che risponde sulla porta 5556
	 */
	@Before
	public void srv() {
		thread = new Thread(new Runnable() {

			@Override
			public void run() {
				context = new ZContext();
				Socket socket = context.createSocket(ZMQ.REP);
				socket.bind("tcp://*:"+porta);
				try {
					String messaggio;
					switch (tipoSrv) {
					case 1:
						//Server che risponde entro il timeout del client concatenando la parola 'ricevuto' al messaggio ricevuto
						messaggio = socket.recvStr();
						socket.send(messaggio+" ricevuto");
						break;
					case 2:
						//Server che non risponde entro il timeout del client
						messaggio = socket.recvStr();
						break;
					case 3:
						//Server che non risponde entro il timeout del client alla prima e alla seconda richiesta
						messaggio = socket.recvStr();
						break;
					}
				}catch(ZMQException e) {
					assertEquals(e.getErrorCode(),ZError.ETERM);
				}
			}
		});
		thread.start();
	}

	@After
	public void killSrv() {
		context.destroy();
		thread.interrupt();
	}

	/**
	 * Creo la connessione invio un messaggio e mi aspetto una risposta non nulla
	 * @throws IOException nel caso di errore sul canale
	 */
	@Test
	public void msgSiRisp() throws IOException {
		tipoSrv = 1;
		Canale c = new CanaleCliZMQ(ip,porta);
		String richiesta = "messaggio";
		String risposta = c.inoltra(richiesta);
		if(risposta==null)
			throw new NullPointerException();
		assertEquals(risposta, richiesta+" ricevuto");
		((CanaleCli)c).chiudi();
	}

	
	/**
	 * Creo la connessione invio un messaggio e mi apetto che si verifichi un errore perche' il server non risponde
	 * @throws IOException nel caso di errore sul canale, e inquesto caso il server non risponde
	 */
	@Test(expected=IOException.class)
	public void msgNoRisp() throws IOException {
		tipoSrv = 2;
		Canale chClient = new CanaleCliZMQ(ip,porta); 
		String richiesta = "messaggio";
		String risposta = chClient.inoltra(richiesta);
		if(risposta==null)
			throw new NullPointerException();
		assertEquals(risposta, richiesta+" ricevuto");
		((CanaleCli)chClient).chiudi();
	}
	
	/**
	 * Creo la connessione invio un messaggio e mi apetto che si verifichi un errore perche' il server non risponde,
	 * quindi reinvio lo stesso messaggio e otterro' lo stesso errore
	 * @throws IOException nel caso di errore sul canale, e inquesto caso il server non risponde
	 */
	@Test(expected=IOException.class)
	public void doppioInoltro() throws IOException {
		tipoSrv = 3;
		Canale chClient = new CanaleCliZMQ(ip,porta); 
		String richiesta = "messaggio";
		try{
			String risposta = chClient.inoltra(richiesta);
			if(risposta==null)
				throw new NullPointerException();
		}catch(IOException e) {
			//Provo a inoltrare nuovamente il messaggio anche se a priori so che il server non rispondera'
			chClient.inoltra(richiesta);
		}
	}
}
