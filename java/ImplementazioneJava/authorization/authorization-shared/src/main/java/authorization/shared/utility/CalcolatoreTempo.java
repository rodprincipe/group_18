/**
 * 
 */
package authorization.shared.utility;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;

/**
 * Classe di utility per gestire data e ora
 * @author Rodrigo
 *
 */
public class CalcolatoreTempo {

    /**
     * Data e ora di adesso come timestamp
     * @return data e ora in questo istante
     */
    public static Timestamp adesso(){
        return Timestamp.valueOf(LocalDateTime.now());
    }
    
    /**
     * Data e ora di adesso piu un tot di ore come timestamp
     * @param ore intero che indica quante ore aggiungere
     * @return data e ora in questo istante piu' un tot di ore
     */
    public static Timestamp adessoPiu(int ore){
        return Timestamp.valueOf(LocalDateTime.now().plusHours(ore));
    }
    
    /**
     * Data e ora di adesso meno un tot di ore come timestamp
     * @param ore intero che indica quante ore sotrarre
     * @return data e ora in questo istante piu' un tot di ore
     */
    public static Timestamp adessoMeno(int ore) {
        return Timestamp.valueOf(LocalDateTime.now().minusHours(ore));
    }

    /**
     * Restituisce la quantita' di tempo rimanente tra il presente e una data di controllo, data
     * da una data di riferimento piu' l'offset definito (dataConrollo = dataRiferimento + giorniOffset)
     * @param dataRiferimento data che tengo come riferimento iniziale per calcolare la data di controllo  
     * @param giorniOffset numero di giorni di scostamento dalla data di rifermento
     * @return quantita' di tempo positiva se 
     */
    public static Duration tempoRimanente(Timestamp dataRiferimento, int giorniOffset){
        Instant adesso = Instant.now();
        Instant dataControllo = dataRiferimento.toInstant().plus(Duration.ofDays(giorniOffset));
        return Duration.between(adesso, dataControllo);
    }

}
