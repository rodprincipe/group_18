package authorization.shared.dati;
/**
 * 
 */

import java.sql.Timestamp;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

/**
 * Questa classe contiene tutte le info riguardo ad uno specifico token.
 * 
 * @author Rodrigo
 *
 */
public class Token {

    @JsonProperty
    private String idToken;
    @JsonProperty
    private final Timestamp dataGenerazione;
    @JsonProperty
    private final String chiave;
    @JsonProperty
    private final String idRisorsa;
    
    /**
     * Permette di istanziare un nuovo token. Senza assegnare un id, il quale verr� generato casualmente.
     * 
     * @param idToken
     *            - una stringa contenente l'id del token
     * @param dataGenerazione
     *            - un Timestamp riferito alla data di ceazione
     * @param chiave
     *            - una stringa contenente la chiave dell'autorizzazione alla quale viene concesso il token
     *            assegnato il token
     * @param idRisorsa
     *            - una stringa contenente l'idrisorsa della risorsa a cui viene
     *            attribuito il token
     */
    @JsonCreator
    public Token( 
            @JsonProperty("dataGenerazione") Timestamp dataGenerazione, 
            @JsonProperty("chiave") String chiave, 
            @JsonProperty("idRisorsa") String idRisorsa) {
        this.idToken = UUID.randomUUID().toString();
        this.dataGenerazione = dataGenerazione;
        this.chiave = chiave;
        this.idRisorsa = idRisorsa;
    }

    /**
     * Permette di istanziare un nuovo token con un determinato id.
     * 
     * @param idToken
     *            - una stringa contenente l'id del token
     * @param dataGenerazione
     *            - un Timestamp riferito alla data di ceazione
     * @param chiave
     *            - una stringa contenente l'username del utente a cui viene
     *            assegnato il token
     * @param idRisorsa
     *            - una stringa contenente l'idrisorsa della risorsa a cui viene
     *            attribuito il token
     */
    @JsonCreator
    public Token(
            @JsonProperty("idToken") 
            String idToken, 
            @JsonProperty("dataGenerazione") 
            Timestamp dataGenerazione, 
            @JsonProperty("chiave") 
            String chiave, 
            @JsonProperty("idRisorsa") 
            String idRisorsa) {
        this.idToken = idToken;
        this.dataGenerazione = dataGenerazione;
        this.chiave = chiave;
        this.idRisorsa = idRisorsa;
    }


    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public Timestamp getDataGenerazione() {
        return dataGenerazione;
    }

    public String getChiave() {
        return chiave;
    }

    public String getIdRisorsa() {
        return idRisorsa;
    }
    
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("idToken", idRisorsa)
                .add("dataGenerazione", dataGenerazione)
                .add("chiave", chiave)
                .add("idRisorsa", idRisorsa)
                .toString();
    }
    
}
