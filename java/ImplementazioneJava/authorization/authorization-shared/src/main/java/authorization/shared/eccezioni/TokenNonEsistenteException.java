/**
 * 
 */
package authorization.shared.eccezioni;

import jsonRpc.core.messaggi.ErroreApplicazione;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Eccezione che viene lanciata nel momento in cui si cerca di utilizzare un token e questo non esiste.
 * @author Rodrigo
 *
 */
public class TokenNonEsistenteException extends Exception implements ErroreApplicazione{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public TokenNonEsistenteException() {
        super("Token NON esistente.");
    }

    /**
     * @param message
     */
    public TokenNonEsistenteException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public TokenNonEsistenteException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public TokenNonEsistenteException(Throwable cause) {
        super(cause);
    }

    @Override
    public ErroreJsonRpc getErrore() {
        return new ErroreJsonRpc(-32400, getMessage());
    }

}
