/**
 * 
 */
package authorization.shared.eccezioni;

import jsonRpc.core.messaggi.ErroreApplicazione;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Eccezione che viene lanciata nel momento in cui si cerca di utilizzare una risorsa e non esiste.
 * @author Rodrigo
 *
 */
public class RisorsaNonEsistenteException extends Exception implements ErroreApplicazione {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public RisorsaNonEsistenteException() {
        super("Risorsa NON esistente.");
    }

    /**
     * @param message
     */
    public RisorsaNonEsistenteException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public RisorsaNonEsistenteException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public RisorsaNonEsistenteException(Throwable cause) {
        super(cause);
    }

    @Override
    public ErroreJsonRpc getErrore() {
        return new ErroreJsonRpc(-32201, getMessage());
    }
}
