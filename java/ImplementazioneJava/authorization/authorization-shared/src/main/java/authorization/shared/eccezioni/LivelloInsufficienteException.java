/**
 * 
 */
package authorization.shared.eccezioni;

import jsonRpc.core.messaggi.ErroreApplicazione;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Eccezione che si verifica nel momento in cui il livello dell'autorizzazione � insufficente per acceedere ad una determinata risorsa.
 * @author Rodrigo
 *
 */

public class LivelloInsufficienteException extends Exception implements ErroreApplicazione {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public LivelloInsufficienteException() {
        super("Autorizzazione con livello insufficente.");
    }

    /**
     * @param message
     */
    public LivelloInsufficienteException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public LivelloInsufficienteException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * @param message
     * @param cause
     */
    public LivelloInsufficienteException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public LivelloInsufficienteException(Throwable cause) {
        super(cause);
    }

    @Override
    public ErroreJsonRpc getErrore() {
        return new ErroreJsonRpc(-32501, getMessage());
    }
}
