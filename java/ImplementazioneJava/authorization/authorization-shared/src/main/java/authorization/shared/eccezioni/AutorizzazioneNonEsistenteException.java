/**
 * 
 */
package authorization.shared.eccezioni;

import jsonRpc.core.messaggi.ErroreApplicazione;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Eccezione che viene lanciata nel momento in cui si cerca di utilizzare un'autorizzazione e questa non esiste.
 * @author Rodrigo
 *
 */
public class AutorizzazioneNonEsistenteException extends Exception implements ErroreApplicazione{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public AutorizzazioneNonEsistenteException() {
        super("Autorizzazione NON esistente.");
    }

    /**
     * @param message
     */
    public AutorizzazioneNonEsistenteException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public AutorizzazioneNonEsistenteException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * @param message
     * @param cause
     */
    public AutorizzazioneNonEsistenteException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public AutorizzazioneNonEsistenteException(Throwable cause) {
        super(cause);
    }

    @Override
    public ErroreJsonRpc getErrore() {
        return new ErroreJsonRpc(-32301, getMessage());
    }

}
