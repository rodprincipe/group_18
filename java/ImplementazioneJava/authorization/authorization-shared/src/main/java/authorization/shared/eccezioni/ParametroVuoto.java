package authorization.shared.eccezioni;

import jsonRpc.core.messaggi.ErroreApplicazione;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Eccezione che si verifica nel momento in cui viene lasciato vuoto almeno un parametro in una chiamata remota
 * @author Rodrigo
 *
 */
public class ParametroVuoto extends Exception implements ErroreApplicazione {

    private static final long serialVersionUID = 1L;

    public ParametroVuoto() {
        super("Riempi tutti i parametri");
    }

    public ParametroVuoto(String message) {
        super(message);
    }

    public ParametroVuoto(Throwable cause) {
        super(cause);
    }

    public ParametroVuoto(String message, Throwable cause) {
        super(message, cause);
    }

    public ParametroVuoto(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    @Override
    public ErroreJsonRpc getErrore() {
        return new ErroreJsonRpc(-32500, getMessage());
    }

}
