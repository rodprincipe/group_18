/**
 * 
 */
package authorization.shared.eccezioni;

import jsonRpc.core.messaggi.ErroreApplicazione;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Eccezione che viene lanciata nel momento in cui si cerca di utilizzare un utente e questo non esiste.
 * @author Rodrigo
 *
 */
public class UtenteNonEsistenteException extends Exception implements ErroreApplicazione {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public UtenteNonEsistenteException() {
        super("Utente NON esistente.");
    }

    /**
     * @param message
     */
    public UtenteNonEsistenteException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public UtenteNonEsistenteException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public UtenteNonEsistenteException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public ErroreJsonRpc getErrore() {
        return new ErroreJsonRpc(-32101, getMessage());
    }

}
