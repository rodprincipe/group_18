package authorization.shared.dati;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;

/**
 * Questa classe contiene tutte le info riguardo ad una risorsa.
 * 
 * @author Rodrigo
 *
 */
public class Risorsa {

    @JsonProperty
    private String idRisorsa;
    @JsonProperty
    private String descrizione;
    @JsonProperty
    private int livello;
    @JsonProperty
    private String indirizzo;

    /**
     * Permette di istanziare un nuova risorsa.
     * 
     * @param idRisorsa
     *            - una stringa contenente l'id della risorsa
     * @param descrizione
     *            - una stringa contenente la descrizione della risorsa
     * @param livello
     *            - un intero contente il valore del livello assegnato alla
     *            risorsa (numero minore equivale a livello maggiore)
     * @param indirizzo
     *            - una stringa contenente l'indirizzo di riferimento alla
     *            risorsa
     */
    @JsonCreator
    public Risorsa(@JsonProperty("idRisorsa") String idRisorsa, @JsonProperty("descrizione") String descrizione, @JsonProperty("livello") int livello, @JsonProperty("indirizzo") String indirizzo) {
        this.idRisorsa = idRisorsa;
        this.descrizione = descrizione;
        this.livello = livello;
        this.indirizzo = indirizzo;
    }

    public String getIdRisorsa() {
        return idRisorsa;
    }

    public void setIdRisorsa(String idRisorsa) {
        this.idRisorsa = idRisorsa;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public int getLivello() {
        return livello;
    }

    public String getIndirizzo() {
        return indirizzo;
    }
    
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("idRisorsa", idRisorsa)
                .add("descrizione", descrizione)
                .add("livello", livello)
                .add("indirizzo", indirizzo)
                .toString();
    }
}
