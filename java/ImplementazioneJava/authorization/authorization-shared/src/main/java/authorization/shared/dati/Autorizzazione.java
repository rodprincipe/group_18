package authorization.shared.dati;

import java.sql.Timestamp;
import java.util.UUID;

import com.google.common.base.MoreObjects;

/**
 * Questa classe contiene tutte le info riguardo ad una specifica
 * autorizzazione.
 * 
 * @author Rodrigo
 *
 */
public class Autorizzazione {

    private String chiave;
    private final int livello;
    private final Timestamp scadenza;
    private final String username;
    private final boolean revocata;

    /**
     * Costruttore completo che permetter di istanziare la classe assegnando un
     * valore a tutti gli attributi utili.
     * 
     * @param chiave
     * @param livello
     * @param scadenza
     * @param username
     */
    public Autorizzazione(String chiave, int livello, boolean revocata, Timestamp scadenza, String username) {
        this.chiave = chiave;
        this.livello = livello;
        this.revocata = revocata;
        this.scadenza = scadenza;
        this.username = username;
    }

    /**
     * Costruttore che permette di istanziare la classe assegnando un valore
     * casuale al'identificatore dell'autorizzazione.
     * 
     * @param livello
     * @param scadenza
     * @param username
     */
    public Autorizzazione(int livello, Timestamp scadenza, String user) {
        this.chiave = UUID.randomUUID().toString();
        this.livello = livello;
        this.revocata = false;
        this.scadenza = scadenza;
        this.username = user;
    }

    public void setChiave(String chiave) {
        this.chiave = chiave;
    }

    /**
     * Ritorna la chiave dell'autorizzazione
     * 
     * @return chiave
     */
    public String getChiave() {
        return chiave;
    }

    /**
     * Ritorna livello dell'autorizzazione
     * 
     * @return livello
     */
    public int getLivello() {
        return livello;
    }

    /**
     * Ritorna scadenza dell'autorizzazione
     * 
     * @return scadenza
     */
    public Timestamp getScadenza() {
        return scadenza;
    }

    /**
     * Ritorna l'utente al quale e' concessa l'autorizzazione
     * 
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Ritorna vero se la autorizzazione risulta revocata, falso altrimenti
     * 
     * @return revocata
     */
    public boolean getRevocata() {
        return revocata;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("chiave", chiave)
                .add("livello", livello)
                .add("scadenza", scadenza)
                .add("username", username)
                .add("revocata", revocata)
                .toString();
    }

}
