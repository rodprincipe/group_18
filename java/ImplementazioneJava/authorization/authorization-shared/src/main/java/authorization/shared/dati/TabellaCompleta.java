/**
 * 
 */
package authorization.shared.dati;

import java.util.Vector;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Rodrigo
 *
 */
public class TabellaCompleta {
    
    @JsonProperty
    private Vector<String> nomiColonne;
    @JsonProperty
    private Vector<Vector<String>> recordTabella;
    
    @JsonCreator
    public TabellaCompleta(@JsonProperty("nomiColonne") Vector<String> nomiColonne,
                           @JsonProperty("recordTabella") Vector<Vector<String>> recordTabella) {
       this.nomiColonne = nomiColonne;
       this.recordTabella = recordTabella;
    }

    public Vector<String> getNomiColonne() {
        return nomiColonne;
    }

    public void setNomiColonne(Vector<String> nomiColonne) {
        this.nomiColonne = nomiColonne;
    }

    public Vector<Vector<String>> getRecordTabella() {
        return recordTabella;
    }

    public void setRecordTabella(Vector<Vector<String>> recordTabella) {
        this.recordTabella = recordTabella;
    }
    
    
}
