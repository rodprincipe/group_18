package authorization.shared.interfacce;

import java.sql.SQLException;
import java.sql.Timestamp;

import org.jetbrains.annotations.NotNull;

import authorization.shared.dati.Risorsa;
import authorization.shared.dati.TabellaCompleta;
import authorization.shared.dati.Token;
import authorization.shared.eccezioni.AutorizzazioneNonEsistenteException;
import authorization.shared.eccezioni.AutorizzazioneRevocataException;
import authorization.shared.eccezioni.AutorizzazioneScadutaException;
import authorization.shared.eccezioni.LivelloInsufficienteException;
import authorization.shared.eccezioni.ParametroVuoto;
import authorization.shared.eccezioni.RisorsaGiaEsistenteException;
import authorization.shared.eccezioni.RisorsaNonEsistenteException;
import authorization.shared.eccezioni.TokenNonEsistenteException;
import authorization.shared.eccezioni.TokenScadutoException;
import authorization.shared.eccezioni.UtenteNonEsistenteException;

public interface Servizi {

    public String generaChiave(@NotNull String user, @NotNull int livello, @NotNull Timestamp scadenza) throws UtenteNonEsistenteException, SQLException, ParametroVuoto;

    public void revocaAutorizzazione(String chiave) throws AutorizzazioneNonEsistenteException, SQLException, ParametroVuoto;

    public Token generaToken(String chiave, String idRisorsa)
            throws AutorizzazioneNonEsistenteException, RisorsaNonEsistenteException, AutorizzazioneScadutaException,
            LivelloInsufficienteException, UtenteNonEsistenteException, SQLException, AutorizzazioneRevocataException, ParametroVuoto;

    public String verificaToken(Token t)
            throws  TokenScadutoException, TokenNonEsistenteException, SQLException, ParametroVuoto;
    
    public boolean aggiungiRisorsa(Risorsa r) throws SQLException, RisorsaGiaEsistenteException;
    
    public void rimuoviRisorsa(String idRisorsa) throws SQLException;

    public TabellaCompleta tutteAutorizzazioni() throws  SQLException;

    public TabellaCompleta tuttiToken() throws SQLException;

    public TabellaCompleta tutteRisorse() throws SQLException;

    public TabellaCompleta tuttiUtenti() throws SQLException;

}