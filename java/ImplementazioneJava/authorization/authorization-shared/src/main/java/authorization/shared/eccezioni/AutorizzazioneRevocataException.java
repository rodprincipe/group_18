/**
 * 
 */
package authorization.shared.eccezioni;

import jsonRpc.core.messaggi.ErroreApplicazione;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Eccezione che si verifica nel momento in cui l'autorizzazione trattata risulta revocata
 * @author Rodrigo
 *
 */
public class AutorizzazioneRevocataException extends Exception implements ErroreApplicazione {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public AutorizzazioneRevocataException() {
        super("Autorizzazione revocata.");
    }

    /**
     * @param message
     */
    public AutorizzazioneRevocataException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public AutorizzazioneRevocataException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * @param message
     * @param cause
     */
    public AutorizzazioneRevocataException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public AutorizzazioneRevocataException(Throwable cause) {
        super(cause);
    }

    @Override
    public ErroreJsonRpc getErrore() {
        return new ErroreJsonRpc(-32303, getMessage());
    }

}
