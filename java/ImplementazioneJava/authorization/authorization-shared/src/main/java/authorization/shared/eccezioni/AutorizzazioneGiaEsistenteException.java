/**
 * 
 */
package authorization.shared.eccezioni;

import jsonRpc.core.messaggi.ErroreApplicazione;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Eccezione lanciata nel momento in cui si tenta di inserire un'autorizzazione nel db ed esiste gi� un'autorizzazione con la stessa chiave.
 * @author Rodrigo
 *
 */
public class AutorizzazioneGiaEsistenteException extends Exception implements ErroreApplicazione{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public AutorizzazioneGiaEsistenteException() {
        super("Autorizzazione gia' esistente");
    }

    /**
     * @param message
     */
    public AutorizzazioneGiaEsistenteException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public AutorizzazioneGiaEsistenteException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public AutorizzazioneGiaEsistenteException(Throwable cause) {
        super(cause);
    }

    @Override
    public ErroreJsonRpc getErrore() {
        return new ErroreJsonRpc(-32300, getMessage());
    }

}
