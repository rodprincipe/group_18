/**
 * 
 */
package authorization.shared.eccezioni;

import jsonRpc.core.messaggi.ErroreApplicazione;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Eccezione che viene lanciata nel momento in cui viene aggiunta una risorsa e l'identificativo che si cerca di utilizzare e' gia' occupato.
 * @author Rodrigo
 *
 */
public class RisorsaGiaEsistenteException extends Exception implements ErroreApplicazione{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public RisorsaGiaEsistenteException() {
        super("Risorsa gia' esistente.");
    }

    /**
     * @param message
     */
    public RisorsaGiaEsistenteException(String message) {
        super(message);
    }

    /**
     * @param cause
     */
    public RisorsaGiaEsistenteException(Throwable cause) {
        super(cause);
    }

    /**
     * @param message
     * @param cause
     */
    public RisorsaGiaEsistenteException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public ErroreJsonRpc getErrore() {
        return new ErroreJsonRpc(-32200, getMessage());
    }

}
