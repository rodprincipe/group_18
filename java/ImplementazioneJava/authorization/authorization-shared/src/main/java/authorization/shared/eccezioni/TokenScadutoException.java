/**
 * 
 */
package authorization.shared.eccezioni;

import jsonRpc.core.messaggi.ErroreApplicazione;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Eccezione che viene lanciata nel momento in cui viene ricercato un token e' scaduto.
 * @author Rodrigo
 *
 */
public class TokenScadutoException extends Exception implements ErroreApplicazione{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public TokenScadutoException() {
        super("Token scaduto.");
    }

    /**
     * @param message
     */
    public TokenScadutoException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public TokenScadutoException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public TokenScadutoException(Throwable cause) {
        super(cause);
    }

    @Override
    public ErroreJsonRpc getErrore() {
        return new ErroreJsonRpc(-32401, getMessage());
    }

}
