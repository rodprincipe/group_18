/**
 * 
 */
package authorization.shared.eccezioni;

import jsonRpc.core.messaggi.ErroreApplicazione;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Eccezione che si verifica nel momento in cui si tenta di inserire un'autorizzazione nel db ed esiste gi� un'autorizzazione con la stessa chiave.
 * @author Rodrigo
 *
 */
public class AutorizzazioneScadutaException extends Exception implements ErroreApplicazione {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public AutorizzazioneScadutaException() {
        super("Autorizzazione scaduta.");
    }

    /**
     * @param message
     */
    public AutorizzazioneScadutaException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public AutorizzazioneScadutaException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * @param message
     * @param cause
     */
    public AutorizzazioneScadutaException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public AutorizzazioneScadutaException(Throwable cause) {
        super(cause);
    }

    @Override
    public ErroreJsonRpc getErrore() {
        return new ErroreJsonRpc(-32303, getMessage());
    }

}
