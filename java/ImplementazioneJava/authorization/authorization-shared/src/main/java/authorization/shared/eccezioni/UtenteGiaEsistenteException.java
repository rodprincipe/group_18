/**
 * 
 */
package authorization.shared.eccezioni;

import jsonRpc.core.messaggi.ErroreApplicazione;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Eccezione che si verifica nel momento in cui si tenta di inserire un utente nel db ed esiste gi� un utente con lo stesso username.
 * @author Rodrigo
 *
 */
public class UtenteGiaEsistenteException extends Exception implements ErroreApplicazione {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public UtenteGiaEsistenteException() {
        super("Utente gia' esistente.");
    }

    /**
     * @param message
     */
    public UtenteGiaEsistenteException(String message) {
        super(message);
    }

    /**
     * @param message
     * @param cause
     */
    public UtenteGiaEsistenteException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @param cause
     */
    public UtenteGiaEsistenteException(Throwable cause) {
        super(cause);
    }

    @Override
    public ErroreJsonRpc getErrore() {
        return new ErroreJsonRpc(-32100, getMessage());
    }

}
