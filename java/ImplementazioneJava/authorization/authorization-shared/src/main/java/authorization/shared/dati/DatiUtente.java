package authorization.shared.dati;

import com.google.common.base.MoreObjects;

/**
 * Questa classe contiene tutte le info riguardanti un utente.
 * 
 * @author Rodrigo
 *
 */
public class DatiUtente {

    private String username;
    private String nome;
    private String cognome;
    private String email;
    private String password = null;

    /**
     * Costruttore completo che permette di istanziare la classe assegnando un
     * valore a tutti gli attributi.
     * 
     * @param username
     * @param nome
     * @param cognome
     * @param email
     * @param password
     */
    public DatiUtente(String username, String nome, String cognome, String email, String password) {
        this.username = username;
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
        this.password = password;
    }

    /**
     * Costruttore che permette istanziare la classe senza dover assegnare un
     * valore al attributo password.
     * 
     * @param username
     * @param nome
     * @param cognome
     * @param email
     */
    public DatiUtente(String username, String nome, String cognome, String email) {
        this.username = username;
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public String getNome() {
        return nome;
    }

    public String getCognome() {
        return cognome;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
    
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("username", username)
                .add("nome", nome)
                .add("cognome", cognome)
                .add("email", email)
                .toString();
    }

}
