package authorization.server.core.gestori;

import java.sql.SQLException;
import java.sql.Timestamp;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import authorization.shared.dati.Autorizzazione;
import authorization.shared.dati.TabellaCompleta;
import authorization.shared.eccezioni.AutorizzazioneNonEsistenteException;
import authorization.shared.eccezioni.UtenteNonEsistenteException;
import authorization.shared.utility.CalcolatoreTempo;

/**
 * Gestisce le autorizzazioni, creandone di nuove, revocandone vecchie e verificando quelle esistenti.
 * Per poter utilizzare i metodi bisogna accedere alla classe tramite una getInstance(), dopodiche si potranno utilizzare tutti i metodi contenuti.
 * @author Rodrigo
 *
 */
public final class GestoreAutorizzazioni{
	
	private static GestoreAutorizzazioni instance = null;
	
    /**
     * Logger slf4j
     */
	private static final Logger log = LoggerFactory.getLogger(GestoreAutorizzazioni.class);

	private GestoreAutorizzazioni() {
		super();
	}

	/**
     * Unica via di accesso al gestore delle autorizzazioni, cosi da evitare pi� istanze dello stesso.
	 * @return GestoreAutorizzazioni
	 */
	public static GestoreAutorizzazioni getInstance() {
		if (instance == null)
			instance = new GestoreAutorizzazioni();
		return instance;
	}
	
	/**
	 * Genera una nuova autorizzazione dato un utente a cui attribuirla e relativo livello e scadenza.
	 * @param user - una Stringa contenente il nome utente al quale attribuire l'autorizzazione
	 * @param livello - un Intero che indica il livello dell'autorizzazione
	 * @param scadenza - un Timestamp che indica la scadenza dell'autorizzazione
	 * @throws SQLException 
	 * @throws UtenteNonEsistenteException 
	 */
	public String generaChiave(@NotNull String user, @NotNull int livello, @NotNull Timestamp scadenza) throws SQLException, UtenteNonEsistenteException {
	    Autorizzazione a = new Autorizzazione(livello, scadenza, user);
	    GestoreDB.getInstance().aggiungiAutorizzazione(a);
	    log.info("Generata autorizzazione con chiave: "+a.getChiave());
	    return a.getChiave();
	}
	
	/**
	 * Imposta un'autorizzazione come revocata 
	 * @param chiave - una Stringa contenente la chiave da impostare come revocata
	 * @return {@link true} se operzione a buon fine, {@link false} altrimenti
	 * @throws SQLException 
	 * @throws AutorizzazioneNonEsistenteException
	 */
	public boolean revocaAutorizzazione(@NotNull String chiave) throws SQLException, AutorizzazioneNonEsistenteException {
	    if(GestoreDB.getInstance().cercaAutorizzazione(chiave)==null)
	        throw new AutorizzazioneNonEsistenteException();
	    log.info("Revocata autorizzazione: "+chiave);
	    return GestoreDB.getInstance().impostaRevocata(chiave);
	}
	
	/**
	 * Data una chiave ne verifica il livello e poi lo restituisce.
	 * @param chiave - una Stringa contenente la chiave dell'autorizzaione di cui verificare il livello
	 * @return livello associato alla autorizzazione della chiave cercata
	 * @throws AutorizzazioneNonEsistenteException
	 * @throws SQLException
	 */
	@Deprecated
	public int verificaLivello(@NotNull String chiave) throws SQLException, AutorizzazioneNonEsistenteException{
	    int livello = GestoreDB.getInstance().cercaAutorizzazione(chiave).getLivello();
	    return livello;
	}

    /**
     * Permette di ottenere la tabella completa di tutte le autorizzazioni con i loro dettagli
     */
    public TabellaCompleta tutteAutorizzazioni() throws SQLException {
        return GestoreDB.getInstance().tutteAutorizzazioni();
    }
    
    /**
     * L'autorizzazione � scaduta?
     * @param a
     * @return true se scaduta false altrimenti
     */
    public boolean scaduta(@NotNull Autorizzazione a) {
        if(CalcolatoreTempo.adesso().after(a.getScadenza()))
            return true;
        return false;
    }
}