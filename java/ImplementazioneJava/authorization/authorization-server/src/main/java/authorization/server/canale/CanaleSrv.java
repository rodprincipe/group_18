package authorization.server.canale;

/**
 * Interfaccia per il canale di comunicazione del server, permette aprirlo, attivare la ricezione, rispondere al messaggio ricevuto e infine chiuderlo.
 * @author Rodrigo
 *
 */
public interface CanaleSrv {
	
	/**
	 * Apre il canale di comunicazione
	 * @throws CanaleException per qualsiasi errore del canale di comunicazione.
	 */
    public void apri() throws CanaleException;
    
    /**
     * Riceve un messaggio sul canale di comunicazione.
     * @return stringa contenente il messaggio ricevuto 
     * @throws CanaleException per qualsiasi errore del canale di comunicazione.
     */
    public String ricevi() throws CanaleException;

    /**
     * Risponde con un messaggio sul canale di comunicazione
     * @param risposta stringa contenente il messaggio di risposta
     * @return {@link true} se il messaggio viene spedito {@link false} altrimenti
     */
    public boolean rispondi(String risposta);

    /**
     * Chiude il canale di comunicazione
     */
    public void chiudi();

}