package authorization.server.start;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import authorization.server.core.ServerCore;
import authorization.server.gui.ServerGui;

/**
 * Applicazione di autorizzazione lato server
 * @author Rodrigo
 *
 */

public class ApplicazioneSrv {
    private final ServerCore core;
    private final ServerGui gui;

    /**
     * Lancia l'applicazione lato server.
     */
    public static void main(String[] args) {
        ApplicazioneSrv app = new ApplicazioneSrv();
        app.avvia();
    }

    /**
     * Costruttore per la applicazione.
     */
    public ApplicazioneSrv() {
        gui = new ServerGui();
        core = new ServerCore();
    }

    /**
     * Avvia la applicazione.
     */
    public void avvia() {
        inizializzaGui();
        inizializzaCore();
    }

    /**
     * Inizializza la grafica del sistema.
     */
    private void inizializzaGui() {
        gui.addWindowListener(exit);
        gui.setVisible(true);
    }

    /**
     * Inizializza il nucleo operativo del sistema.
     * @throws SQLException 
     */
    private void inizializzaCore() {
        core.lancia();
    }

    /**
     * Termina il nucleo del sistema
     */
    private void terminaCore() {
        core.termina();
    }

    /**
     * Contenitore del evento di chiusura dell'applicazione
     */
    private final WindowAdapter exit = new WindowAdapter() {
        public void windowClosing(WindowEvent e)
        {
            JFrame frame = (JFrame)e.getSource();
            //Dialog di chiusura applicazione. 
            int result = JOptionPane.showConfirmDialog(
                    frame,
                    "Sei sicuro di voler chiudere l'applicazione?\n"
                            + "Le eventuali richieste in gestione verranno perse.",
                            "Chiusura applicazione",
                            JOptionPane.YES_NO_OPTION);

            //Se la chiusura viene confermata lancio la procedura di terminazione.
            if (result == JOptionPane.YES_OPTION) {
                terminaCore();
                gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }
        }
    };

}
