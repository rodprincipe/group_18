/**
 * Package contenente tutti i gestori. Ogni gestore si occupa delle operazioni su un tipo di oggetto. 
 */
/**
 * @author Rodrigo
 *
 */
package authorization.server.core.gestori;