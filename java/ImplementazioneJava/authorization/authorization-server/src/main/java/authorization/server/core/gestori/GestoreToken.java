package authorization.server.core.gestori;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import authorization.shared.dati.Autorizzazione;
import authorization.shared.dati.Risorsa;
import authorization.shared.dati.TabellaCompleta;
import authorization.shared.dati.Token;
import authorization.shared.eccezioni.AutorizzazioneNonEsistenteException;
import authorization.shared.eccezioni.AutorizzazioneRevocataException;
import authorization.shared.eccezioni.AutorizzazioneScadutaException;
import authorization.shared.eccezioni.LivelloInsufficienteException;
import authorization.shared.eccezioni.RisorsaNonEsistenteException;
import authorization.shared.eccezioni.TokenNonEsistenteException;
import authorization.shared.eccezioni.TokenScadutoException;
import authorization.shared.utility.CalcolatoreTempo;

/**
 * Gestisce i token attribuiti ad ogni autorizzazione. Ne crea di nuovi e verifica quelli esistenti.
 * @author Rodrigo
 *
 */
public class GestoreToken {

    private static GestoreToken instance = null;
    
    /**
     * Logger slf4j
     */
    private static final Logger log = LoggerFactory.getLogger(GestoreToken.class);
    
    private GestoreToken() {
        super();
    }

    /**
     * Unica via di accesso al gestore dei token, cosi da evitare piu' istanze dello stesso.
     * 
     * @return instance
     */
    public static GestoreToken getInstance(){
        if(instance == null)
            instance = new GestoreToken();
        return instance;
    }

    /**
     * Genera un nuovo token se l'autorizzazione ha sufficenti privilegi rispetto alla risorsa. Nel caso in cui l'autorizzazione data non abbia un livello sufficente verra' lanciata un eccezione.<br>
     * Per essere sufficente il valore della autorizzazione deve essere minore o uguale al livello della risorsa che voglio accedere.<br><br>
     * Autorizzazione liv 4 = Risorsa liv 4 -> Posso accedere<br>
     * Autorizzazione liv 3 < Risorsa liv 4 -> Posso accedere<br>
     * Autorizzazione liv 4 > Risorsa liv 3 -> NON posso accedere<br>
     * Autorizzazione liv 0 -> posso accedere a tutte le risorse<br>
     * @param chiave - una stringa conenente la chaive dell'autorizzazione tramite la quale chiedere il token
     * @param idRisorsa - una stringa contente l'identificativo della risorsa per la quale viene richiesto l'accesso
     * @return {@link Token} valido per l'accesso alla risorsa richiesta
     * @throws SQLException
     * @throws RisorsaNonEsistenteException
     * @throws AutorizzazioneNonEsistenteException
     * @throws LivelloInsufficienteException
     * @throws AutorizzazioneScadutaException
     * @throws AutorizzazioneRevocataException
     */
    public Token generaToken(@NotNull String chiave, @NotNull String idRisorsa) throws SQLException, RisorsaNonEsistenteException, AutorizzazioneNonEsistenteException, LivelloInsufficienteException, AutorizzazioneScadutaException, AutorizzazioneRevocataException {
        Risorsa r = GestoreDB.getInstance().cercaRisorsa(idRisorsa);
        Autorizzazione a = GestoreDB.getInstance().cercaAutorizzazione(chiave);
        
        if(r==null)
            throw new RisorsaNonEsistenteException();
        if(a==null)
            throw new AutorizzazioneNonEsistenteException();
        
        if(a.getLivello() > r.getLivello()){
            throw new LivelloInsufficienteException();
        }else if(GestoreAutorizzazioni.getInstance().scaduta(a)){
            throw new AutorizzazioneScadutaException();
        }else if(a.getRevocata()) {
            throw new AutorizzazioneRevocataException();
        }else{
            Token t = new Token(Timestamp.valueOf(LocalDateTime.now()),a.getChiave(),r.getIdRisorsa());
            GestoreDB.getInstance().aggiungiToken(t);
            log.info("Token generato: "+t.toString());
            return t;
        }
    }
    
    /**
     * Permette verificare il lasso di tempo rimanente ad un determinato token. Dopo averlo cercarto e trovato ne restituira' il tempo di validita' rimanente.
     * @param t - un Token istanziato
     * @return Duration - un oggetto "Duration" che indica la durata riamanente di validita' del token
     * @throws SQLException 
     * @throws TokenScadutoException 
     * @throws TokenNonEsistenteException 
     */
    public Duration verificaToken(@NotNull Token t) throws SQLException, TokenScadutoException, TokenNonEsistenteException{
       t = GestoreDB.getInstance().cercaToken(t.getIdToken());
       if(t == null)
           throw new TokenNonEsistenteException();
       Duration d = tempoRimanenteDa(t.getDataGenerazione());
       if (d.isNegative())
           throw new TokenScadutoException("Token scaduto.");
       return d;
    }

    /**
     * Calcola, data la data di generazione di un token, quanto tempo delle 24h di valita' gli rimangono
     * @param dataGenerazione data in cui e' stato generato il token
     * @return 'Duration' contentente il lasso di tempo rimanente 
     */
    private Duration tempoRimanenteDa(Timestamp dataGenerazione) {
        return CalcolatoreTempo.tempoRimanente(dataGenerazione, 1);
    }

    /**
     * Permette di ottenere la tabella completa di tutti i token con i loro dettagli
     */
    public TabellaCompleta tuttiToken() throws SQLException {
        return GestoreDB.getInstance().tuttiToken();
    }
}
