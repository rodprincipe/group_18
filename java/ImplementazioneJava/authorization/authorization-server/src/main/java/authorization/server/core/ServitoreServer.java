/**
 * 
 */
package authorization.server.core;

import java.sql.SQLException;
import java.sql.Timestamp;

import com.google.common.base.Strings;

import authorization.server.core.gestori.GestoreAutorizzazioni;
import authorization.server.core.gestori.GestoreDatiUtente;
import authorization.server.core.gestori.GestoreRisorse;
import authorization.server.core.gestori.GestoreToken;
import authorization.shared.dati.Risorsa;
import authorization.shared.dati.TabellaCompleta;
import authorization.shared.dati.Token;
import authorization.shared.eccezioni.AutorizzazioneNonEsistenteException;
import authorization.shared.eccezioni.AutorizzazioneRevocataException;
import authorization.shared.eccezioni.AutorizzazioneScadutaException;
import authorization.shared.eccezioni.LivelloInsufficienteException;
import authorization.shared.eccezioni.ParametroVuoto;
import authorization.shared.eccezioni.RisorsaGiaEsistenteException;
import authorization.shared.eccezioni.RisorsaNonEsistenteException;
import authorization.shared.eccezioni.TokenNonEsistenteException;
import authorization.shared.eccezioni.TokenScadutoException;
import authorization.shared.eccezioni.UtenteNonEsistenteException;
import authorization.shared.interfacce.Servizi;

/**
 * Classe che si occupa di servire tutte le chiamate dei vari metodi.<br>
 * Ogni chiamata viene girata al rispettivo gestore che processera' la richiesta.
 * @author Rodrigo
 */
public class ServitoreServer implements Servizi {

    private static ServitoreServer instance=null;
    
    private ServitoreServer() {
        super();
    }
    
    public static ServitoreServer getInstance() {
        if(instance==null)
            instance = new ServitoreServer();
        return instance;
    }

    public String generaChiave(String user,
                               int livello,
                               Timestamp scadenza) throws UtenteNonEsistenteException, 
                                                          SQLException, ParametroVuoto {
        if(Strings.isNullOrEmpty(user) || scadenza==null)
            throw new ParametroVuoto();
        return GestoreAutorizzazioni.getInstance().generaChiave(user, livello, scadenza);
    }

    public void revocaAutorizzazione(String chiave) throws AutorizzazioneNonEsistenteException, 
                                                              SQLException, ParametroVuoto {
        if(Strings.isNullOrEmpty(chiave))
            throw new ParametroVuoto();
        GestoreAutorizzazioni.getInstance().revocaAutorizzazione(chiave);
    }

    public Token generaToken(String chiave,
                             String idRisorsa) throws SQLException,
                                                      AutorizzazioneNonEsistenteException,
                                                      RisorsaNonEsistenteException,
                                                      AutorizzazioneScadutaException,
                                                      LivelloInsufficienteException,
                                                      AutorizzazioneRevocataException, ParametroVuoto{
        if(Strings.isNullOrEmpty(chiave) || Strings.isNullOrEmpty(idRisorsa))
            throw new ParametroVuoto();
        return GestoreToken.getInstance().generaToken(chiave, idRisorsa);
    }

    
    public String verificaToken(Token t) throws  TokenScadutoException,
                                                 TokenNonEsistenteException, 
                                                 SQLException, ParametroVuoto {
        if(t==null)
            throw new ParametroVuoto();
        return GestoreToken.getInstance().verificaToken(t).toString();
    }

    public TabellaCompleta tutteAutorizzazioni() throws SQLException {

         TabellaCompleta tab =  GestoreAutorizzazioni.getInstance().tutteAutorizzazioni();
        return tab;
    }
    
    public TabellaCompleta tuttiToken() throws SQLException   {

        TabellaCompleta tab =  GestoreToken.getInstance().tuttiToken();
        return tab;
    }
    public TabellaCompleta tutteRisorse() throws SQLException   {

        TabellaCompleta tab =  GestoreRisorse.getInstance().tutteRisorse();
        return tab;
    }
    
    public TabellaCompleta tuttiUtenti() throws SQLException   {

        TabellaCompleta tab =  GestoreDatiUtente.getInstance().tuttiUtenti();
        return tab;
    }

    public boolean aggiungiRisorsa(Risorsa r) throws SQLException, RisorsaGiaEsistenteException {
        return GestoreRisorse.getInstance().aggiungiRisorsa(r);
    }

    public void rimuoviRisorsa(String idRisorsa) throws SQLException {
        GestoreRisorse.getInstance().rimuoviRisorsa(idRisorsa);
    }
    
}