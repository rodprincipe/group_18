/**
 * 
 */
package authorization.server.canale;


/**
 * Eccezione che il canale, creato dal server, puo' produrre nel momento in cui si produce un errore sullo stesso.
 * @author Rodrigo
 *
 */
public class CanaleException extends Exception {

    private static final long serialVersionUID = 1L;
    private CanaleErr err;
    
    /**
     * Enum dei possibili errori.
     */
    public enum CanaleErr{

        ALTRO (1, "Errore sul canale di comunicazione"), 
        TERM (2,"Canale di comunicazione terminato"),
        NOBIND(3,"Manca il bind sul indirizzo del canale di comunicazione"),
    	INDKO(4,"Indirizzo del canale di comunicazione occupato");
        
        private final int codice;
        private final String descr;
        
        
        CanaleErr(int codice, String descr) {
            this.codice = codice;
            this.descr = descr;
        }
        
        public int getCodice() {
            return codice;
        }
        
        public String getDescr() {
            return descr;
        }
        
        @Override
        public String toString() {
            return "Errore "+getCodice()+": "+getDescr();
        }
        
    }
    
    public CanaleErr getErr() {
        return err;
    }

    public CanaleException(CanaleErr err) {
        super(err.getDescr());
        this.err = err;
    }

    public CanaleException(CanaleErr err, Throwable cause) {
        super(err.getDescr(), cause);
        this.err = err;
    }

}
