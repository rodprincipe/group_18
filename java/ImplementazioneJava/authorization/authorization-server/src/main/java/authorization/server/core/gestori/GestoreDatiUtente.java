package authorization.server.core.gestori;

import java.sql.SQLException;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import authorization.shared.dati.DatiUtente;
import authorization.shared.dati.TabellaCompleta;
import authorization.shared.eccezioni.UtenteGiaEsistenteException;

/**
 * Gestisce i dati dei vari utente, permette di ottenere dati di uno specifico utente.
 * @author Rodrigo
 *
 */
public class GestoreDatiUtente {

    private static GestoreDatiUtente instance = null;
    
    /**
     * Logger slf4j
     */
    private static final Logger log = LoggerFactory.getLogger(GestoreDatiUtente.class);

    private GestoreDatiUtente() {
        super();
    }

    /**
     * Unica via di accesso al gestore dei dati utente, cosi da evitare pi� istanze dello stesso
     * @return instance
     */
    public static GestoreDatiUtente getInstance() {
        if (instance == null)
            instance = new GestoreDatiUtente();
        return instance;
    }
    
    /**
     * Permette di effettuare nel sistema la ricerca di un determinato utente, nel caso in cui viene trovato restituisce l'istanza di DatiUtente contenente tutti i sui dati
     * @param username
     * @return dati utente se trovato l'utente altrimenti null
     */
    public DatiUtente cercaUtente(@NotNull String username) throws SQLException {
        return GestoreDB.getInstance().cercaUtente(username);
    }

    /**
     * Permette creare un nuovo utente nel sistema. Lancia un'eccezione nel caso in cui il nome utente scelto sia gi� utilizzato.
     * @param username
     * @param nome
     * @param cognome
     * @param email
     * @param password
     * @throws UtenteGiaEsistenteException 
     * @throws SQLException 
     */
    public void creaUtente(@NotNull String username, @NotNull String nome, @NotNull String cognome, @NotNull String email, @NotNull String password) throws SQLException, UtenteGiaEsistenteException {
        DatiUtente datiUtente = new DatiUtente(username, nome, cognome, email, password);
        GestoreDB.getInstance().aggiungiUtente(datiUtente);
        log.info("Utente aggiunto: "+datiUtente.toString());
    }
    
    /**
     * Permette di ottenere la tabella completa di tutti gli utenti con i loro dettagli
     * @return tabella completa di tutti gli utenti con i loro dettagli
     */
    public TabellaCompleta tuttiUtenti() throws SQLException {
        return GestoreDB.getInstance().tuttiUtenti();
    }
}
