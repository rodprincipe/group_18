package authorization.server.gui;

import javax.swing.JTextArea;

import org.apache.log4j.WriterAppender;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Classe appender per l'aggiunta di log personalizzati 
 * @author Rodrigo
 *
 */
public class AreaTestoAppender extends WriterAppender {

    /**
     * Area di testo grafica sulla quale visualizzare i log
     */
    private static JTextArea areaTesto = new JTextArea();

    /**
     * Imposta l'area di testo sulla quale visualizzare i log
     * @param textArea {@link JTextArea} sulla quale visualizzare i log
     */
    public static void setTextArea(final JTextArea textArea) {
        AreaTestoAppender.areaTesto = textArea;
    }

    /**
     * Formatta e poi aggiunge il log 
     * @param eventoLog {@linkplain LoggingEvent} del nuovo log
     */
    @Override
    public void append(final LoggingEvent eventoLog) {
        final String messaggio = this.layout.format(eventoLog);
        areaTesto.append(messaggio);
    }
}