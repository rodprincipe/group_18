package authorization.server.gui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

/**
 * Classe contente la grafica del server.
 * @author Rodrigo
 *
 */
public class ServerGui extends JFrame {

    private static final long serialVersionUID = 1L;
    
    /**
     * Pannello contenente tutta la grafica
     */
    private JPanel contentPane;
    
    /**
     * Area di testo per visualizzare i log
     */
    private JTextArea textArea;

    /**
     * Crea il framework.
     */
    public ServerGui() {
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
        setBounds(100, 100, 1000, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        textArea = new JTextArea();
        textArea.setEditable(false);
        contentPane.add(textArea, BorderLayout.CENTER);
        
        JScrollPane scrollPane = new JScrollPane(textArea);
        contentPane.add(scrollPane, BorderLayout.CENTER);
        
        AreaTestoAppender.setTextArea(textArea);
    }
}
