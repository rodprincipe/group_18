package authorization.server.canale;

import org.slf4j.LoggerFactory;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;
import org.zeromq.ZMQException;

import authorization.server.canale.CanaleException.CanaleErr;
import zmq.ZError;

/**
 * Canale di comunicazione ZeroMQ con approccio a code lato server
 * @author Rodrigo
 *
 */
public class CanaleSrvZMQ implements CanaleSrv {

	private final ZContext context;
	private final Socket socket;
	private final String porta;
	private boolean aperto;

	private static final org.slf4j.Logger log = LoggerFactory.getLogger(CanaleSrvZMQ.class);

	/**
	 * Crea un nuovo canale di comunicazione sul quale ricevere e inviare i messaggi
	 * @param porta sulla quale creare il canale di comunicazione
	 * @throws CanaleException se il canale ha dei problemi
	 */
	public CanaleSrvZMQ(String porta) throws CanaleException {
		//Definisco contesto e socket sul quale ricevere i messaggi
		try {
			context = new ZContext();
			socket = context.createSocket(ZMQ.REP);
			this.porta=porta;
		}catch(ZMQException e){
			throw new CanaleException(CanaleErr.ALTRO, e);
		}
	}
	
	/**
	 * Crea un nuovo canale di comunicazione sulla porta standard 5555
	 * @throws CanaleException se il canale ha dei problemi
	 */
	public CanaleSrvZMQ() throws CanaleException {
		this("5555");
	}

	
	public void apri() throws CanaleException {
		try {
		aperto = socket.bind("tcp://*:"+porta);
		if(aperto)
			log.info("Canale di comunicazione aperto");
		else
			log.info("Canale NON aperto");
		}catch(ZMQException e) {
			if(e.getErrorCode()==ZError.EADDRINUSE || e.getErrorCode()==ZError.EADDRNOTAVAIL) 
				throw new CanaleException(CanaleErr.INDKO,e);
			else
				throw new CanaleException(CanaleErr.ALTRO, e);
		}

	}
	
    /**
     * Attiva la ricezione di messaggi sul canale di comunicazione e ritorna il messaggio ricevuto.<br>
     * Lancia eccezioni solo in due casi: nel caso di errori a livello del canale oppure nel caso in cui viene chiuso il canale cosi da indicare la corretta chiusura.
     * @return stringa contenente il messaggio ricevuto 
     * @throws CanaleException per qualsiasi errore del canale di comunicazione.<br> In chiusura avra' errore {@link CanaleException.CanaleErr#TERM}.
     */
	public String ricevi() throws CanaleException {
		if(!aperto)
			throw new CanaleException(CanaleErr.NOBIND);

		try {
			//Attivo ricezione messaggi
			String messaggio = new String(socket.recv(0));
			return messaggio;

		} catch (ZMQException e) {
			if (e.getErrorCode() == ZError.ETERM) {
				throw new CanaleException(CanaleErr.TERM,e);
			}else {
				throw new CanaleException(CanaleErr.ALTRO,e);
			}
		}
	}

	public boolean rispondi(String risposta) {
		//Invio risposta
		return socket.send(risposta.getBytes(), 0);
	}

	public void chiudi() {
		if(context!=null) {
			context.destroy();
			if(aperto)
				log.info("Canale di comunicazione chiuso");
		}
	}
}
