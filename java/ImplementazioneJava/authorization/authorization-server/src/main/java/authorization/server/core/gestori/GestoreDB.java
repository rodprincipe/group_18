package authorization.server.core.gestori;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;
import java.util.Vector;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import authorization.shared.dati.Autorizzazione;
import authorization.shared.dati.DatiUtente;
import authorization.shared.dati.Risorsa;
import authorization.shared.dati.TabellaCompleta;
import authorization.shared.dati.Token;
import authorization.shared.eccezioni.AutorizzazioneNonEsistenteException;
import authorization.shared.eccezioni.RisorsaGiaEsistenteException;
import authorization.shared.eccezioni.RisorsaNonEsistenteException;
import authorization.shared.eccezioni.UtenteGiaEsistenteException;
import authorization.shared.eccezioni.UtenteNonEsistenteException;

/**
 * Gestisce la connessione al database. Apertura, chiusura della connessione e tutte le query fanno parte di cio'.
 * @author Rodrigo
 *
 */
public class GestoreDB {

    private static GestoreDB instance = null;

    /**
     * Logger slf4j
     */
    private static final Logger log = LoggerFactory.getLogger(GestoreDB.class);
    
    /**
     * Indirizzo e porta del database.
     */
    private static final String DB_HOST = "localhost:3306";

    /**
     * Nome del database.
     */
    private static final String DB_NOME = "authorization";

    /**
     * Nome utente per l'accesso al database.
     */
    private static final String USER = "root";

    /**
     * Password per l'accesso al database.
     */
    private static final String PASS = "toor";

    /**
     * Connesione al data base SQL
     */
    private static Connection conn = null;


    /**
     * Costruttore del gestore del DataBase che si occuper� di eseguire le query SQL. 
     * @throws SQLException
     */
    private GestoreDB() throws SQLException {
        super();
    }

    /**
     * Unica via di accesso al GestoreDB per evitare piu' istanze dello stesso.
     * @return istanza del GestoreDB
     * @throws SQLException - se non connesso al DB
     */
    public static GestoreDB getInstance() throws SQLException  {
        if (instance==null)
            instance = new GestoreDB();
        if (conn==null||conn.isClosed()) {
            SQLException e = new SQLException("Manca connesione al DB");
            log.error(e.getMessage());
            throw e;
        }
        return instance;
    }    

    public static Connection getConn() {
        return conn;
    }

    /**
     * Apre la connessione con il DB verso specifico host
     * @throws SQLException
     */
    public static void apriConn(String host, String nome) throws SQLException {
        String dbHost = (host.isEmpty()) ? DB_HOST : host ;
        String dbNome = (nome.isEmpty()) ? DB_NOME : nome ;
        String dbUrl = "jdbc:mysql://"+dbHost+"/"+dbNome+"?autoreconnect=true&useSSL=false";
        try {
            conn = DriverManager.getConnection(dbUrl, USER, PASS);
            log.info("Connessione al DB aperta");
        } catch (SQLException e) {
            SQLException eBis = new SQLException("Problema in connessione al DB", e);
            log.info(eBis.getMessage());
            log.error(e.getMessage());
            throw eBis;
        }
    }

    /**
     * Apre la connessione con il DB con host locale
     * @throws SQLException
     */
    public static void apriConn() throws SQLException {
        apriConn("","");
    }

    /**
     * Chiude la connessione con il DB.
     * @throws SQLException 
     */
    public static void chiudiConn() throws SQLException {
        if(conn!=null) {
            try {
                conn.close();
                log.info("Connessione DB chiusa");
            } catch (SQLException e) {
                SQLException eBis = new SQLException("Problema in disconnessione al DB", e);
                log.info(eBis.getMessage());
                throw eBis;
            }
        }
    }

    /**
     * Carica sul db una nuova autorizzazione.
     * @param a - un Autorizzazione contenente i dettagli dell'autorizzazione da caricare
     * @return {@link Boolean}
     * @throws SQLException 
     * @throws UtenteNonEsistenteException 
     */
    public boolean aggiungiAutorizzazione(@NotNull Autorizzazione a) throws SQLException, UtenteNonEsistenteException {
        String query = "insert into `autorizzazione` (`chiave`,`livello`,`scadenza`,`isrevocato`,`username`) values (?,?,?,?,?)";
        PreparedStatement ps;
        ps = conn.prepareStatement(query);
        try{
            ps.setString(1, a.getChiave());
            ps.setInt(2, a.getLivello());
            ps.setTimestamp(3, a.getScadenza());
            ps.setBoolean(4, false);
            ps.setString(5, a.getUsername());
            ps.execute();
        }catch (SQLException e) {
            if(cercaAutorizzazione(a.getChiave())!=null) {
                a.setChiave(UUID.randomUUID().toString());
                aggiungiAutorizzazione(a);
            }else if(cercaUtente(a.getUsername())==null) {
                throw new UtenteNonEsistenteException("Autorizzazione non aggiunta: utente non valido",e);
            }else {
                throw new SQLException("Autorizzazione non inserita", e);
            }
        }finally {
            ps.close();
        }
        return true;
    }
    /**
     * Cerca sul db un'autorizzazione secondo la sua chiave. Se trovata ne restituisce l'istanza, se non viene trovata lancia un eccezione.
     * @param chiave - una stringa con la chiave da ricercare
     * @return un Autorizzazione contenente i dettagli dell'autorizzazione cercata altrimenti null
     * @throws SQLException
     */
    public Autorizzazione cercaAutorizzazione(String chiave) throws SQLException {
        String query = "select * from autorizzazione where chiave = ? ";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, chiave);
        ResultSet rs = ps.executeQuery();

        if(!rs.next())
            return null;
        else{
            Autorizzazione a = new Autorizzazione(
                    rs.getString("chiave"),
                    rs.getInt("livello"),
                    rs.getBoolean("isRevocato"),
                    rs.getTimestamp("scadenza"),
                    rs.getString("username"));
            rs.close();
            ps.close();
            return a;
        }
    }

    /**
     * Imposta sul db una specifica autorizzazione come revocata.
     * @param chiave - una stringa con la chiave dell'autorizzazione da revocare
     * @return {@link Boolean}
     * @throws SQLException
     * @throws AutorizzazioneNonEsistenteException 
     */
    public boolean impostaRevocata(String chiave) throws  AutorizzazioneNonEsistenteException, SQLException {
        String query = "UPDATE `autorizzazione` SET `IsRevocato`='1' WHERE `chiave`=?";
        PreparedStatement ps = conn.prepareStatement(query);
        try{
            ps.setString(1, chiave);
            ps.execute();
        }catch(SQLException e) {
            if(cercaAutorizzazione(chiave)==null)
                throw new AutorizzazioneNonEsistenteException("Autorizzazione non revocata: chiave non valida");
            else
                throw new SQLException("Autorizzazione non revocata",e);
        }finally {
            ps.close();
        }
        return true;
    }

    /**
     * Carica sul db un nuovo token.
     * @param t - un Token contenente i dettagli del token da caricare
     * @return {@link Boolean}
     * @throws SQLException
     * @throws UtenteNonEsistenteException
     * @throws AutorizzazioneNonEsistenteException 
     */
    public boolean aggiungiToken(@NotNull Token t) throws SQLException, AutorizzazioneNonEsistenteException{
        String query = "insert into `token` (`idtoken`,`datagenerazione`,`chiave`,`idrisorsa`) values (?,?,?,?)";
        PreparedStatement ps;
        ps = conn.prepareStatement(query);
        try {
            ps.setString(1, t.getIdToken());
            ps.setTimestamp(2, t.getDataGenerazione());
            ps.setString(3, t.getChiave());
            ps.setString(4, t.getIdRisorsa());
            ps.execute();
            ps.close();
        }catch (SQLException e) {
            if(cercaToken(t.getIdToken())!=null) {
                t.setIdToken(UUID.randomUUID().toString());
                aggiungiToken(t);
            }else if(cercaAutorizzazione(t.getChiave())==null) {
                throw new AutorizzazioneNonEsistenteException("Token non aggiunto: chiave non valida",e);
            }else {
                throw new SQLException("Token non aggiunto", e);
            }
        }finally {
            ps.close();
        }
        return true;
    }

    /**
     * Cerca sul db un token secondo il suo id. Se trovato ne restituisce l'istanza, se non viene trovato lancia un eccezione.
     * @param idToken - una stringa contenente l'id del token da ricercare
     * @return un {@link Token} contenente i dettagli del token cercato altrimenti null
     * @throws SQLException 
     */
    public Token cercaToken(String idToken) throws SQLException {
        String query = "select * from `token` where idtoken=?";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, idToken);
        ResultSet rs = ps.executeQuery();

        if(!rs.next())
            return null;
        else{
            Token token = new Token(
                    rs.getString("idtoken"),
                    rs.getTimestamp("datagenerazione"),
                    rs.getString("chiave"),
                    rs.getString("idrisorsa"));
            rs.close();
            ps.close();
            return token;
        }
    }

    /**
     * Aggiunge sul db una nuova risorsa.
     * @param r - una {@link Risorsa} da caricare
     * @return {@link Boolean}
     * @throws SQLException 
     * @throws RisorsaGiaEsistenteException 
     */
    public boolean aggiungiRisorsa(@NotNull Risorsa r) throws SQLException, RisorsaGiaEsistenteException {
        String query = "insert into `risorsa` (`idrisorsa`,`descrizione`,`livello`,`indirizzo`) values (?,?,?,?)";
        PreparedStatement ps = conn.prepareStatement(query);
        try{
            ps.setString(1, r.getIdRisorsa());
            ps.setString(2, r.getDescrizione());
            ps.setInt(3, r.getLivello());
            ps.setString(4, r.getIndirizzo());
            ps.execute();
        } catch (SQLException e) {
            if(cercaRisorsa(r.getIdRisorsa())!=null)   
                throw new RisorsaGiaEsistenteException("Risorsa non aggiunta: identificativo gia' utilizzato");
            else {
                e.printStackTrace();
                throw new SQLException("Risorsa non aggiunta",e);
            }
                
        }finally {
            ps.close();
        }
        return true;
    }
    /**
     * Cerca sul db una risorsa secondo il suo id. Se trovata ne restituisce l'istanza, se non viene trovata lancia un eccezione.
     * @param idRisorsa - una stringa con l'id della risorsa da ricercare
     * @return una Risorsa contenente i dettagli della risorsa cercata altrimenti null
     * @throws SQLException
     */
    public Risorsa cercaRisorsa(String idRisorsa) throws SQLException {
        String query = "select * from risorsa where idrisorsa = ? ";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, idRisorsa);
        ResultSet rs = ps.executeQuery();

        if(!rs.next())
            return null;
        else{
            Risorsa r = new Risorsa(
                    rs.getString("idRisorsa"),
                    rs.getString("descrizione"),
                    rs.getInt("livello"),
                    rs.getString("indirizzo"));
            rs.close();
            ps.close();
            return r;
        }
    }
    
    /**
     * Rimuove del db una risorsa esistente se non ha delle autorizzazioni pendenti, in quel caso genera una SQLException.
     * @param idRisorsa - stringa del identificativo della risorsa da eliminare
     * @throws SQLException
     * @throws RisorsaNonEsistenteException 
     */
    public void rimuoviRisorsa(String idRisorsa) throws SQLException {
        String query = "DELETE FROM `risorsa` WHERE `IdRisorsa`= ?";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, idRisorsa);
        ps.execute();
    }
    
    /**
     * Aggiunge sul db un nuovo utente.
     * @param dU - un "DatiUtente" da caricare
     * @return {@link Boolean}
     * @throws SQLException 
     * @throws UtenteGiaEsistenteException 
     */
    public boolean aggiungiUtente(@NotNull DatiUtente dU) throws SQLException, UtenteGiaEsistenteException {
        String query = "insert into `datiutente` (`username`,`nome`,`cognome`,`email`,`password`) values (?,?,?,?,password(?))";
        PreparedStatement ps = conn.prepareStatement(query);
        try{
            ps.setString(1, dU.getUsername());
            ps.setString(2, dU.getNome());
            ps.setString(3, dU.getCognome());
            ps.setString(4, dU.getEmail());
            ps.setString(5, dU.getPassword());
            ps.execute();
        } catch (SQLException e) {
            if(cercaUtente(dU.getUsername())!=null)
                throw new UtenteGiaEsistenteException("Utente non aggiunto: username gia' utilizzato");
            else
                throw new SQLException("Utente non aggiunto",e);
        }finally {
            ps.close();
        }
        return true;
    }

    /**
     * Cerca sul db un utente secondo il suo username. Se trovato ne restituisce l'istanza, se non viene trovato lancia un eccezione.
     * @param username - una stringa contenente l'username del utente da ricercare sul db
     * @return {@link DatiUtente} del utente cercato altrimenti null
     * @throws SQLException
     */
    public DatiUtente cercaUtente(String username) throws SQLException {
        String query = "select * from `datiutente` where ?";
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setString(1, username);
        ResultSet rs = ps.executeQuery();

        if(!rs.next())
            return null;
        else{
            DatiUtente datiUtente = new DatiUtente(
                    rs.getString("username"),
                    rs.getString("nome"),
                    rs.getString("cognome"),
                    rs.getString("email"));
            rs.close();
            ps.close();
            return datiUtente;
        }
    }

    /**
     * Permette eseguire query di tipo select e restituisce il result set intero con descrizione delle colonne. E' possibilie effettuare order by.
     * @param sql - una Stringa contenente la query da eseguire tipo:
     * <pre>
     *              select * from autorizzazione order by scadenza
     *              select * from risorsa
     * </pre>
     * @return un oggetto "TabellaCompleta" contenente intestazione della tabella e relativi dati risultato della query
     * @throws SQLException 
     */
    private TabellaCompleta eseguiSelectAll(String sql) throws SQLException{

        Statement stmt = null;
        ResultSet rs = null;
        Vector<String> nomiColonne = new Vector<String>();
        Vector<Vector<String>> recordTabella = new Vector<Vector<String>>();

        stmt = conn.createStatement();
        rs = stmt.executeQuery(sql);
        if (rs != null) {
            ResultSetMetaData colonne = rs.getMetaData();
            int i = 0;
            while (i < colonne.getColumnCount()) {
                i++;
                nomiColonne.add(colonne.getColumnName(i));
            }
            while (rs.next()) {
                Vector<String> riga = new Vector<String>();
                for (i = 0; i < nomiColonne.size(); i++) {
                    riga.add(rs.getString(nomiColonne.get(i)));
                }
                recordTabella.add(riga);
            }
        }

        return new TabellaCompleta(nomiColonne,recordTabella);
    }

    public TabellaCompleta tutteAutorizzazioni() throws SQLException {
        return eseguiSelectAll("select * from autorizzazione order by scadenza desc");
    }

    public TabellaCompleta tuttiToken() throws SQLException {
        return eseguiSelectAll("select * from token order by datagenerazione desc");
    }

    public TabellaCompleta tutteRisorse() throws SQLException { 
        return eseguiSelectAll("select * from risorsa");
    }

    public TabellaCompleta tuttiUtenti() throws SQLException {
        return eseguiSelectAll("select * from datiutente");
    }

}