package authorization.server.core.gestori;

import java.sql.SQLException;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import authorization.shared.dati.Risorsa;
import authorization.shared.dati.TabellaCompleta;
import authorization.shared.eccezioni.RisorsaGiaEsistenteException;
import authorization.shared.eccezioni.RisorsaNonEsistenteException;

/**
 * Gestice le risorse e le verifica.
 * @author Rodrigo
 */
public class GestoreRisorse {
    
    private static GestoreRisorse instance = null;
    
    /**
     * Logger slf4j
     */
    private static final Logger log = LoggerFactory.getLogger(GestoreRisorse.class);

    private GestoreRisorse() {
        super();
    }
    
    /**
     * Unica via di accesso al gestore delle risorse, cosi da evitare pi� istanze dello stesso.
     * @return istanza del {@link GestoreRisorse}
     */
    public static GestoreRisorse getInstance(){
        if(instance == null)
            instance = new GestoreRisorse();
        return instance;
    }
    
    /**
     * Dato un id ne verifica il livello della risorsa associata e poi lo restituisce.
     * @param idRisorsa - una Stringa che contiene l'id della risorsa della quale verificare il livello
     * @return livello associato al id della risorsa cercata
     * @throws SQLException
     * @throws RisorsaNonEsistenteException
     */
    public int verificaLivello(@NotNull String idRisorsa) throws SQLException, RisorsaNonEsistenteException{
        return GestoreDB.getInstance().cercaRisorsa(idRisorsa).getLivello();
    }

    /**
     * Permette di ottenere la tabella completa di tutte le risorse con i loro dettagli
     * @return tabella con tutte le risorse
     */
    public TabellaCompleta tutteRisorse() throws SQLException {
        return GestoreDB.getInstance().tutteRisorse();
    }

    /**
     * Permette di aggiungere una risorsa 
     * @param r risorsa da aggiungere
     * @return {@link Boolean}
     * @throws SQLException
     * @throws RisorsaGiaEsistenteException
     */
    public boolean aggiungiRisorsa(@NotNull Risorsa r) throws SQLException, RisorsaGiaEsistenteException {
        if(GestoreDB.getInstance().aggiungiRisorsa(r)) {
            log.info("Risorsa aggiunta: "+r.toString());
            return true;
        }
        return false;
    }

    /**
     * Permette rimuovere una risorsa che non ha dei token allegati
     * @param idRisorsa identificativo risorda da rimuovere
     * @throws SQLException
     * @throws RisorsaNonEsistenteException
     */
    public void rimuoviRisorsa(@NotNull String idRisorsa) throws SQLException {
        log.info("Rimuovo risorsa con id: "+idRisorsa);
        GestoreDB.getInstance().rimuoviRisorsa(idRisorsa);
    }
}
