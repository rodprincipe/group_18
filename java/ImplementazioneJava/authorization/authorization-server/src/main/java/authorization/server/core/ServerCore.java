package authorization.server.core;

import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import authorization.server.canale.CanaleException;
import authorization.server.canale.CanaleException.CanaleErr;
import authorization.server.canale.CanaleSrv;
import authorization.server.canale.CanaleSrvZMQ;
import authorization.server.core.gestori.GestoreDB;
import jsonRpc.server.JsonRpcServer;

/**
 * Classe contente il core del server.
 * @author Rodrigo
 *
 */
public class ServerCore {
    
    /**
     * Logger slf4j
     */
    private static final Logger log = LoggerFactory.getLogger(ServerCore.class);

    /**
     * Canale di comunicazione per l'invio e la ricezione dei messaggi
     */
    private CanaleSrv canale=null;
    
    /**
     * Parser per processare Json
     */
    private ObjectMapper mapper;
    
    /**
     * Remote Procedure Call lato server secondo le specifiche JSON-RPC 2.0
     */
    private JsonRpcServer rpcServer;
    
    /**
     * Thread del core.
     */
    private Thread thrCore;
    
    /**
     * Indica se il core e' stato terminato
     */
    private boolean terminato=false;

    /**
     * Costrutttore del core del server: esegue le operazioni di inizializzazione principali
     */
    public ServerCore() {
        inizializzaRpcServer();
    }


    /**
     * Lancia il server
     */
    public void lancia() {
    	log.info("Avvio...");
        thrCore = new Thread(new Runnable() {
            public void run() {
                try {
                    inizializzaConnessioni();
                    attivaRicezioneGestioneMessaggi();
                }catch(CanaleException e) {
                	 log.error(e.getMessage(),e.getCause());
                	 termina();
                } catch (Exception e) {
                    //Catch di qualsiasi eccezione che si genera in fase di avvio del core e terminazione
                    // del core per mantenere la sicurezza complessiva del sistema
                    log.error("Errore durante l'avvio del core", e);
                    termina();
                }
            }
        });

        thrCore.start();
    }
    
    /**
     * Termina il server
     */
    public void termina() {
        if(!terminato) {
	        terminato=true;
	        
	        if(canale!=null) {
	            canale.chiudi();
	        }
	        
	        try{
	            GestoreDB.chiudiConn();
	        }catch (SQLException e) {
	            log.error(e.getCause().getMessage());
	        }
	        
	        log.info("Terminato.");
        }
    }

    /**
     * Inizializza il Remote Procedure Call lato server
     */
    private void inizializzaRpcServer() {

        mapper = new ObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        rpcServer = new JsonRpcServer(mapper, ServitoreServer.getInstance());

    }

    /**
     * Inizializza le connessioni esterne
     * @throws SQLException
     * @throws CanaleException 
     */
    private void inizializzaConnessioni() throws SQLException, CanaleException {
    	inizializzaDB();
        inizializzaCanale();

    }

    /**
     * Inizializza il canale di comunicazione
     * @throws CanaleException 
     */
    private void inizializzaCanale() throws CanaleException {
        canale = new CanaleSrvZMQ();
        canale.apri();
    }

    /**
     * Inizializza la connessione con il data base SQL
     * @throws SQLException
     */
    private void inizializzaDB() throws SQLException {
        GestoreDB.apriConn();
    }

    /**
     * Attiva la ricezione dei messaggi sul canale di comunicazione
     */
    private void attivaRicezioneGestioneMessaggi() {
        while (!terminato) {
            try {
                String richiesta = canale.ricevi();

                log.debug("Ricevuto nuovo messaggio: " + richiesta);

                String risposta = rpcServer.gestisci(richiesta);

                canale.rispondi(risposta);

                log.debug("Invio nuovo messaggio: " + risposta);

            }catch (CanaleException e) {
                if (e.getErr() != CanaleErr.TERM) {
                    log.error("Errore canale: " + e.getErr().toString());
                }
            }
            //Codice che non verra' mai raggiunto
        }
    }
}

