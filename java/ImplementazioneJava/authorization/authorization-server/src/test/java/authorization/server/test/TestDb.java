package authorization.server.test;

import java.sql.SQLException;

import org.junit.Test;

import authorization.server.core.gestori.GestoreDB;
import authorization.shared.dati.Risorsa;
import authorization.shared.eccezioni.RisorsaGiaEsistenteException;

public class TestDb {

    /**
     * Connessione db down o host non raggiungibile
     * @throws SQLException
     */
    @Test(expected = SQLException.class)
    public void connDbKo() throws SQLException {
    	//Apro la connessione ad un indirizzo non valido
        GestoreDB.apriConn("1234567:3360",""); 
    }
    
    /**
     * Cerco di eseguire una operazione senza aver creato prima una connessione al db
     * @throws SQLException
     * @throws RisorsaGiaEsistenteException
     */
    @Test(expected = SQLException.class)
    public void  querySenzaConn() throws SQLException, RisorsaGiaEsistenteException {
        GestoreDB.getInstance().aggiungiRisorsa(new Risorsa("id","prova", 3, "indirizzo"));
    }
}
