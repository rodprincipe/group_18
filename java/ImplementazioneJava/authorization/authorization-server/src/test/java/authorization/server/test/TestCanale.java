package authorization.server.test;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import authorization.server.canale.CanaleSrv;
import authorization.server.canale.CanaleException;
import authorization.server.canale.CanaleException.CanaleErr;
import authorization.server.canale.CanaleSrvZMQ;

class TestCanale {

	@Test
	public void indirzzoOccupato() {
		try {
			CanaleSrv c1 = new CanaleSrvZMQ();
			CanaleSrv c2 = new CanaleSrvZMQ();
			c1.apri();
			c2.apri();
		} catch (CanaleException e) {
			assertEquals(e.getErr(), CanaleErr.INDKO);
		}finally {
		}
	}
	
}
