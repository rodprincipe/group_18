package authorization.server.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.time.Duration;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import authorization.server.core.ServitoreServer;
import authorization.server.core.gestori.GestoreDB;
import authorization.shared.dati.Risorsa;
import authorization.shared.dati.Token;
import authorization.shared.eccezioni.AutorizzazioneNonEsistenteException;
import authorization.shared.eccezioni.AutorizzazioneRevocataException;
import authorization.shared.eccezioni.AutorizzazioneScadutaException;
import authorization.shared.eccezioni.LivelloInsufficienteException;
import authorization.shared.eccezioni.ParametroVuoto;
import authorization.shared.eccezioni.RisorsaGiaEsistenteException;
import authorization.shared.eccezioni.RisorsaNonEsistenteException;
import authorization.shared.eccezioni.TokenNonEsistenteException;
import authorization.shared.eccezioni.TokenScadutoException;
import authorization.shared.eccezioni.UtenteNonEsistenteException;
import authorization.shared.utility.CalcolatoreTempo;

public class TestServitore {
    
    /**
     * Creo la connessione al db per tutti i test
     * @throws SQLException
     */
    @BeforeClass
    public static void apriConn() throws SQLException {
        GestoreDB.apriConn("localhost:3306","authorization_test");
    }
    
    @Test(expected = UtenteNonEsistenteException.class)
    public void nuovaAutUserKO() throws UtenteNonEsistenteException, SQLException, ParametroVuoto {
        ServitoreServer.getInstance().generaChiave("utenteNE", 2, CalcolatoreTempo.adessoPiu(24));
    }
    
    @Test(expected = AutorizzazioneNonEsistenteException.class)
    public void revocaAutNE() throws AutorizzazioneNonEsistenteException, SQLException, ParametroVuoto {
        ServitoreServer.getInstance().revocaAutorizzazione("chiaveAutorizzazioneNE");
    }

    @Test(expected = AutorizzazioneNonEsistenteException.class)
    public void nuovoTokenAutNE() throws AutorizzazioneNonEsistenteException, RisorsaNonEsistenteException, AutorizzazioneScadutaException, SQLException, AutorizzazioneRevocataException, LivelloInsufficienteException, ParametroVuoto {
        ServitoreServer.getInstance().generaToken("chiaveAutorizzazioneNonEsistente", "risorsaDiProvaLiv3");
    }
    
    @Test(expected = RisorsaNonEsistenteException.class)
    public void nuovoTokenRisNE() throws AutorizzazioneNonEsistenteException, RisorsaNonEsistenteException, AutorizzazioneScadutaException, SQLException, AutorizzazioneRevocataException, LivelloInsufficienteException, ParametroVuoto {
        ServitoreServer.getInstance().generaToken("chiaveDiProva", "idRisorsaNonEsistente");
    }

    @Test(expected = AutorizzazioneScadutaException.class)
    public void nuovoTokenAutScad() throws UtenteNonEsistenteException, SQLException, AutorizzazioneNonEsistenteException, RisorsaNonEsistenteException, AutorizzazioneScadutaException, LivelloInsufficienteException, AutorizzazioneRevocataException, ParametroVuoto {
        String chiave = ServitoreServer.getInstance().generaChiave("utenteDiProva", 2, CalcolatoreTempo.adessoMeno(1));
        ServitoreServer.getInstance().generaToken(chiave, "risorsaDiProvaLiv3");
    }
    
    @Test(expected = LivelloInsufficienteException.class)
    public void nuovoTokenAutInsuff() throws UtenteNonEsistenteException, SQLException, AutorizzazioneNonEsistenteException, RisorsaNonEsistenteException, AutorizzazioneScadutaException, LivelloInsufficienteException, AutorizzazioneRevocataException, ParametroVuoto {
        String chiave = ServitoreServer.getInstance().generaChiave("utenteDiProva", 4, CalcolatoreTempo.adessoPiu(1));
        ServitoreServer.getInstance().generaToken(chiave, "risorsaDiProvaLiv3");
    }
    
    @Test(expected = AutorizzazioneRevocataException.class)
    public void nuovoTokenAutRev() throws UtenteNonEsistenteException, SQLException, AutorizzazioneNonEsistenteException, RisorsaNonEsistenteException, AutorizzazioneScadutaException, LivelloInsufficienteException, AutorizzazioneRevocataException, ParametroVuoto {
        String chiave = ServitoreServer.getInstance().generaChiave("utenteDiProva", 2, CalcolatoreTempo.adessoPiu(1));
        ServitoreServer.getInstance().revocaAutorizzazione(chiave);
        ServitoreServer.getInstance().generaToken(chiave, "risorsaDiProvaLiv3");
    }

    @Test(expected = TokenScadutoException.class)
    public void verificaTokenScaduto() throws TokenScadutoException, TokenNonEsistenteException, SQLException, ParametroVuoto {
        ServitoreServer.getInstance().verificaToken(new Token("tokenScaduto", null, null, null));
    }
    
    @Test(expected = TokenNonEsistenteException.class)
    public void verificaTokenNE() throws TokenScadutoException, TokenNonEsistenteException, SQLException, ParametroVuoto {
        ServitoreServer.getInstance().verificaToken(new Token("tokenNonEsistente", null, null, null));
    }
    
    @Test(expected = RisorsaGiaEsistenteException.class)
    public void nuovaRisKO() throws SQLException, RisorsaGiaEsistenteException {
        ServitoreServer.getInstance().aggiungiRisorsa(new Risorsa("risorsaDiProvaLiv3", "descrizione", 1, "indirizzo"));
    }
    
    @Test
    public void nuovaRisOK() throws SQLException, RisorsaGiaEsistenteException {
        Risorsa r = new Risorsa("risorsaDel"+CalcolatoreTempo.adesso().toString().replaceAll(" ", "_"), "risorsa di prova creata il "+CalcolatoreTempo.adesso(),1, "indirizz");
        ServitoreServer.getInstance().aggiungiRisorsa(r);
        assertEquals(GestoreDB.getInstance().cercaRisorsa(r.getIdRisorsa()).getIdRisorsa(),r.getIdRisorsa());
    }
    
    @Test
    public void cancellaRis() throws SQLException, RisorsaNonEsistenteException, RisorsaGiaEsistenteException {
        Risorsa r = new Risorsa("risorsaDel"+CalcolatoreTempo.adesso().toString().replaceAll(" ", "_"), "risorsa di prova creata il "+CalcolatoreTempo.adesso(),1, "indirizz");
        try {
        	ServitoreServer.getInstance().aggiungiRisorsa(r);
        	ServitoreServer.getInstance().rimuoviRisorsa(r.getIdRisorsa());
        	assertNull(GestoreDB.getInstance().cercaRisorsa(r.getIdRisorsa()));
        }catch(SQLException e) {
        	e.printStackTrace();
        	fail();
        }
    }
    
    @Test
    public void sequenzaOperazioni() throws UtenteNonEsistenteException, SQLException, AutorizzazioneNonEsistenteException, RisorsaNonEsistenteException, AutorizzazioneScadutaException, LivelloInsufficienteException, TokenScadutoException, TokenNonEsistenteException, AutorizzazioneRevocataException, ParametroVuoto {
        //Creo una nuova autorizzazione
    	String chiave = ServitoreServer.getInstance().generaChiave("utenteDiProva", 3, CalcolatoreTempo.adessoPiu(12));
        assertTrue(GestoreDB.getInstance().cercaAutorizzazione(chiave)!=null);
        //Richiedo il token di accesso ad una risorsa tramite l'autorizzazione appena creata
        Token token = ServitoreServer.getInstance().generaToken(chiave, "risorsaDiProvaLiv3");
        assertTrue(GestoreDB.getInstance().cercaToken(token.getIdToken())!=null);
        //Verifico che il token non abbia durata maggiore di 24 h
        Duration tempoRimanenteToken = Duration.parse(ServitoreServer.getInstance().verificaToken(token));
        assertTrue(tempoRimanenteToken.compareTo(Duration.ofDays(1))<0);
        //Revoco l'autorizzazione
        assertFalse(GestoreDB.getInstance().cercaAutorizzazione(chiave).getRevocata());
        ServitoreServer.getInstance().revocaAutorizzazione(chiave);
        assertTrue(GestoreDB.getInstance().cercaAutorizzazione(chiave).getRevocata());
        //Verifico che l'autorizzazione sia revocata
        assertTrue(GestoreDB.getInstance().cercaAutorizzazione(chiave).getRevocata());
    }
    
    @AfterClass
    public static void chiudiConn() throws SQLException {
        GestoreDB.chiudiConn();
    }
    
}