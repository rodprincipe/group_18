package jsonRpc.client.costruttori;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Per la costruzione di una nuova richiesta vedi {@link CostruttoreNotifiche}.<br>
 * Impostati tutti i membri della notifica procedere con {@link #aggiungi()} per aggiungere la richiesta.
 */
public abstract class CostruttoreNotifichePerLotto {

    /**
     * Costruttore della notifica classico
     */
    private CostruttoreNotifiche costruttoreNotifica;
    
    /**
     * Permette la creazione di notifiche da aggiungere al lotto.
     * @param mapper jackson mapper per processare i json
     */
    CostruttoreNotifichePerLotto(ObjectMapper mapper) {
        costruttoreNotifica = new CostruttoreNotifiche(null, mapper);
    }

    /**
     * Vedi {@link CostruttoreNotifiche#metodo(String)} per l'utilizzo
     */
    public CostruttoreNotifichePerLotto metodo(String nomeMetodo) {
        costruttoreNotifica = costruttoreNotifica.metodo(nomeMetodo);
        return this;
    }

    /**
     * Vedi {@link CostruttoreNotifiche#parametro(String, Object)} per l'utilizzo
     */
    public CostruttoreNotifichePerLotto parametro(String nomeParametro, Object parametro) {
        costruttoreNotifica = costruttoreNotifica.parametro(nomeParametro, parametro);
        return this;
    }

    /**
     * Vedi {@link CostruttoreNotifiche#parametri(Object...)} per l'utilizzo
     */
    public CostruttoreNotifichePerLotto parametri(Object... valori) {
        costruttoreNotifica = costruttoreNotifica.parametri(valori);
        return this;
    }
    
    /**
     * Vedi {@link CostruttoreNotifiche#notificaJSON()} per l'utilizzo
     */
    ObjectNode notificaJSON() {
        return costruttoreNotifica.notificaJSON();
    }

    /**
     * Aggiunge la notifica creata al lotto di richieste.<br>
     * <b>Implementare alla creazione di un nuovo {@link CostruttoreRichiestePerLotto}</b>
     * <br>
     * <br>
     * Esempio:
     * <pre>
     * CostruttoreNotifichePerLotto cN =
     *      new CostruttoreNotifichePerLotto(canale, mapper) {
     *  
     *          //Override del metodo aggiungi
     *          public CostruttoreLottoDiRichieste aggiungi() {
     *              lotto.add(notificaJSON());
     *              return new CostruttoreLottoDiRichieste(canale, mapper, lotto, costruttoriRisposte);
     *          }  
     *      }
     * </pre>
     * @return lotto di richieste con nuova richiesta aggiuta
     */
    public abstract CostruttoreLottoDiRichieste aggiungi();
    
}
