package jsonRpc.client;

import org.jetbrains.annotations.NotNull;

import com.fasterxml.jackson.databind.ObjectMapper;

import jsonRpc.client.costruttori.CostruttoreLottoDiRichieste;
import jsonRpc.client.costruttori.CostruttoreNotifiche;
import jsonRpc.client.costruttori.CostruttoreRichieste;

/**
 * Client JSON-RPC, tramite factory crea richieste e notifiche secondo lo standard JSON-RPC.<br>
 * Parametri fondamentali sono il {@link Canale} e il mapper {@link ObjectMapper} per la gestione dei JSON.
 */
public class JsonRpcClient {

    /**
     * Canale per l'inolto delle richieste e per la ricezione delle relative risposte
     */
    @NotNull
    private Canale canale;

    /**
     * JSON mapper per la conversione dei JSON in tipi java
     */
    @NotNull
    private ObjectMapper mapper;

    /**
     * Costruisce un client con uno specifico {@link Canale}
     * @param canale per l'inolto delle richieste
     */
    public JsonRpcClient(@NotNull Canale canale) {
        this(canale, new ObjectMapper());
    }

    /**
     * Costruisce un client con uno specifico {@link Canale} e {@link ObjectMapper} personalizzato
     * @param canale per l'inolto delle richieste
     * @param mapper    JSON mapper per gestione JSON
     */
    public JsonRpcClient(@NotNull Canale canale, @NotNull ObjectMapper mapper) {
        this.canale = canale;
        this.mapper = mapper;
    }

    /**
     * Genera un costruttore di richieste JSON-RPC
     * 
     * @return costruttore delle richieste
     */
    @NotNull
    public CostruttoreRichieste<Object> creaRichiesta() {
        return new CostruttoreRichieste<Object>(canale, mapper);
    }

    /**
     * Genera un costruttore di notifiche JSON-RPC 
     *
     * @return costruttore delle notifiche
     */
    @NotNull
    public CostruttoreNotifiche creaNotifica() {
        return new CostruttoreNotifiche(canale, mapper);
    }

    /**
     * Crea un costruttore di lotti di richieste JSON-RPC nuovo 
     * @return costruttore di lotti di richieste
     */
    @NotNull
    public CostruttoreLottoDiRichieste creaLottoRichieste() {
        return new CostruttoreLottoDiRichieste(canale, mapper);
    }

}
