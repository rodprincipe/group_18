package jsonRpc.client.costruttori;

import java.io.IOException;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.databind.node.ValueNode;

import jsonRpc.client.Canale;
import jsonRpc.client.eccezioni.JsonRpcException;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * <p>
 * Fornisce un costruttore di richieste JSON-RPC.<br>
 * Il quale crea la richiesta, setta il tipo del oggetto ritornato atteso
 * e inoltra la richiesta gestendo gli eventuali errori.
 * </p>
 * <p>
 * Delega la gestione dei JSON alla libreria Jackson,
 * tramite l'oggetto {@link ObjectMapper} viene creata la richiesta
 * e inoltrata tramite il {@link jsonRpc.client.Canale}.
 * </p>
 */
public class CostruttoreRichieste<T> extends CostruttoreRichiesteAstratto {

    /**
     * Metodo della richiesta JSON-RPC 
     */
    @NotNull
    private final String metodo;

    /**
     * Id JSON della richiesta JSON-RPC
     */
    @NotNull
    private final ValueNode id;

    /**
     * Parametri della richiesta JSON-RPC strutturati con oggetto JSON
     */
    @NotNull
    private final ObjectNode parametriOgg;

    /**
     * Parametri della richiesta JSON-RPC strutturati con array JSON
     */
    @NotNull
    private final ArrayNode parametriArr;

    /**
     * Tipo generico JSON atteso come risultato della richiesta JSON-RPC
     */
    @NotNull
    private JavaType tipoReturn;

    /**
     * Istanzia un nuovo costruttore di richieste JSON-RPC vuoto
     *
     * @param canale front-end che si occupera' di inoltrare il messaggio
     * @param mapper    mapper per processare la richiesta JSON
     */
    public CostruttoreRichieste(@NotNull Canale canale, @NotNull ObjectMapper mapper) {
        super(canale, mapper);
        id = NullNode.instance;
        parametriOgg = mapper.createObjectNode();
        parametriArr = mapper.createArrayNode();
        metodo = "";
        tipoReturn = mapper.constructType(Object.class);
    }

    /**
     * Istanzia un nuovo costrutore di richieste JSON-RPC, completo di tutti i parametri: 
     * gli elementi della richiesta, mapper per gestire i JSON, canale che inoltrerà i messaggi, tipo di ritorno della richiesta. 
     *
     * @param canale    
     * @param mapper      
     * @param metodo       
     * @param id           
     * @param parametriJSON 
     * @param arrayParams  
     * @param tipoReturn     
     */
    private CostruttoreRichieste(@NotNull Canale canale, @NotNull ObjectMapper mapper, @NotNull String metodo,
            @NotNull ValueNode id, @NotNull ObjectNode parametriJSON, @NotNull ArrayNode arrayParams,
            @NotNull JavaType tipoReturn) {
        super(canale, mapper);
        this.metodo = metodo;
        this.id = id;
        this.parametriOgg = parametriJSON;
        this.parametriArr = arrayParams;
        this.tipoReturn = tipoReturn;
    }

    /**
     * Id con valore json null?
     * @return <code>true</code> se id istanziato con valore null Json altirmenti <code>false</code>
     */
    boolean idNull() {
        return id.isNull();
    }

    /**
     * Imposta id della richiesta JSON-RPC come long
     *
     * @param id - identificativo della richiesta come Long
     * @return nuovo costruttore della richiesta con id settato
     */
    @NotNull
    public CostruttoreRichieste<T> id(@NotNull Long id) {
        return new CostruttoreRichieste<T>(canale, mapper, metodo, new LongNode(id), parametriOgg, parametriArr, tipoReturn);
    }

    /**
     * Imposta id della richiesta JSON-RPC come intero
     *
     * @param id - identificativo della richiesta come stringa
     * @return nuovo costruttore della richiesta con id settato
     */
    @NotNull
    public CostruttoreRichieste<T> id(@NotNull Integer id) {
        return new CostruttoreRichieste<T>(canale, mapper, metodo, new IntNode(id), parametriOgg, parametriArr, tipoReturn);
    }

    /**
     * Imposta id della richiesta JSON-RPC come stringa
     *
     * @param id - identificativo della richiesta come stringa
     * @return nuovo costruttore della richiesta con id settato
     */
    @NotNull
    public CostruttoreRichieste<T> id(@NotNull String id) {
        return new CostruttoreRichieste<T>(canale, mapper, metodo, new TextNode(id), parametriOgg, parametriArr, tipoReturn);
    }

    /**
     * Imposta metodo della richiesta JSON-RPC
     *
     * @param nomeMetodo - stringa con il nome del metodo
     * @return costruttore della richiesta con metodo settato
     */
    @NotNull
    public CostruttoreRichieste<T> metodo(@NotNull String nomeMetodo) {
        return new CostruttoreRichieste<T>(canale, mapper, nomeMetodo, id, parametriOgg, parametriArr, tipoReturn);
    }

    /**
     * Aggiunge un parametro alla richiesta. <br>
     * Aggiungere ogni singolo parametro con relativo nome.<br>
     * La chiamata del metodo verra' effettuata con riferimento ai parametri <u>per nome</u>.<br>
     * Aggregazione dei parametri alla richiesta in un oggetto come da specifica JSON-RPC 2.0.<br>
     * <b>Non utilizzare {@link #parametri(Object...)} per dichiarare altri parametri</b>
     *
     * @param nomeParametro  nome del parametro
     * @param parametro oggetto contenente i valori del parametro
     * @return costruttore della richiesta con nuovo parametro settato
     */
    @NotNull
    public CostruttoreRichieste<T> parametro(@NotNull String nomeParametro, @NotNull Object parametro) {
        ObjectNode parametriOggNuovi = parametriOgg.deepCopy();
        parametriOggNuovi.set(nomeParametro, mapper.valueToTree(parametro));
        return new CostruttoreRichieste<T>(canale, mapper, metodo, id, parametriOggNuovi, parametriArr, tipoReturn);
    }

    /**
     * Setta l'array dei parametri per la richiesta in costruzione.<br>
     * Fornire array di oggetti contenente i valori dei parametri del metodo, <br>
     * seguendo l'ordine previsto dal server dato che chiamata del metodo verra' <br>
     * effettuata con riferimento ai parametri <u>per posizione</u>.<br>
     * Aggregazione dei parametri alla richiesta in un array come da specifica JSON-RPC 2.0.<br>
     * <b>Non utilizzare {@link #parametro(String, Object)} per dichiarare altri parametri</b>
     *
     * @param valori - array di oggetti quali i valori dei parametri, valori ordinati come da chiamata del metodo remoto vuole
     * @return costruttore della richiesta con nuovo array di parametri settato
     */
    @NotNull
    public CostruttoreRichieste<T> parametri(@NotNull Object... valori) {
        return new CostruttoreRichieste<T>(canale, mapper, metodo, id, parametriOgg, daArrayJavaAdArrayJSON(valori), tipoReturn);
    }

    /**
     * Imposta il tipo di ritorno del metodo chiamato tramite la richiesta JSON-RPC  
     *
     * @param tipoReturn classe del oggetto ritornato (NomeClasse.class)
     * @param <R>        tipo generico del oggetto che verra' ritornato
     * @return costruttore della richiesta con tipo di ritorno settato
     */
    @NotNull
    public <R> CostruttoreRichieste<R> ritorna(@NotNull Class<R> tipoReturn) {
        return new CostruttoreRichieste<R>(canale, mapper, metodo, id, parametriOgg, parametriArr, mapper.constructType(tipoReturn));
    }

    /**
     * 
     * Converte la richiesta JSON in stringa e la inoltra attraverso il {@link Canale}, quindi
     * converte la risposta nel tipo aspettato.
     *
     * @return oggetto del tipo atteso {@code <T>} equivalente al risultato della richiesta JSON-RPC 2.0
     * @throws JsonRpcException      nel caso in cui il server restituisce un messaggio d'errore
     * @throws IllegalStateException se la risposta e' {@code null}. Metodo remoto proabiblmente void, utilizza {@link #eseguiNull()}
     */
    @NotNull
    public T esegui() {
        T risultato = convertiRisposta(parseRisposta(inoltraRichiesta(convertiRichiesta(richiestaJSON()))));
        if (risultato == null) {
            throw new IllegalStateException("Risposta null. Utilizza eseguiNull().");
        }
        return risultato;
    }

    /**
     * Esegue una richiesta e la converte nel valore null del tipo aspettato.<br>
     *
     * @return null del tipo atteso
     * @throws JsonRpcException se il server risponde con un errore
     */
    @Nullable
    public T eseguiNull() { 
        return convertiRisposta(parseRisposta(convertiRichiesta(richiestaJSON())));
    }

    /**
     * Scompone la risposta negli elementi Json e controlla che rispetti le specifiche JSON-RPC 2.0,
     * quindi converte il risultato nel tipo atteso.
     * @param risposta come Json
     * @return risultato del tipo apettato <T>
     */
    @Nullable
    T convertiRisposta(JsonNode rispostaJSON) {

        JsonNode risultato = rispostaJSON.get(RISULTATO);
        JsonNode errore = rispostaJSON.get(ERRORE);
        JsonNode versione = rispostaJSON.get(JSONRPC);
        JsonNode id = rispostaJSON.get(ID);

        if (versione == null) {
            throw new IllegalStateException("Non e' una risposta JSON-RPC 2.: " + rispostaJSON);
        }
        if (!versione.asText().equals(VERSIONE_2_0)) {
            throw new IllegalStateException("Versione errata nella risposta JSON-RPC " + rispostaJSON);
        }
        if (id == null) {
            throw new IllegalStateException("Id indefinito nella risposta JSON-RPC " + rispostaJSON);
        }

        if (errore == null) {
            if (risultato != null) {
                return mapper.convertValue(risultato, tipoReturn);
            } else {
                throw new IllegalStateException("Risposta senza risultato e senza errore: " + rispostaJSON);
            }
        } else {
            ErroreJsonRpc errorMessage;
            try {
                errorMessage = mapper.treeToValue(errore, ErroreJsonRpc.class);
            } catch (JsonProcessingException e) {
                throw new IllegalStateException("Errore nel parsing del errore JSON-RPC: " + errore, e);
            }
            throw new JsonRpcException(errorMessage);
        }
    }

    /**
     * Semplice parse della risposta
     * @param risposta come stringa
     * @return risposta come Json
     */
    JsonNode parseRisposta(String risposta) { 
        try {
            return mapper.readTree(risposta);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Errore nel parsing della risposta JSON-RPC: " + risposta, e);
        } catch (IOException e) {
            throw new IllegalStateException("Errore IO durante la gestione della risposta; "+e.getMessage(), e);
        }
    }

    /**
     * Inoltra attraverso il {@link Canale} la richiesta e restituisce la risposta
     * @param richiesta come stringa
     * @return risposta come stringa
     */
    String inoltraRichiesta(String richiesta) {
        String rispostaStringa;
        try {
            rispostaStringa = canale.inoltra(richiesta);
        } catch (IOException e) {
            throw new IllegalStateException("Errore di comunicazione; "+e.getMessage(), e);
        }
        return rispostaStringa;
    }

    /**
     * Converte una richiesta Json in stringa cosi da poterla inoltrare
     * @param richiesta come oggetto json ({@link ObjectNode})
     * @return richiesta come stringa secondo lo standard JSON-RPC 2.0
     */
    String convertiRichiesta(ObjectNode richiesta) {
        String richiestaStringa;
        try {
            richiestaStringa = mapper.writeValueAsString(richiesta);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Impossibile convertire " + richiesta + " in JSON", e);
        }
        return richiestaStringa;
    }

    /**
     * Produce la richiesta come oggetto JSON contenente tutti gli elementi secondo specifica JSO-RPC 2.0.<br>
     * Come da specifiche JSON-RPC 2.0 la richiesta deve id e metodo, in caso contrario lancia un'eccezione.
     * @return oggetto JSON
     * @throws IllegalAccessException se la richiesta non rispetta le specifiche 
     */
    ObjectNode richiestaJSON() {
        if(id.isNull()) {
            throw new IllegalArgumentException("Richiesta senza id");
        }
        return componiRichiesta();
    }

    /**
     * Aggrega tutti gli elementi allegati alla richiesta e produce l'oggetto corrispondente,
     * che sarà da convertire in stringa per poterlo inviare.
     * @return oggetto Json della richiesta
     */
    ObjectNode componiRichiesta() {
        return componiRichiestaAstr(id, metodo, getParametri());
    }
    
    /**
     * Recupero i parametri indipendentemente dal modo in cui gli ho strutturati per la chiamata (per nome [oggetto] o per posizione [array])
     * 
     * @return {@link JsonNode} contenente i parametri
     * @throws IllegalArgumentException se i parametri sono sia "per posizione" che "per nome"
     */
    @NotNull
    private JsonNode getParametri() {
        if(parametriOgg.size()==0 && parametriArr.size()==0)
            return NullNode.getInstance();
        
        if (parametriOgg.size() > 0) {
            if (parametriArr.size() > 0) {
                throw new IllegalArgumentException("Parametri settati sia come oggetto che come array");
            }
            return parametriOgg;
        }
        return parametriArr;
    }

}
