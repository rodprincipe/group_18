package jsonRpc.client.costruttori;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Per la costruzione di una nuova richiesta vedi {@link CostruttoreRichieste}.<br>
 * Impostati tutti i membri della richiesta procedere con {@link #aggiungi()} per aggiungere la richiesta.
 */
public abstract class CostruttoreRichiestePerLotto {

    /**
     * Costruttore della richiesta classico
     */
    private CostruttoreRichieste<?> costruttoreRichiesta;

    /**
     * Permette la creazione di notifiche da aggiungere al lotto.
     * @param mapper jackson mapper per processare i json
     */
    public CostruttoreRichiestePerLotto(ObjectMapper mapper) {
        costruttoreRichiesta = new CostruttoreRichieste<Object>(null,mapper);
    }

    /**
     * Costruttore utilizzato esclusivamente per gestire gli errori
     * @param mapper per la gestione dei json
     * @return costruttore di errori istanza 
     * @throws IllegalStateException se cerco di aggiungere questo costruttore
     */
    public static CostruttoreRichiestePerLotto costruttoreErrori(ObjectMapper mapper) {
        return new CostruttoreRichiestePerLotto(mapper) {
            @Override
            public CostruttoreLottoDiRichieste aggiungi() {
                throw new IllegalStateException("Non puoi aggiungere questo costruttore");
            }
        };
    }

    /**
     * Vedi {@link CostruttoreRichieste#id(Long)} per l'utilizzo
     */
    public CostruttoreRichiestePerLotto id(Long id) {
        this.costruttoreRichiesta = costruttoreRichiesta.id(id);
        return this;
    }

    /**
     * Vedi {@link CostruttoreRichieste#id(Integer)} per l'utilizzo
     */
    public CostruttoreRichiestePerLotto id(Integer id) {
        this.costruttoreRichiesta = costruttoreRichiesta.id(id);
        return this;
    }

    /**
     * Vedi {@link CostruttoreRichieste#id(String)} per l'utilizzo
     */
    public CostruttoreRichiestePerLotto id(String id) {
        this.costruttoreRichiesta = costruttoreRichiesta.id(id);
        return this;
    }

    /**
     * Vedi {@link CostruttoreRichieste#metodo(String)} per l'utilizzo
     */
    public CostruttoreRichiestePerLotto metodo(String nomeMetodo) {
        this.costruttoreRichiesta = costruttoreRichiesta.metodo(nomeMetodo);
        return this;
    }
    
    /**
     * Vedi {@link CostruttoreRichieste#parametro(String, Object)} per l'utilizzo
     */
    public CostruttoreRichiestePerLotto parametro(String nomeParametro, Object parametro) {
        this.costruttoreRichiesta = costruttoreRichiesta.parametro(nomeParametro, parametro);
        return this;
    }

    /**
     * Vedi {@link CostruttoreRichieste#parametri(Object...)} per l'utilizzo
     */
    public CostruttoreRichiestePerLotto parametri(Object... valori) {
        this.costruttoreRichiesta = costruttoreRichiesta.parametri(valori);
        return this;
    }

    /**
     * Vedi {@link CostruttoreRichieste#ritorna(Class)} per l'utilizzo
     */
    public CostruttoreRichiestePerLotto ritorna(Class<?> tipoReturn) {
        this.costruttoreRichiesta = costruttoreRichiesta.ritorna(tipoReturn);
        return this;
    }

    /**
     * Vedi {@link CostruttoreRichieste#convertiRisposta(JsonNode)} per l'utilizzo
     */
    Object convertiRisposta(JsonNode risposta) {
        return costruttoreRichiesta.convertiRisposta(risposta);
    }

    /**
     * Vedi {@link CostruttoreRichieste#richiestaJSON()} per l'utilizzo
     */
    ObjectNode richiestaJSON() {
        return costruttoreRichiesta.richiestaJSON();
    }

    /**
     * Aggiunge la richiesta creata al lotto di richieste.<br>
     * <b>Implementare alla creazione di un nuovo {@link CostruttoreRichiestePerLotto}</b>
     * <br>
     * <br>
     * Esempio:
     * <pre>
     * CostruttoreRichiestePerLotto cR = 
     *      new CostruttoreRichiestePerLotto(canale, mapper) {
     *      
     *          //Override del metodo aggiungi
     *          public CostruttoreLottoDiRichieste aggiungi() {
     *              lotto.add(richiestaJSON());
     *              return new CostruttoreLottoDiRichieste(canale, mapper, lotto, costruttoriRisposte);
     *          }
     *          
     *      }
     * </pre>
     * @return lotto di richieste con nuova richiesta aggiuta
     */
    public abstract CostruttoreLottoDiRichieste aggiungi();


}
