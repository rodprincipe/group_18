package jsonRpc.client.eccezioni;

import java.util.List;
import java.util.Map;

import org.jetbrains.annotations.NotNull;

import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Eccezione lanciata nel caso in cui il server ritorna per qualche richiesta un errore
 */
public class JsonRpcLottoException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * Risultati delle richieste con successo
     */
    @NotNull
    private Map<String, Object> successi;

    /**
     * Risultati delle richieste con errore
     */
    @NotNull
    private List<ErroreJsonRpc> errori;

    public JsonRpcLottoException(@NotNull Map<String, Object> successi, @NotNull List<ErroreJsonRpc> errori) {
        this.successi = successi;
        this.errori = errori;
    }

    @NotNull
    public Map<String, Object> getSuccessi() {
        return successi;
    }

    @NotNull
    public List<ErroreJsonRpc> getErrori() {
        return errori;
    }
}
