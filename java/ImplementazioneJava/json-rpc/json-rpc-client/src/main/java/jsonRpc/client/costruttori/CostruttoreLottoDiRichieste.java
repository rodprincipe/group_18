/**
 * 
 */
package jsonRpc.client.costruttori;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jetbrains.annotations.NotNull;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import jsonRpc.client.Canale;
import jsonRpc.client.eccezioni.JsonRpcException;
import jsonRpc.client.eccezioni.JsonRpcLottoException;
import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * <p>
 * Fornisce un costruttore che permette la creazione di lotti di richieste JSON-RPC.<br>
 * Permette la aggiunta di richieste e di notifiche.
 * </p>
 * <p>
 * Delega il costruttore di richieste {@link CostruttoreRichieste} per la creazione di richieste
 * e il {@link CostruttoreNotifiche} per la creazione di notifiche.
 * </p>
 * @param <T>
 *  
 */
public class CostruttoreLottoDiRichieste extends CostruttoreRichiesteAstratto{

    /**
     * Lotto di richieste ben formate
     */
    private ArrayNode lotto;

    /**
     * Costruttori per convertire i risultati delle richieste nel tipo di oggetto atteso
     */
    private Map<String, CostruttoreRichiestePerLotto> costruttoriRisposte;

    /**
     * Crea un nuovo costruttore di lotti di richieste.<br>
     * Delega al canale di comunicazione l'inoltro dei messaggi e al mapper il processamento dei json.
     * @param canale di comunicazione per inoltrare i messaggi
     * @param mapper strumento per processare i json
     */
    public CostruttoreLottoDiRichieste(Canale canale, ObjectMapper mapper) {
        super(canale, mapper);
        lotto = mapper.createArrayNode();
        costruttoriRisposte = new HashMap<String,CostruttoreRichiestePerLotto>();
        //Costruttore di errore alla chiave null
        costruttoriRisposte.put(null, CostruttoreRichiestePerLotto.costruttoreErrori(mapper));
    }

    /**
     * Produce un creatore di lotti di richieste con i paramentri definiti: canale, mapper, lotto di richieste, costruttori di richieste.
     * @param canale di comunicazione per inoltrare i messaggi
     * @param mapper strumento per processare i json
     * @param nuovoLotto lotto di richieste JSON-RPC 2.0 ben formato
     * @param costruttoriRisposteNuovi mappa dei costruttori di risposte in base con chiave l'id della risorrsa
     */
    private CostruttoreLottoDiRichieste(Canale canale, ObjectMapper mapper, ArrayNode nuovoLotto, Map<String, CostruttoreRichiestePerLotto> costruttoriRisposteNuovi) {
        super(canale, mapper);
        lotto = nuovoLotto;
        costruttoriRisposte = costruttoriRisposteNuovi;
    }

    /**
     * Crea una richiesta JSON-RPC da aggiungere al lotto.
     * @return costruttore della richiesta
     */
    @NotNull
    public <T> CostruttoreRichiestePerLotto creaRichiesta(){
        CostruttoreRichiestePerLotto costruttoreRichiesta = new CostruttoreRichiestePerLotto(mapper) {

            public CostruttoreLottoDiRichieste aggiungi() {
                ObjectNode richiesta = richiestaJSON();
                String id = idStringa(richiesta.get(ID));
                lotto.add(richiesta);
                costruttoriRisposte.put(id , this);
                return new CostruttoreLottoDiRichieste(canale, mapper, lotto, costruttoriRisposte);
            }
        }; 
        return costruttoreRichiesta;
    }

    /**
     * Crea una notifica JSON-RPC da aggiungere al lotto.
     * @return costruttore della notifica
     */
    @NotNull
    public CostruttoreNotifichePerLotto creaNotifica() {
        CostruttoreNotifichePerLotto costruttoreNotifica= new CostruttoreNotifichePerLotto(mapper) {

            @Override
            public CostruttoreLottoDiRichieste aggiungi() {
                lotto.add(notificaJSON());
                return new CostruttoreLottoDiRichieste(canale, mapper, lotto, costruttoriRisposte);
            }
        }; 
        return costruttoreNotifica;
    }

    /**
     * Esegue la richiesta.<br>
     * Converte le richieste JSON in stringa e inoltra il lotto intero attraverso il {@link Canale}, quindi
     * converte le risposte nel tipo atteso.
     * @return Map <id richiesta, risultato>    
     */
    @NotNull
    public Map<String, Object> esegui(){
        Map<String, Object> risultato = convertiRisposta(inoltra(richiestaInStringa()));

        if(!risultato.isEmpty()) {
            //Risultato non vuoto
            return risultato;
        }
        else {
            if(costruttoriRisposte.isEmpty()) {
                //Risultato vuoto e lotto di sole notifiche
                return risultato;
            }else {
                throw new IllegalArgumentException("Risposta vuota con risposte non processate");
            }
        }
    }

    /**
     * Scompone la risposta negli elementi Json e controlla che rispetti le specifiche JSON-RPC 2.0,
     * quindi converte il risultato nel tipo atteso.
     *
     * @param rispostaStringa risposta json come stringa
     * @return mappa <id della richiesta, risultato di successo> dei risultati ottenuti
     */
    private Map<String, Object> convertiRisposta(String rispostaStringa) {
        Map<String, Object> successi = new HashMap<String,Object>();
        List<ErroreJsonRpc> errori = new ArrayList<ErroreJsonRpc>();
        if(rispostaStringa.isEmpty()) {

            // Rimuovo il costruttore degli errori
            costruttoriRisposte.remove(null); 

            //Risposta vuota (lotto contenente solo notifiche, restituisco mappa vuota)
            return successi;
        }

        ArrayNode arrayRisposte = mapper.createArrayNode();
        try {
            JsonNode lottoRisposteJSON = mapper.readTree(rispostaStringa);
            if(lottoRisposteJSON.isArray())
                arrayRisposte = (ArrayNode) lottoRisposteJSON;
            JsonNode errore = lottoRisposteJSON.get(ERRORE);

            if(!lottoRisposteJSON.isArray()) {
                //Risposta non è array
                if(errore==null) {
                    //Risposta non è nemmeno un errore
                    throw new IllegalStateException("Atteso un array oppure un errore, invece:"+lottoRisposteJSON.getNodeType());
                }else {
                    //Risposta è errore, lancio eccezione JsonRpc
                    ErroreJsonRpc errorMessage = mapper.treeToValue(errore, ErroreJsonRpc.class);
                    throw new JsonRpcException(errorMessage);
                }
            }else {
                //Rispota è array
                if(arrayRisposte.size()==0) {
                    //Risposta è array vuoto, non contemplato da JSON-RPC 2.0
                    throw new IllegalStateException("La risposta è un array vuoto");
                }else {
                    //Risposta array non vuoto, converto i risultati
                    for (JsonNode elemento : arrayRisposte) {
                        if(elemento.isObject()) {
                            try {
                                String id = idStringa(elemento.get(ID));
                                if(!costruttoriRisposte.containsKey(id))
                              throw new IllegalStateException("Non è stata fatta nessuna richiesta con questo id");
                                successi.put(id, costruttoriRisposte.get(id).convertiRisposta(elemento));
                                costruttoriRisposte.remove(id); //Rimuovo il costruttore della risposta dopo averlo utilizzato
                            }catch (NullPointerException e) {
                                throw new IllegalStateException("Id indefinito nella risposta JSON-RPC " + elemento);
                            }catch (JsonRpcException e) {
                                errori.add(e.getMessaggioErrore());
                            }
                        }else
                            throw new IllegalArgumentException("Elemento nel array della risposta che non e' un oggetto");
                    }
                }
            }
        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Errore nel parsing della risposta JSON-RPC: " + rispostaStringa, e);
        } catch (IOException e) {
            throw new IllegalStateException("Errore IO durante la gestione della risposta", e);
        }

        costruttoriRisposte.remove(null); // Rimuovo anche il costruttore degli errori

        if(costruttoriRisposte.isEmpty()) { //Vuoto se ho provveduto a tutte le richieste e non ho avuto errori
            return successi;
        }else{
            if (!errori.isEmpty()) {
                throw new JsonRpcLottoException(successi, errori);
            }else {
                throw new IllegalStateException("Ci sono richieste non risposte ma non ci sono errori");
            }
        }
    }

    /**
     * Converte id della richiesta in stringa.<br>
     * Dopo aver verificato che l'id sia conforme con le specifiche JSON-RPC 2.0 lo converte in stringa
     * @param id come Json 
     * @return id come stringa
     * @throws IllegalArgumentException se l'id non e' stringa, numero o null
     */
    @NotNull
    protected String idStringa(JsonNode id) {
        if (id.isNull())
            return null;
        if (id.isLong()) {
            return String.valueOf(id.longValue());
        } else if (id.isInt()) {
            return String.valueOf(id.intValue());
        } else if (id.isTextual()) {
            return id.textValue();
        }
        throw new IllegalArgumentException("Id non valido " + id);
    }

    /**
     * Inoltra attraverso il {@link Canale} la richiesta e restituisce la risposta
     * @param richiestaStringa richiesta come stringa
     * @return risposta come stringa
     */
    @NotNull
    private String inoltra(String richiestaStringa) {
        String rispostaStringa;
        try {
            rispostaStringa = canale.inoltra(richiestaStringa);
        } catch (IOException e) {
            throw new IllegalStateException("Errore IO durante la gestione della richiesta", e);
        }
        return rispostaStringa;
    }

    /**
     * Compone la richiesta come da lotto costruito e ritorna la stringa della richiesta pronta da inoltrare.
     * @return lotto di richieste come stringa
     */
    @NotNull
    private String richiestaInStringa() {
        String richiestaStringa;
        try {
            richiestaStringa = mapper.writeValueAsString(lotto);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Impossibile convertire il lotto in JSON" + lotto, e);
        }
        return richiestaStringa;
    }

}
