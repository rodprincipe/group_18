package jsonRpc.client.costruttori;

import org.jetbrains.annotations.NotNull;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import jsonRpc.client.Canale;

/**
 * <p>
 * Fornisce un costruttore di notifiche JSON-RPC.
 * </p>
 * <p>
 * Delega il costruttore di richieste {@link CostruttoreRichieste} per creare una richiesta 
 * ma non permette impostare ne un id ne un tipo di ritorno atteso.
 * </p>
 */
public class CostruttoreNotifiche {

    /**
     * Costruttore di richieste
     */
    private CostruttoreRichieste<Void> costruttoreRichiesta;

    /**
     * Crea un costruttore di notifiche JSON-RPC.
     *
     * @param canale di comunicazione sul quale viene inoltrata la richiesta JSON-RPC
     * @param mapper    mapper per la gestione dei JSON
     */
    public CostruttoreNotifiche(@NotNull Canale canale, @NotNull ObjectMapper mapper) {
        costruttoreRichiesta = new CostruttoreRichieste<Void>(canale, mapper);
    }

    /**
     * Assegna un nuovo costruttore di notifiche.
     *
     * @param nuovoCostruttoreRichiesta a new notification request builder
     */
    private CostruttoreNotifiche(CostruttoreRichieste<Void> nuovoCostruttoreRichiesta) {
        this.costruttoreRichiesta = nuovoCostruttoreRichiesta;
    }

    /**
     * Imposta il nome del metodo remoto da chiamare
     *
     * @param nomeMetodo nome del metodo
     * @return nuovo costruttore di notifiche
     */
    @NotNull
    public CostruttoreNotifiche metodo(@NotNull String nomeMetodo) {
        return new CostruttoreNotifiche(costruttoreRichiesta.metodo(nomeMetodo));
    }

    /**
     * Aggiunge un parametro alla richiesta. <br>
     * Aggiungere ogni singolo parametro con relativo nome.<br>
     * La chiamata del metodo verra' effettuata con riferimento ai parametri <u>per nome</u>.<br>
     * Aggregazione dei parametri alla richiesta in un oggetto come da specifica JSON-RPC 2.0.<br>
     * <b>Non utilizzare {@link #parametri(Object...)} per dichiarare altri parametri</b>
     *
     * @param nomeParametro  nome del parametro
     * @param parametro oggetto contenente i valori del parametro
     * @return costruttore della richiesta con nuovo parametro settato

     */
    @NotNull
    public CostruttoreNotifiche parametro(@NotNull String nomeParametro, @NotNull Object parametro) {
        return new CostruttoreNotifiche(costruttoreRichiesta.parametro(nomeParametro, parametro));
    }

    /**
     * Setta l'array dei parametri per la richiesta in costruzione.<br>
     * Fornire array di oggetti contenente i valori dei parametri del metodo, <br>
     * seguendo l'ordine previsto dal server dato che chiamata del metodo verra' <br>
     * effettuata con riferimento ai parametri <u>per posizione</u>.<br>
     * Aggregazione dei parametri alla richiesta in un array come da specifica JSON-RPC 2.0.<br>
     * <b>Non utilizzare {@link #parametro(String, Object)} per dichiarare altri parametri</b>
     *
     * @param valori - array di oggetti quali i valori dei parametri, valori ordinati come da chiamata del metodo remoto vuole
     * @return costruttore della richiesta con nuovo array di parametri settato
     */
    @NotNull
    public CostruttoreNotifiche parametri(@NotNull Object... valori) {
        return new CostruttoreNotifiche(costruttoreRichiesta.parametri(valori));
    }

    /**
     * Inoltra la notifca attraverso il {@link Canale}
     */
    public void esegui() {
        costruttoreRichiesta.inoltraRichiesta(convertiRichiesta(notificaJSON()));
    }

    /**
     * Converte una notifica Json in stringa cosi da poterla inoltrare
     * @param richiesta come oggetto json ({@link ObjectNode})
     * @return richiesta come stringa secondo lo standard JSON-RPC 2.0
     */
    private String convertiRichiesta(ObjectNode notificaJSON) {
        return costruttoreRichiesta.convertiRichiesta(notificaJSON);
    }

    /**
     * Aggrega tutti gli elementi allegati alla notifica e produce l'oggetto corrispondente,
     *  che sarà da convertire in stringa per poterlo inviare.
     * @return oggetto Json della notifica
     */
    ObjectNode notificaJSON() {
        if(!costruttoreRichiesta.idNull())
            throw new IllegalArgumentException("Notifica con id");
        return costruttoreRichiesta.componiRichiesta();
    }

}
