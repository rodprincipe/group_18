package jsonRpc.client.costruttori;

import org.jetbrains.annotations.NotNull;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ValueNode;

import jsonRpc.client.Canale;

/**
 * Costruttore astratto per creare le richieste JSON-RPC
 */
public abstract class CostruttoreRichiesteAstratto {

    // Costanti della convenzione JSON-RPC 2.0
    protected static final String VERSIONE_2_0 = "2.0";
    protected static final String RISULTATO = "result";
    protected static final String ERRORE = "error";
    protected static final String JSONRPC = "jsonrpc";
    protected static final String ID = "id";
    protected static final String METODO = "method";
    protected static final String PARAMETRI = "params";

    /**
     * Front-end per inoltrare la richiesta generata e riceve la risposta
     */
    @NotNull
    protected final Canale canale;

    /**
     * Jackson mapper per processarer i JSON
     */
    @NotNull
    protected final ObjectMapper mapper;

    public CostruttoreRichiesteAstratto(@NotNull Canale canale, @NotNull ObjectMapper mapper) {
        this.canale = canale;
        this.mapper = mapper;
    }

    /**
     * Crea un array JSON partendo da un array classico
     *
     * @param valori array di oggetti contenente i valori
     * @return array JSON
     */
    @NotNull
    protected ArrayNode daArrayJavaAdArrayJSON(@NotNull Object[] valori) {
        ArrayNode arrayJSON = mapper.createArrayNode();
        for (Object valore : valori) {
            arrayJSON.add(mapper.valueToTree(valore));
        }
        return arrayJSON;
    }

    /**
     * Crea una nuova richiesta JSON-RPC come oggetto JSON
     *
     * @param id            id della richiesta
     * @param metodo        nome del metodo della richiesta
     * @param parametri     parametri della richiesta
     * @return oggetto JSON contenente tutti i parametri della richiesta JSON-RPC 2.0
     * @throws IllegalArgumentException se il metodo non e' settato
     */
    @NotNull
    protected ObjectNode componiRichiestaAstr(@NotNull ValueNode id, @NotNull String metodo,
                                 @NotNull JsonNode parametri) {
        if (metodo.isEmpty()) {
            throw new IllegalArgumentException("Metodo non settato");
        }
        ObjectNode richiestaJSON = mapper.createObjectNode();
        richiestaJSON.put(JSONRPC, VERSIONE_2_0);
        richiestaJSON.put(METODO, metodo);
        if(!parametri.isNull())
            richiestaJSON.set(PARAMETRI, parametri);
        if (!id.isNull()) {
            richiestaJSON.set(ID, id);
        }
        return richiestaJSON;
    }
}
