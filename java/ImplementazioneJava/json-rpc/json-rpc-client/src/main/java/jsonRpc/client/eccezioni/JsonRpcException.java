package jsonRpc.client.eccezioni;

import org.jetbrains.annotations.NotNull;

import jsonRpc.core.messaggi.ErroreJsonRpc;

/**
 * Rappresenta un errore JSON-RPC ritornato dal server
 */
public class JsonRpcException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    
    /**
     * Messaggio d'errore
     */
    @NotNull
    private ErroreJsonRpc messaggioErrore;

    public JsonRpcException(@NotNull ErroreJsonRpc messaggioErrore) {
        super(messaggioErrore.toString());
        this.messaggioErrore = messaggioErrore;
    }

    @NotNull
    public ErroreJsonRpc getMessaggioErrore() {
        return messaggioErrore;
    }
}
