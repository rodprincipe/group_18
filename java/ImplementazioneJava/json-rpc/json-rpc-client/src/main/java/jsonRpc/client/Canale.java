package jsonRpc.client;

import java.io.IOException;

import org.jetbrains.annotations.NotNull;

/**
 * Interfaccia tramite la quale verra' inoltrata la richiesta JSON-RPC
 */
public interface Canale {

    /**
     * Spedisce la richiesta JSON-RPC in formato testo e ritorna la risposta JSON-RPS sempre in formato testo.
     * @param richiesta JSON-RPC come stringa
     * @return risposta JSON-RPC come stringa
     */
    @NotNull
    public String inoltra(@NotNull String richiesta) throws IOException;
}
