package jsonRpc.client.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.junit.Test;

import jsonRpc.client.JsonRpcClient;
import jsonRpc.client.Canale;
import jsonRpc.client.eccezioni.JsonRpcException;
import jsonRpc.client.eccezioni.JsonRpcLottoException;

/**
 * Classe di test Junit.<br>
 * Esegue il test di tutte le situazioni per il client proposte da specifica JSON-RPC 2.0
 */
public class TestClient {
    
    public Canale creaCanale(String richiestaTest, String rispostaTest, boolean checkRichieste){
        final String richiestaTestTrim = richiestaTest.replaceAll("\\s+","");
        final String rispostaTestTrim = rispostaTest.replaceAll("\\s+","");

        Canale canale = new Canale() {

            @Override
            public String inoltra(String richiesta) throws IOException {
                String richiestaTrim = richiesta.replaceAll("\\s+","");
                if(checkRichieste)
                    assertEquals(richiestaTestTrim, richiestaTrim);
                return rispostaTestTrim;
            }
        };
        return canale;
    }

    @Test
    public void parametriPos1() {
        JsonRpcClient c = new JsonRpcClient(creaCanale("{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [42, 23], \"id\": 1}",
                                                           "{\"jsonrpc\": \"2.0\", \"result\": 19, \"id\": 1}",
                                                           true));
        int risultato = c.creaRichiesta()
                .metodo("subtract")
                .parametri(42,23)
                .id(1)
                .ritorna(int.class)
                .esegui();
        assertEquals(42-23, risultato);
    }
    
    @Test
    public void parametriPos2() {
        JsonRpcClient c = new JsonRpcClient(creaCanale("{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [23, 42], \"id\": 2}", 
                                                           "{\"jsonrpc\": \"2.0\", \"result\": -19, \"id\": 2}",
                                                           true));
        int risultato = c.creaRichiesta()
                .id(2)
                .metodo("subtract")
                .parametri(23,42)
                .ritorna(int.class)
                .esegui();
        assertEquals(23-42, risultato);
    }
    
    @Test
    public void parametriNome1() {
        JsonRpcClient c = new JsonRpcClient(creaCanale("{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": {\"subtrahend\": 23, \"minuend\": 42}, \"id\": 3}", 
                                                           "{\"jsonrpc\": \"2.0\", \"result\": 19, \"id\": 3}",
                                                           true));
        int risultato = c.creaRichiesta()
                .id(3)
                .metodo("subtract")
                .parametro("subtrahend", 23)
                .parametro("minuend", 42)
                .ritorna(int.class)
                .esegui();
        int min = 42;
        int sub = 23;
        assertEquals(min-sub, risultato);
    }
    
    @Test
    public void parametriNome2() {
        JsonRpcClient c = new JsonRpcClient(creaCanale("{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": {\"minuend\": 42, \"subtrahend\": 23}, \"id\": 4}", 
                                                           "{\"jsonrpc\": \"2.0\", \"result\": 19, \"id\": 4}",
                                                           true));
        int risultato = c.creaRichiesta()
                .id(4)
                .metodo("subtract")
                .parametro("minuend", 42)
                .parametro("subtrahend", 23)
                .ritorna(int.class)
                .esegui();
        int min = 42;
        int sub = 23;
        assertEquals(min-sub, risultato);
    }
    
    @Test
    public void notifica1() {
        JsonRpcClient c = new JsonRpcClient(creaCanale("{\"jsonrpc\": \"2.0\", \"method\": \"update\", \"params\": [1,2,3,4,5]}",
                                                           "",
                                                           true));
        c.creaNotifica()
                .metodo("update")
                .parametri(1,2,3,4,5)
                .esegui();

    }

    @Test
    public void notifica2() {
        JsonRpcClient c = new JsonRpcClient(creaCanale("{\"jsonrpc\": \"2.0\", \"method\": \"foobar\"}",
                                                           "",
                                                           true));
        c.creaNotifica()
                .metodo("foobar")
                .esegui();

    }
    
    @SuppressWarnings("unused")
    @Test(expected = JsonRpcException.class)
    public void richiestaMetodoNonEsistente() { 
        JsonRpcClient c = new JsonRpcClient(creaCanale("{\"jsonrpc\": \"2.0\", \"method\": \"foobar\", \"id\": \"1\"}", 
                                                           "{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32601, \"message\": \"Method not found\"}, \"id\": \"1\"}",
                                                           false)   );
        try {
            int risultato = c.creaRichiesta()
                    .id(1)
                    .metodo("foobar")
                    .ritorna(int.class) //ritorno fittizio
                    .esegui();
        }catch (JsonRpcException e) {
            assertEquals(-32601,e.getMessaggioErrore().getCodice());
            throw e;
        }        
    }

    @SuppressWarnings("unused")
    @Test(expected = JsonRpcException.class)
    public void richiestaJsonNonValido() {
        JsonRpcClient c = new JsonRpcClient(creaCanale("{\"jsonrpc\": \"2.0\", \"method\": \"foobar, \"params\": \"bar\", \"baz]", 
                                                           "{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32700, \"message\": \"Parse error\"}, \"id\": null}",
                                                           false)   );
        try {
            int risultato = c.creaRichiesta()
                    .id(1)
                    .metodo("foobar")
                    .ritorna(int.class) //ritorno fittizio
                    .esegui();
        }catch (JsonRpcException e) {
            assertEquals(-32700,e.getMessaggioErrore().getCodice());
            throw e;
        }        
    }

    @SuppressWarnings("unused")
    @Test(expected = JsonRpcException.class)
    public void richiestaNonValida() {
        JsonRpcClient c = new JsonRpcClient(creaCanale("{\"jsonrpc\": \"2.0\", \"method\": 1, \"params\": \"bar\"}", 
                                                           "{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null}",
                                                           false));
        try {
            int risultato = c.creaRichiesta()
                    .id(2)
                    .metodo("1")
                    .parametri("bar")
                    .ritorna(int.class) //ritorno fittizio
                    .esegui();
        }catch (JsonRpcException e) {
            assertEquals(-32600,e.getMessaggioErrore().getCodice());
            throw e;
        }        
    }

    @SuppressWarnings("unused")
    @Test(expected = JsonRpcException.class)
    public void lottoJsonNonValido() {
        JsonRpcClient c = new JsonRpcClient(creaCanale(
                "[\r\n" + 
                "  {\"jsonrpc\": \"2.0\", \"method\": \"sum\", \"params\": [1,2,4], \"id\": \"1\"},\r\n" + 
                "  {\"jsonrpc\": \"2.0\", \"method\"\r\n" + 
                "]",
                "{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32700, \"message\": \"Parse error\"}, \"id\": null}",
                        false));
        try {
            Map<String, Object> risultato = c.creaLottoRichieste()
                    .creaRichiesta()
                        .id(1)
                        .metodo("A") //metodo fittizio
                        .ritorna(Object.class) //ritorno fittizio
                        .aggiungi()
                    .esegui();
        }catch (JsonRpcException e) {
            assertEquals(-32700,e.getMessaggioErrore().getCodice());
            throw e;
        }
    }

    @SuppressWarnings("unused")
    @Test(expected = JsonRpcLottoException.class)
    public void lottoNonValidoVuoto() {
                JsonRpcClient c = new JsonRpcClient(creaCanale(
                        "[]",
                        "[\r\n" + 
                        "  {\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null}\r\n" + 
                        "]",
                        false));
                try {
                Map<String, Object> risultato = c.creaLottoRichieste()
                    .creaRichiesta()
                        .id(1)
                        .metodo("A") //metodo fittizio
                        .ritorna(Object.class)  //ritorno fittizio
                        .aggiungi()
                    .esegui();
                }catch (JsonRpcLottoException e) {
                assertEquals(1,e.getErrori().size());
                throw e;
                }
    }
    
    @SuppressWarnings("unused")
    @Test(expected = JsonRpcLottoException.class)
    public void lottoNonValidoNonVuoto() {
                JsonRpcClient c = new JsonRpcClient(creaCanale(
                        "[1]",
                        "[\r\n" + 
                        "  {\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null}\r\n" + 
                        "]",
                        false));
                try {
                Map<String, Object> risultato = c.creaLottoRichieste()
                    .creaRichiesta()
                        .id(1)
                        .metodo("A") // metodo fittizio
                        .ritorna(Object.class) //ritorno fittizio
                        .aggiungi()
                    .esegui();
                }catch (JsonRpcLottoException e) {
                assertEquals(1,e.getErrori().size());
                assertEquals(-32600, e.getErrori().get(0).getCodice());
                throw e;
                }
    }

    @SuppressWarnings("unused")
    @Test(expected = JsonRpcLottoException.class)
    public void lottoNonValido() {
        JsonRpcClient c = new JsonRpcClient(creaCanale(
                                                            "[1,2,3]",
                                                            "[\r\n" + 
                                                            "  {\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null},\r\n" + 
                                                            "  {\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null},\r\n" + 
                                                            "  {\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null}\r\n" + 
                                                            "]",
                                                            false));
        try {
            Map<String, Object> risultato = c.creaLottoRichieste()
                                                .creaRichiesta()
                                                    .id(1)
                                                    .metodo("A") //metodo fittizio
                                                    .ritorna(Object.class) //ritorno fittizio
                                                    .aggiungi()
                                                .creaRichiesta()
                                                    .id(2)
                                                    .metodo("B") //metodo fittizio
                                                    .ritorna(Object.class) //ritorno fittizio
                                                    .aggiungi()
                                                .creaRichiesta()
                                                    .id(3)
                                                    .metodo("C") //metodo fittizio
                                                    .ritorna(Object.class) //ritorno fittizio
                                                    .aggiungi()
                                                .esegui();
        }catch (JsonRpcLottoException e) {
            assertEquals(3,e.getErrori().size());
            assertEquals(-32600, e.getErrori().get(0).getCodice());
            assertEquals(-32600, e.getErrori().get(1).getCodice());
            assertEquals(-32600, e.getErrori().get(2).getCodice());
            throw e;
        }
    }
    
    @SuppressWarnings("unused")
    @Test(expected = JsonRpcLottoException.class)
    public void lottoConErrori() {
        JsonRpcClient c = new JsonRpcClient(creaCanale(
                "[\r\n" + 
                    "{\"jsonrpc\": \"2.0\", \"method\": \"sum\", \"params\": [1,2,4], \"id\": \"1\"},\r\n" + 
                    "{\"jsonrpc\": \"2.0\", \"method\": \"notify_hello\", \"params\": [7]},\r\n" + 
                    "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [42,23], \"id\": \"2\"},\r\n" + 
                    "{\"foo\": \"boo\"},\r\n" + 
                    "{\"jsonrpc\": \"2.0\", \"method\": \"foo.get\", \"params\": {\"name\": \"myself\"}, \"id\": \"5\"},\r\n" + 
                    "{\"jsonrpc\": \"2.0\", \"method\": \"get_data\", \"id\": \"9\"} \r\n" + 
                "]", 
                "[\r\n" + 
                        "{\"jsonrpc\": \"2.0\", \"result\": 7, \"id\": \"1\"},\r\n" + 
                        "{\"jsonrpc\": \"2.0\", \"result\": 19, \"id\": \"2\"},\r\n" + 
                        "{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null},\r\n" + 
                        "{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32601, \"message\": \"Method not found\"}, \"id\": \"5\"},\r\n" + 
                        "{\"jsonrpc\": \"2.0\", \"result\": [\"hello\", 5], \"id\": \"9\"}\r\n" + 
                "]",
                false));
        try {
            Map<String, Object> risultato = c.creaLottoRichieste()
                                                .creaRichiesta()
                                                    .id(1)
                                                    .metodo("sum")
                                                    .parametri(1,2,4)
                                                    .ritorna(int.class)
                                                    .aggiungi()
                                                .creaNotifica()
                                                    .metodo("notify_hello")
                                                    .parametri(7)
                                                    .aggiungi()
                                                .creaRichiesta()
                                                    .metodo("subtract")
                                                    .parametri(42,23)
                                                    .id(2)
                                                    .ritorna(int.class)
                                                    .aggiungi()
                                                .creaRichiesta()
                                                    .id(999)
                                                    .metodo("metodoPerRichiestaInvalida")
                                                    .parametri(1)
                                                    .ritorna(int.class)
                                                    .aggiungi()
                                                 .creaRichiesta()
                                                    .id(5)
                                                    .metodo("foo.get")
                                                    .parametro("name", "myself")
                                                    .aggiungi()
                                                 .creaRichiesta()
                                                     .id(9)
                                                     .metodo("get_data")
                                                     .ritorna(ArrayList.class)
                                                     .aggiungi()
                                                     .esegui();
        }catch (JsonRpcLottoException e) {
            assertEquals(2,e.getErrori().size());
            throw e;
        }
    }
    
    @Test
    public void lottoSoloNotifiche() {
        JsonRpcClient c = new JsonRpcClient(creaCanale(
                                                            "[\r\n" + 
                                                            "        {\"jsonrpc\": \"2.0\", \"method\": \"notify_sum\", \"params\": [1,2,4]},\r\n" + 
                                                            "        {\"jsonrpc\": \"2.0\", \"method\": \"notify_hello\", \"params\": [7]}\r\n" + 
                                                            "]",
                                                            "",
                                                            true));
        Map<String, Object> risultato = c.creaLottoRichieste()
                                                .creaNotifica()
                                                    .metodo("notify_sum")
                                                    .parametri(1,2,4)
                                                    .aggiungi()
                                                .creaNotifica()
                                                    .metodo("notify_hello")
                                                    .parametri(7)
                                                    .aggiungi()
                                                .esegui();
        assertTrue(risultato.isEmpty());

        }
    
}

