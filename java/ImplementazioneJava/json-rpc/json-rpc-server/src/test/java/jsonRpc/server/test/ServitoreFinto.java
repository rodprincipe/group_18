package jsonRpc.server.test;

import jsonRpc.server.strutturaServitore.NomeParametro;
public class ServitoreFinto {

    public ServitoreFinto() {
        super();
    }

    public int subtract(@NomeParametro(valore="minuend") int minuend, @NomeParametro(valore="subtrahend") int subtrahend) {
        return minuend-subtrahend;
    }

    public void update(int a, int b, int c, int d, int f) {
        //codice...
    }

    public int sum(int val1, int val2, int val3) {
        return val1+val2+val3;
    }
    
    public void notify_hello(int valore) {
        //codice
    }
    
    public Object[] get_data() {
        return new Object[]{"hello", 5};
    }

}
