package jsonRpc.server.test;
import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import jsonRpc.server.JsonRpcServer;

/**
 * Classe di test Junit.<br>
 * Esegue il test di tutte le situazioni secondo le specifiche JSON-RPC 2.0 
 */
public class TestServer {
    
    private static JsonRpcServer server;
    
    @BeforeClass
    public static void inizializza() {
        ServitoreFinto servitore = new ServitoreFinto();
        server = new JsonRpcServer(servitore);
    }
    
    @Test
    public void parametriPos1() {
        System.out.println("Test: parametriPos1");
        String richiesta = "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [42, 23], \"id\": 1}";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "{\"jsonrpc\": \"2.0\", \"result\": 19, \"id\": 1}";
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }
    
    @Test
    public void parametriPos2() {
        System.out.println("Test: parametriPos2");
        String richiesta = "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [23, 42], \"id\": 2}";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "{\"jsonrpc\": \"2.0\", \"result\": -19, \"id\": 2}";
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }

    @Test
    public void parametriNome1() {
        System.out.println("Test: parametriNome1");
        String richiesta = "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": {\"subtrahend\": 23, \"minuend\": 42}, \"id\": 3}";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "{\"jsonrpc\": \"2.0\", \"result\": 19, \"id\": 3}";
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }
    
    @Test
    public void parametriNome2() {
        System.out.println("Test: parametriNome1");
        String richiesta = "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": {\"minuend\": 42, \"subtrahend\": 23}, \"id\": 4}";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "{\"jsonrpc\": \"2.0\", \"result\": 19, \"id\": 4}";
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }
    
    @Test
    public void notifica1() {
        System.out.println("Test: notifica1");
        String richiesta = "{\"jsonrpc\": \"2.0\", \"method\": \"update\", \"params\": [1,2,3,4,5]}";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "";
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }
    
    @Test
    public void notifica2() {
        System.out.println("Test: notifica2");
        String richiesta = "{\"jsonrpc\": \"2.0\", \"method\": \"foobar\"}";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "";
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }
    
    @Test
    public void richiestaMetodoNonEsistente() {
        System.out.println("Test: richiestaMetodoNonEsistente");
        String richiesta = "{\"jsonrpc\": \"2.0\", \"method\": \"foobar\", \"id\": \"1\"}";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32601, \"message\": \"Method not found\"}, \"id\": \"1\"}";
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }
    
    @Test
    public void richiestaJsonNonValido() {
        System.out.println("Test: richiestaJsonNonValida");
        String richiesta = "{\"jsonrpc\": \"2.0\", \"method\": \"foobar, \"params\": \"bar\", \"baz]";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32700, \"message\": \"Parse error\"}, \"id\": null}";
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }
    
    @Test
    public void richiestaNonValida() {
        System.out.println("Test: richiestaNonValida");
        String richiesta = "{\"jsonrpc\": \"2.0\", \"method\": 1, \"params\": \"bar\"}";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null}";
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }

    
    @Test
    public void lottoJsonNonValido() {
        System.out.println("Test: lottoJsonNonValido");
        String richiesta =   "[\r\n" + 
                             "  {\"jsonrpc\": \"2.0\", \"method\": \"sum\", \"params\": [1,2,4], \"id\": \"1\"},\r\n" + 
                             "  {\"jsonrpc\": \"2.0\", \"method\"\r\n" + 
                             "]";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32700, \"message\": \"Parse error\"}, \"id\": null}";
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }
    
    @Test
    public void lottoNonValidoVuoto() {
        System.out.println("Test: lottoNonValidoVuoto");
        String richiesta =   "[]";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null}\r\n";
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }
    
    @Test
    public void lottoNonValidoNonVuoto() {
        System.out.println("Test: lottoNonValidoNonVuoto");
        String richiesta =  "[1]";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "[\r\n" + 
                                "  {\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null}\r\n" + 
                                "]";
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }
    
    @Test
    public void lottoNonValido() {
        System.out.println("Test: lottoNonValido");
        String richiesta =  "[1,2,3]";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "[\r\n" + 
                                "  {\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null},\r\n" + 
                                "  {\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null},\r\n" + 
                                "  {\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null}\r\n" + 
                                "]";
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }

    @Test
    public void lottoConErrori() {
        System.out.println("Test: lottoConErrori");
        String richiesta =  "[\r\n" + 
                                "{\"jsonrpc\": \"2.0\", \"method\": \"sum\", \"params\": [1,2,4], \"id\": \"1\"},\r\n" + 
                                "{\"jsonrpc\": \"2.0\", \"method\": \"notify_hello\", \"params\": [7]},\r\n" + 
                                "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [42,23], \"id\": \"2\"},\r\n" + 
                                "{\"foo\": \"boo\"},\r\n" + 
                                "{\"jsonrpc\": \"2.0\", \"method\": \"foo.get\", \"params\": {\"name\": \"myself\"}, \"id\": \"5\"},\r\n" + 
                                "{\"jsonrpc\": \"2.0\", \"method\": \"get_data\", \"id\": \"9\"} \r\n" + 
                            "]";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "[\r\n" + 
                                        "{\"jsonrpc\": \"2.0\", \"result\": 7, \"id\": \"1\"},\r\n" + 
                                        "{\"jsonrpc\": \"2.0\", \"result\": 19, \"id\": \"2\"},\r\n" + 
                                        "{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32600, \"message\": \"Invalid Request\"}, \"id\": null},\r\n" + 
                                        "{\"jsonrpc\": \"2.0\", \"error\": {\"code\": -32601, \"message\": \"Method not found\"}, \"id\": \"5\"},\r\n" + 
                                        "{\"jsonrpc\": \"2.0\", \"result\": [\"hello\", 5], \"id\": \"9\"}\r\n" + 
                                "]" ;
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
    }
    
    @Test
    public void lottoSoloNotifiche() {
        System.out.println("Test: lottoSoloNotifiche");
        String richiesta =  "[\r\n" + 
                            "        {\"jsonrpc\": \"2.0\", \"method\": \"notify_sum\", \"params\": [1,2,4]},\r\n" + 
                            "        {\"jsonrpc\": \"2.0\", \"method\": \"notify_hello\", \"params\": [7]}\r\n" + 
                            "]";
        String rispostaEffettiva = server.gestisci(richiesta);
        String rispostaAttesa = "" ;
        String rispostaEffettivaTrim = rispostaEffettiva.replaceAll("\\s+","");
        String rispostaAttesaTrim = rispostaAttesa.replaceAll("\\s+","");
        assertEquals(rispostaAttesaTrim, rispostaEffettivaTrim);
        System.out.println("~~~~~~~~~~~~~~~~");System.out.println();
        }
}
