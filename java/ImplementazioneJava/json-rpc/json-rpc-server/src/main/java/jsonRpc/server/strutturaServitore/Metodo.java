package jsonRpc.server.strutturaServitore;

import java.lang.reflect.Method;

import org.jetbrains.annotations.NotNull;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableMap;

/**
 * Dati di un metodo JSON-RPC<br>
 * <li>Nome del metodo
 * <li>Metodo Java
 * <li>Visibilita' del metodo
 * <li>Tipo Java che ritorna il metodo
 * <li>Mappa dei parametri
 */
public class Metodo {

    /**
     * Nome metodo JSON-RPC
     */
    @NotNull
    private final String nomeMetodoJsonRpc;

    /**
     * Metodo Java
     */
    @NotNull
    private final Method metodoJava;
    
    /**
     * Visibilita' del metodo
     */
    private final int visibilita;
    
    /**
     * Tipo del oggetto che verra ritornato dalla chiamata del metodo
     */
    private Class<?> ritorno;
    
    /**
     * Mappa tra nome del metodo JSON-RPC e i suoi parametri
     */
    @NotNull
    private final ImmutableMap<String, Parametro> mappaParametri;

    
    public Metodo(@NotNull String nomeMetodoJsonRpc, @NotNull Method metodoJava, @NotNull ImmutableMap<String, Parametro> mappaParametri) {
        this.nomeMetodoJsonRpc = nomeMetodoJsonRpc;
        this.metodoJava = metodoJava;
        this.visibilita = metodoJava.getModifiers();
        this.ritorno = metodoJava.getReturnType();
        this.mappaParametri = mappaParametri;
    }

    @NotNull
    public String getNomeMetodoJsonRpc() {
        return nomeMetodoJsonRpc;
    }

    @NotNull
    public Method getMetodoJava() {
        return metodoJava;
    }

    @NotNull
    public int getVisibilita() {
        return visibilita;
    }

    /**
     * @return the ritorno
     */
    public Class<?> getRitorno() {
        return ritorno;
    }

    @NotNull
    public ImmutableMap<String, Parametro> getMappaParametri() {
        return mappaParametri;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("nome", nomeMetodoJsonRpc)
                .add("metodo", metodoJava)
                .add("parametri", mappaParametri)
                .toString();
    }
}
