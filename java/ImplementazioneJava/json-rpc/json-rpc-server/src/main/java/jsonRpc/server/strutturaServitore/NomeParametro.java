package jsonRpc.server.strutturaServitore;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)

/**
 * Annotazione per assegnare un nome personalizzato ad un parametro di un determinato metodo
 */
public @interface NomeParametro {

    /**
     * Ritorna il nome del parametro
     * @return nome settato per il parametro
     */
    String valore();
}
