package jsonRpc.server.strutturaServitore;

import com.google.common.base.MoreObjects;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;

/**
 * Dati di un parametro JSON-RPC<br>
 * <li>Nome del parametro del metodo JSON-RPC
 * <li>Tipo del parametro
 * <li>Tipo generico del parametro
 * <li>Indice di posizione
 */
public class Parametro {

    /**
     * Nome parametro JSON-RPC
     */
    @NotNull
    private final String nomeParametroJsonRpc;

    /**
     * Tipo Java del parametro
     */
    @NotNull
    private final Class<?> tipo;

    /**
     * Tipo generico del parametro
     */
    @NotNull
    private final Type generico;

    /**
     * Indice di posizione tra gli argomenti del metodo
     */
    private final int indice;

    public Parametro(@NotNull String nome, @NotNull Class<?> tipo
            , @NotNull Type tipoGenerico, int indice) {
        this.nomeParametroJsonRpc = nome;
        this.tipo = tipo;
        this.generico = tipoGenerico;
        this.indice = indice;
    }

    @NotNull
    public String getNomeParametroJsonRpc() {
        return nomeParametroJsonRpc;
    }

    @NotNull
    public Class<?> getTipo() {
        return tipo;
    }

    public int getIndice() {
        return indice;
    }

    @NotNull
    public Type getGenerico() {
        return generico;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("nome", nomeParametroJsonRpc)
                .add("tipo", tipo)
                .add("generic", generico)
                .add("indice", indice)
                .toString();
    }
}
