package jsonRpc.server;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.HashMap;
import java.util.Map;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ContainerNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ValueNode;
import com.google.common.collect.ImmutableMap;

import jsonRpc.core.messaggi.ErroreApplicazione;
import jsonRpc.core.messaggi.ErroreJsonRpc;
import jsonRpc.core.messaggi.Richiesta;
import jsonRpc.core.messaggi.Risposta;
import jsonRpc.core.messaggi.RispostaErrore;
import jsonRpc.core.messaggi.RisultatoSuccesso;
import jsonRpc.server.dati.Metodo;
import jsonRpc.server.dati.NomeParametro;
import jsonRpc.server.dati.Parametro;

/**
 * Classe principale per processare lato server le richieste JSON-RPC 2.0.<br><br>
 * -Converte il testo della richiesta JSON-RPC un un albero JSON.<br>
 * -Controlla che la richiesta si conforme con le specifiche JSON-RPC 2.0.<br>
 * -Analizza il servitore delle chiamate fornito per la gestione delle richieste.<br>
 * -Trova il metodo richiesto<br>
 * -Prepara i parametri per il metodo richiesto in base ai suoi metadati<br>
 * -Invoca il metodo<br>
 * -Costruisce la risposta come albero JSON e in seguito la trasforma in testo<br>
 * -In caso di errore ritorna l'errore opportuno secondo la specifica JSON-RPC 2.0<br>
 */
public class JsonRpcServer {

    // Messaggi di errore
    private static final ErroreJsonRpc ERRORE_PARSING = new ErroreJsonRpc(-32700, "Parse error");
    private static final ErroreJsonRpc METODO_NON_TROVATO = new ErroreJsonRpc(-32601, "Method not found");
    private static final ErroreJsonRpc RICHIESTA_INVALIDA = new ErroreJsonRpc(-32600, "Invalid Request");
    private static final ErroreJsonRpc PARAMETRI_INVALIDI = new ErroreJsonRpc(-32602, "Invalid params");
    private static final ErroreJsonRpc ERRORE_INTERNO = new ErroreJsonRpc(-32603, "Internal error");
    private static final ErroreJsonRpc ERRORE_SERVER = new ErroreJsonRpc(-32000, "Server error");

    private static final Logger log = LoggerFactory.getLogger(JsonRpcServer.class);
    @NotNull
    private ObjectMapper mapperJSON;
    private Object servitore;
    private Map<String, Metodo> servitoreMappato;

    public Map<String, Metodo> getServitoreMappato() {
        return servitoreMappato;
    }

    /**
     * Inizializza un server JSON-RPC 2.0
     *
     * @param mapperJSON    mapper JSON
     * @param servitore     servitore delle chiamate che gestira' la chiamata
     */
    public JsonRpcServer(@NotNull ObjectMapper mapperJSON, Object servitore) {
        this.mapperJSON = mapperJSON;
        this.servitore = servitore;
        this.servitoreMappato = analizzaServitore(servitore);
    }

    /**
     * Inizializza un server JSON-RPC 2.0 con parametri standard per mapper e specifico servitore delle chiamate
     * 
     * @param servitore servitore delle chiamate che gestira' la chiamata
     */
    public JsonRpcServer(Object servitore) {
        this(new ObjectMapper(), servitore);
    }

    /**
     * Analizza il servitore fornito e ne restituisce una mapppa con le specifiche di ogni metodo e con i reltivi parametri
     * @param servitore classe di cui analizzare i vari metodi
     * @return mappaura dei metodi
     */
    private Map<String, Metodo> analizzaServitore(Object servitore) {
        Map<String, Metodo> mappaMetodi = new HashMap<String, Metodo>();
        for (Method metodo : servitore.getClass().getMethods()) {
            log.info("Metodo : "+metodo.getName());
            ImmutableMap.Builder<String, Parametro> mappaParametri = ImmutableMap.builder();
            int indice = 0;
            String nomeParametroString;
            for (Parameter parametro : metodo.getParameters()) {
                nomeParametroString = parametro.getName();
                for (Annotation a : parametro.getAnnotations()){
                    if(a.annotationType().equals(NomeParametro.class)) {
                        NomeParametro nomeParametro = (NomeParametro)a;
                        nomeParametroString = nomeParametro.valore();
                    }
                }
                log.debug("\tParametro :"+nomeParametroString);
                Parametro parametroNuovo = new Parametro(nomeParametroString, parametro.getType(), parametro.getParameterizedType(), indice);
                mappaParametri.put(nomeParametroString, parametroNuovo);
                indice++;
            }
            Metodo metodoNuovo = new Metodo(metodo.getName(), metodo, mappaParametri.build());
            mappaMetodi.put(metodo.getName(), metodoNuovo);
        }
        return mappaMetodi;
    }

    /**
     * Gestisce una richieste o un lotto di richieste JSON-RPC.<br>
     * Dopo aver analizzato il servitore delle chiamate gli delega la gestione della richiesta e ritorna una risposta JSON-RPC (di successo o di errore)
     *
     * @param richiestaStringa stringa con la richiesta 
     * @return stringa di risposta
     */
    @NotNull
    public String gestisci(@NotNull String richiestaStringa) {

        JsonNode richiestaJson;
        try {
            richiestaJson = mapperJSON.readTree(richiestaStringa);
            if (log.isDebugEnabled()) {
                log.debug("Richiesta : {}", mapperJSON.writeValueAsString(richiestaJson));
            }
        } catch (IOException e) {
            log.error("Problema nel parsing della richiesta: \""+ richiestaStringa+"\"");
            return inJson(new RispostaErrore(ERRORE_PARSING));
        }

        if (richiestaJson.isObject()) {
            Risposta risposta = gestisciRichiesta(richiestaJson);
            return isNotifica(richiestaJson, risposta) ? "" : inJson(risposta);
        } 
        else if (richiestaJson.isArray() && richiestaJson.size() > 0) {
            ArrayNode risposte = mapperJSON.createArrayNode();
            for (JsonNode singolaRichiestaJson : (ArrayNode) richiestaJson) {
                Risposta risposta = gestisciRichiesta(singolaRichiestaJson);
                if (!isNotifica(singolaRichiestaJson, risposta)) {
                    risposte.add(mapperJSON.convertValue(risposta, ObjectNode.class));
                }
            }
            return risposte.size() > 0 ? inJson(risposte) : "";
        }else {
        log.error("Richiesta JSON-RPC non valida: \"" + richiestaStringa + "\"");
        return inJson(new RispostaErrore(RICHIESTA_INVALIDA));
        }
    }

    /**
     * E' una notifica JSON-RPC 2.0?<br>
     * <u>N.B. : una notifica non puo produrre un errore di parsing o di richesta invalida</u>
     *
     * @param richiesta a request in a JSON tree format
     * @param risposta    a response in a Java object format
     * @return {@code true} if a request is a "notification request"
     */
    private boolean isNotifica(@NotNull JsonNode richiesta, @NotNull Risposta risposta) {
        if (richiesta.get("id") == null) {
            if (risposta instanceof RisultatoSuccesso) {
                return true;
            } else if (risposta instanceof RispostaErrore) {
                int errorCode = ((RispostaErrore) risposta).getErrore().getCodice();
                if (errorCode != ERRORE_PARSING.getCodice() && errorCode != RICHIESTA_INVALIDA.getCodice()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Analizza e converte la richiesta JSON-RPC, se e' una richiesta valida e ben formata la gestisce.<br>
     * Nel caso venga lanciata una eccezione eseguendo la richiesta verra' gestita anche questa.
     *
     * @param richiestaJSON richiesta JSON-RPC come albero JSON
     * @param servitoreMappato     istanza del servitore delle chiamate
     * @return risposta JSON-RPC 
     */
    @NotNull
    private Risposta gestisciRichiesta(@NotNull JsonNode richiestaJSON) {
        Richiesta richiesta;
        try {
            richiesta = mapperJSON.convertValue(richiestaJSON, Richiesta.class);
        } catch (Exception e) {
            log.error("Richiesta JSON-RPC non valida: \"" + richiestaJSON.asText()+"\"", e);
            return new RispostaErrore(RICHIESTA_INVALIDA);
        }

        try {
            return analizzaPiuInvoca(richiesta);
        } catch (Exception e) {
            log.error("Eccezione "+e.getCause().getClass().getSimpleName()+" durate la gestione della richiesta: " + richiesta);
            return gestisciErrore(richiesta, e);
        }
    }

    /**
     * Gestisce un eccezione generata dalla chiamata del metodo richiesto.
     *
     * @param richiesta come oggetto java
     * @param e eccezione generata
     * @return risposta di errore con messaggio appropriato
     */
    @NotNull
    private RispostaErrore gestisciErrore(@NotNull Richiesta richiesta, @NotNull Exception e) {
        if(e instanceof InvocationTargetException){
            Throwable eReale = e.getCause();
            if (eReale instanceof ErroreApplicazione) {
                ErroreJsonRpc errore = ((ErroreApplicazione)eReale).getErrore();
                return new RispostaErrore(richiesta.getId(), errore);
            }else {
                return new RispostaErrore(richiesta.getId(), ERRORE_INTERNO);
            }
        }else {
            return new RispostaErrore(richiesta.getId(), ERRORE_SERVER);
        }
    }
    
    /**
     * Analizza una richiesta JSON-RPC e dopo aver verificato che abbia tutti gli elementi di una richiesta JSON-RPC 2.0
     * invoca il metodo richiesto e ritorna una risposta JSON-RPC.
     *
     * @param richiesta secondo specifiche JSON-RPC 2.0 
     * @param servitoreMappato instanza del servitore delle chiamate au metodi remoti
     * @return risposta JSON-RPC 
     * @throws Exception in caso di errori a runtime oppure in caso di errori dalla applicazione (lanciati dal servitore)
     */
    @NotNull
    private Risposta analizzaPiuInvoca(@NotNull Richiesta richiesta) throws Exception {
        String nomeMetodo = richiesta.getMetodo();
        String jsonrpc = richiesta.getJsonrpc();
        ValueNode idJSON = richiesta.getId();
        if (jsonrpc == null || nomeMetodo == null) {
            log.error("Non è una richiesta JSON-RPC mancano degli elementi: " + richiesta);
            return new RispostaErrore(idJSON, RICHIESTA_INVALIDA);
        }

        if (!jsonrpc.equals(Risposta.VERSIONE)) {
            log.error("Non è una richiesta JSON-RPC 2.0: " + richiesta);
            return new RispostaErrore(idJSON, RICHIESTA_INVALIDA);
        }

        JsonNode parametriJSON = richiesta.getParametri();
        if (!parametriJSON.isObject() && !parametriJSON.isArray() && !parametriJSON.isNull()) {
            log.error("I parametri possono essere un oggetto, un array o null: " + richiesta);
            return new RispostaErrore(idJSON, RICHIESTA_INVALIDA);
        }

        Metodo metodo = servitoreMappato.get(nomeMetodo);
        if (metodo == null) {
            log.error("Metodo \"" + nomeMetodo + "\" non trovato nel servitore \"" + servitore.getClass().getSimpleName() +"\"");
            return new RispostaErrore(idJSON, METODO_NON_TROVATO);
        }
        if (metodo.getVisibilita()!=Modifier.PUBLIC) {
            log.error("Metodo \"" + nomeMetodo + "\" non pubblico nel servitore \"" + servitore.getClass().getSimpleName() +"\"");
            return new RispostaErrore(idJSON, METODO_NON_TROVATO);
        }

        ContainerNode<?> contenitoreParametri = !parametriJSON.isNull() ? (ContainerNode<?>) parametriJSON : mapperJSON.createObjectNode();
        Object[] parametriConvertiti;
        try {
            parametriConvertiti = convertiParametri(contenitoreParametri, metodo);
        } catch (IllegalArgumentException e) {
            log.error("Parametri non validi: " + contenitoreParametri + " per il metodo " + metodo.getNomeMetodoJsonRpc(),e);
            return new RispostaErrore(idJSON, PARAMETRI_INVALIDI);
        }
        Object risultato = metodo.getMetodoJava().invoke(servitore, parametriConvertiti);
        return new RisultatoSuccesso(idJSON, risultato);
    }

    /**
     * Converte i parametri forniti nel tipo atteso dal metodo
     *
     * @param parametriJSON strutturati per la chiamata per nome (come oggetti json) o per posizione (come array json)
     * @param metodo contente tutti i dettagli del metodo da chiamare
     * @return array di oggetti java da passare al momento del invocazione del metodo
     */
    @NotNull
    private Object[] convertiParametri(@NotNull ContainerNode<?> parametriJSON,@NotNull Metodo metodo) {
        int numParametriMetodo = metodo.getMappaParametri().size();
        int numParametri = parametriJSON.size();
        
        if (numParametri != numParametriMetodo) {
            throw new IllegalArgumentException("Il numero di argomenti forniti per il metodo \""
                                                +metodo.getNomeMetodoJsonRpc()+"\" non e'valido:"
                                                +" forniti:" + numParametri
                                                +" richiesti:" + numParametriMetodo + "");
        }

        Object[] parametriMetodo = new Object[numParametriMetodo];
        for (Parametro parametro : metodo.getMappaParametri().values()) {
            int indice = parametro.getIndice();
            String nome = parametro.getNomeParametroJsonRpc();
            JsonNode parametroJSON = parametriJSON.isObject() ? parametriJSON.get(nome) : parametriJSON.get(indice);
            if (parametroJSON == null || parametroJSON.isNull()) {
                    throw new IllegalArgumentException("Manca il parametro \"" + nome +"\" del metodo \"" + metodo.getNomeMetodoJsonRpc()+"\"");
            }

            try {
                JsonParser jsonParser = mapperJSON.treeAsTokens(parametroJSON);
                JavaType javaType = mapperJSON.getTypeFactory().constructType(parametro.getGenerico());
                parametriMetodo[indice] = mapperJSON.readValue(jsonParser, javaType);
            } catch (IOException e) {
                throw new IllegalArgumentException("Tipo parametro errato: " + parametroJSON + ". Tipo parametro atteso: '" + parametro, e);
            }
        }
        return parametriMetodo;
    }

    /**
     * Metodo per trasformare un oggetto in stringa Json che non lanci eccezioni uncheched 
     *
     * @param daTrasformare oggetto da trasformare in stringa json
     * @return stringa trasformata in json
     */
    @NotNull
    private String inJson(@NotNull Object daTrasformare) {
        try {
            String trasformato = mapperJSON.writeValueAsString(daTrasformare);
            if (log.isDebugEnabled()) {
                log.debug("Risposta: {}", trasformato);
            }
            return trasformato;
        } catch (JsonProcessingException e) {
            log.error("Impossibile scrivere in json: " + daTrasformare, e);
            throw new IllegalStateException(e);
        }
    }
}
