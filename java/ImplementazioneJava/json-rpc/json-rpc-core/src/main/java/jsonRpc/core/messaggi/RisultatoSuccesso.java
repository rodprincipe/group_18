package jsonRpc.core.messaggi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.node.ValueNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Rappresentazione di una risposta JSON-RPC di successo
 */
@JsonPropertyOrder({"jsonrpc","result","id"})
public class RisultatoSuccesso extends Risposta {

    @Nullable
    @JsonProperty("result")
    private final Object risultato;

    public RisultatoSuccesso(@JsonProperty("id") @NotNull ValueNode id,
                           @JsonProperty("result") @Nullable Object risultato) {
        super(id);
        this.risultato = risultato;
    }

    @Nullable
    public Object getRisultato() {
        return risultato;
    }
}
