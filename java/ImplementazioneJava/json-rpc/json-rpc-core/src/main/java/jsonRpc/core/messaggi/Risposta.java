package jsonRpc.core.messaggi;

import org.jetbrains.annotations.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.node.ValueNode;

/**
 * Rappresentazione di una risposta JSON-RPC (di successo o di errore)
 */
public class Risposta {

    public static final String VERSIONE = "2.0";

    @NotNull
    @JsonProperty("jsonrpc")
    private final String jsonrpc;

    @NotNull
    @JsonProperty("id")
    private final ValueNode id;

    public Risposta(@NotNull ValueNode id) {
        this.id = id;
        jsonrpc = VERSIONE;
    }

    public Risposta(@NotNull ValueNode id, @NotNull String jsonrpc) {
        this.id = id;
        this.jsonrpc = jsonrpc;
    }

    @NotNull
    public String getJsonrpc() {
        return jsonrpc;
    }

    @NotNull
    public ValueNode getId() {
        return id;
    }
}
