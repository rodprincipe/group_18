package jsonRpc.core.messaggi;

/**
 * Interfaccia implementa da una eccezione che definisce un nuovo errore JSON-RPC 2.0 con codice e ralativo errore
 */
public interface ErroreApplicazione {
    
    /**
     * Getter del errore
     * @return {@link ErroreJsonRpc}
     */
    public ErroreJsonRpc getErrore();
}
