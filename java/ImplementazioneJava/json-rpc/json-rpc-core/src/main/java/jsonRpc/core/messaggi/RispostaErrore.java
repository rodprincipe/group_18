package jsonRpc.core.messaggi;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.ValueNode;
import org.jetbrains.annotations.NotNull;

/**
 * Rappresentazione di una risposta JSON-RPC di errore
 */
@JsonPropertyOrder({"jsonrpc","error","id"})
public class RispostaErrore extends Risposta {

    @NotNull
    @JsonProperty("error")
    private final ErroreJsonRpc errore;

    @JsonCreator
    public RispostaErrore(@JsonProperty("id") @NotNull ValueNode id,
                         @JsonProperty("error") @NotNull ErroreJsonRpc errore) {
        super(id);
        this.errore = errore;
    }

    public RispostaErrore(@NotNull ErroreJsonRpc errore) {
        super(NullNode.getInstance());
        this.errore = errore;
    }

    @NotNull
    public ErroreJsonRpc getErrore() {
        return errore;
    }
}
