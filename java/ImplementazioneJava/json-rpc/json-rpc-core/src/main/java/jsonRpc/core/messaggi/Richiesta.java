package jsonRpc.core.messaggi;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ValueNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


/**
 * Rappresentazione di una richiesta JSON-RPC
 */
public class Richiesta {

    @Nullable
    private final String jsonrpc;

    @Nullable
    private final String metodo;

    @NotNull
    private final JsonNode parametri;

    @NotNull
    private final ValueNode id;

    public Richiesta(@JsonProperty("jsonrpc") @Nullable String jsonrpc,
                   @JsonProperty("method") @Nullable String metodo,
                   @JsonProperty("params") @NotNull JsonNode parametri,
                   @JsonProperty("id") @NotNull ValueNode id) {
        this.jsonrpc = jsonrpc;
        this.metodo = metodo;
        this.id = id;
        this.parametri = parametri;
    }

    @Nullable
    public String getJsonrpc() {
        return jsonrpc;
    }

    @Nullable
    public String getMetodo() {
        return metodo;
    }

    @NotNull
    public ValueNode getId() {
        return id;
    }

    @NotNull
    public JsonNode getParametri() {
        return parametri;
    }

    @Override
    public String toString() {
        return "Request{jsonrpc=" + jsonrpc + ", method=" + metodo + ", id=" + id + ", params=" + parametri + "}";
    }
}
