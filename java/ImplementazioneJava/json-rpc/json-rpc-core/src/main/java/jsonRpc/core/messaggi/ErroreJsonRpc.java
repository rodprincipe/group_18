package jsonRpc.core.messaggi;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.jetbrains.annotations.NotNull;

/**
 * Rappresentazinoe di un messaggio di errore JSON-RPC
 */
public class ErroreJsonRpc {

    @JsonProperty("code")
    private final int codice;

    @NotNull
    @JsonProperty("message")
    private final String messaggio;

    public ErroreJsonRpc(@JsonProperty("code") int codice,
                        @JsonProperty("message") @NotNull String messaggio) {
        this.codice = codice;
        this.messaggio = messaggio;
    }

    @NotNull
    public int getCodice() {
        return codice;
    }

    @NotNull
    public String getMessaggio() {
        return messaggio;
    }

    @Override
    public String toString() {
        return "ErrorMessage{codice=" + codice + ", messaggio=" + messaggio + "}";
    }
}
